--USE [CWP_3.0]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_NOVARTIS_JADENU]    Script Date: 10/17/2018 6:15:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--EXEC usp_CF_NOVARTIS_JADENU @ClientID = 15, @Userid = 2635, @IndOrDrug = 'IO'
ALTER PROCEDURE [dbo].[usp_CF_NOVARTIS_JADENU]
	@ClientID INT,
	@Userid INT,
	@IndOrDrug VARCHAR(50)
AS
BEGIN

DECLARE @selectedFields AS VARCHAR(MAX)
DECLARE @StateRaw AS CF_NOVARTIS_STATE_IO
DECLARE @PbmRaw   AS CF_NOVARTIS_PBM_IO
DECLARE @States TABLE(ID INT IDENTITY(1,1) NOT NULL, [State] VARCHAR(255), [StateName] VARCHAR(255))
DECLARE @PbmBT       AS CF_NOVARTIS_RESULT_JADENU
/*DECLARE @PbmNTDT     AS CF_NOVARTIS_RESULT_JADENU*/
DECLARE @OutputUnion AS CF_NOVARTIS_RESULT_JADENU

	
	
	------------------------ Get Raw tables from usp_Get_Plans ------------------------
	-- Get State File
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_STATE_IO','');
	INSERT INTO @StateRaw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = @IndOrDrug, 							
			@Userid = @Userid, 		
			@SelectFields = @selectedFields,
			@Level = 'State'


	-- Get PBM File
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_PBM_IO','');
	INSERT INTO @PbmRaw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = @IndOrDrug, 							
			@Userid = @Userid, 				
			@SelectFields = @selectedFields,					
			@isPBMOnly = 1
																		--SELECT * FROM @StateRaw
																		--SELECT * FROM @PbmRaw

	
	
	------------------------ Filter, remove unwanted rows ------------------------
	DELETE FROM @StateRaw
	WHERE (Drug_Name <> 'Jadenu')

	DELETE FROM @PbmRaw
	WHERE (Drug_Name <> 'Jadenu') OR (MCoid NOT IN (SELECT McoId FROM PBMFilter pf WHERE (pf.ClientID = @ClientID) AND (pf.Indication = @IndOrDrug)))

																		--SELECT * FROM @StateRaw
																		--SELECT * FROM @PbmRaw


	
	
	------------------------ Remapping and Business Rules ------------------------

	-- Fill State BT
	INSERT INTO @OutputUnion
		SELECT
			[STATE_CD] = LTRIM(RTRIM(REPLACE(REPLACE([State],CHAR(13),' '),CHAR(10),' ') )),                                 /* State-PBM specific */
			[STATE_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([StateName],CHAR(13),' '),CHAR(10),' ') )),                           /* State-PBM specific */
			[PLAN_ID] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_RANK] = '',
			[LIVES] = LTRIM(RTRIM(REPLACE(REPLACE([Pharmacy],CHAR(13),' '),CHAR(10),' ') )),                                 /* State-PBM specific */

			[PLAN_TYPE] =	REPLACE(REPLACE(
			
								ISNULL( CASE LTRIM(RTRIM([Segment]))                /* State-PBM specific */
									WHEN 'Managed Medicaid' THEN 'Medicaid'
									WHEN 'State Medicaid' THEN 'Medicaid'
									WHEN 'Managed Medicare' THEN 'Medicare'
									WHEN 'Commercial MCO' THEN 'Commercial'
								ELSE 
									LTRIM(RTRIM([Segment]))
								END,
								'')

							,CHAR(13),' '),CHAR(10),' '),

			[TIER] =		REPLACE(REPLACE(

								ISNULL( CASE LTRIM(RTRIM([Novartis_Healthplan_Management])) 
									WHEN 'To PI or Better' THEN 'Covered'
									WHEN 'Non-Preferred' THEN 'Covered'
									WHEN 'Step Edit - Preferred' THEN 'Covered'
								ELSE 
									LTRIM(RTRIM([Novartis_Healthplan_Management]))
								END,
								'')

							,CHAR(13),' '),CHAR(10),' '),

			[TIER_NUMBER] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )), 
			[INDICATION] = 'BT',                                                        /* BT-NTDT specific */
			[DW_INS_DT] = LTRIM(RTRIM(REPLACE(REPLACE([dbo].[udf_ConvertToDate]([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )), 
			[RETAIL_COPAY_MIN] = '',
			[RETAIL_COPAY_MAX] = '',
			[MO_COPAY_MIN] = '',
			[MO_COPAY_MAX] = '',

			[PA_URL] =	REPLACE(REPLACE(

							ISNULL( CASE LTRIM(RTRIM([URL_to_PA_Form])) 
										WHEN 'Data Not Available' THEN LTRIM(RTRIM([General_PA_Form]))
									ELSE 
										LTRIM(RTRIM([URL_to_PA_Form]))
									END,
									'')

						,CHAR(13),' '),CHAR(10),' '),

			[RESTRICTION_CODE] = REPLACE(REPLACE(

									[dbo].[udf_CF_NOVARTIS_JADENU_GetRestrictionCode](                  /* BT-NTDT specific */
										[PA_Required],
										[Quantity_Limitation],
										[TIO_Specific_Iron_Chelating_Agent_Failure]
									 )

								,CHAR(13),' '),CHAR(10),' '),

			[RESTRICTION_DETAIL_TEXT] = REPLACE(REPLACE(

											[dbo].[udf_CF_NOVARTIS_JADENU_GetRestrictionDetailText](     /* BT-NTDT specific */
												[TIO_Specific_Iron_Chelating_Agent_Failure],
												[TIO_Age_Restrictions],
												[TIO_Blood_Transfusion_Minimum_Description],
												[TIO_Serum_Ferritin_Levels_Description],
												[Lab_Requirements],
												[Proof_of_Effectiveness_Required_Summary],
												[Specialist_Appr]
											)

										,CHAR(13),' '),CHAR(10),' '),

			[PROD_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') ))

		FROM @StateRaw



/*
	-- Fill State NTDT
	INSERT INTO @OutputUnion
		SELECT
			[STATE_CD] = LTRIM(RTRIM(REPLACE(REPLACE([State],CHAR(13),' '),CHAR(10),' ') )),                                 /* State-PBM specific */
			[STATE_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([StateName],CHAR(13),' '),CHAR(10),' ') )),                           /* State-PBM specific */
			[PLAN_ID] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_RANK] = '',
			[LIVES] = LTRIM(RTRIM(REPLACE(REPLACE([Pharmacy],CHAR(13),' '),CHAR(10),' ') )),                                 /* State-PBM specific */

			[PLAN_TYPE] =	REPLACE(REPLACE(

								ISNULL( CASE LTRIM(RTRIM([Segment]))                /* State-PBM specific */
									WHEN 'Managed Medicaid' THEN 'Medicaid'
									WHEN 'State Medicaid' THEN 'Medicaid'
									WHEN 'Managed Medicare' THEN 'Medicare'
									WHEN 'Commercial MCO' THEN 'Commercial'
								ELSE 
									LTRIM(RTRIM([Segment]))
								END,
								'')

							,CHAR(13),' '),CHAR(10),' '),

			[TIER] =	REPLACE(REPLACE(

							ISNULL( CASE LTRIM(RTRIM([Novartis_Healthplan_Management])) 
								WHEN 'To PI or Better' THEN 'Covered'
								WHEN 'Non-Preferred' THEN 'Covered'
								WHEN 'Step Edit - Preferred' THEN 'Covered'
							ELSE 
								LTRIM(RTRIM([Novartis_Healthplan_Management]))
							END,
							'')

						,CHAR(13),' '),CHAR(10),' '),

			[TIER_NUMBER] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[INDICATION] = 'NTDT',                                                        /* BT-NTDT specific */
			[DW_INS_DT] = LTRIM(RTRIM(REPLACE(REPLACE([dbo].[udf_ConvertToDate]([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[RETAIL_COPAY_MIN] = '',
			[RETAIL_COPAY_MAX] = '',
			[MO_COPAY_MIN] = '',
			[MO_COPAY_MAX] = '',

			[PA_URL] =	REPLACE(REPLACE(
			
							ISNULL( CASE LTRIM(RTRIM([URL_to_PA_Form])) 
										WHEN 'Data Not Available' THEN LTRIM(RTRIM([General_PA_Form]))
									ELSE 
										LTRIM(RTRIM([URL_to_PA_Form]))
									END,
									'')

						,CHAR(13),' '),CHAR(10),' '),

			[RESTRICTION_CODE] =	REPLACE(REPLACE(

										[dbo].[udf_CF_NOVARTIS_JADENU_GetRestrictionCode](                  /* BT-NTDT specific */
											[PA_Required],
											[Quantity_Limitation],
											[NTDT_Specific_Iron_Chelating_Agent_Failure]
										 )

									,CHAR(13),' '),CHAR(10),' '),

			[RESTRICTION_DETAIL_TEXT] = REPLACE(REPLACE(

											[dbo].[udf_CF_NOVARTIS_JADENU_GetRestrictionDetailText](     /* BT-NTDT specific */
												[NTDT_Specific_Iron_Chelating_Agent_Failure],
												[NTDT_IO_Age_Restrictions],
												[NTDT_Liver_Iron_Concentration_Required_Description],
												[NTDT_Serum_Ferritin_Levels_Description],
												[Lab_Requirements],
												[Proof_of_Effectiveness_Required_Summary],
												[Specialist_Appr]
											)

										,CHAR(13),' '),CHAR(10),' '),

			[PROD_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') ))

		FROM @StateRaw
*/




	-- Fill PBM BT
	INSERT INTO @PbmBT
		SELECT
			[STATE_CD] = '',                                                    /* State-PBM specific */
			[STATE_NAME] = '',                                                  /* State-PBM specific */
			[PLAN_ID] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_NAME] = LTRIM(RTRIM([Plan_Name])),
			[PLAN_RANK] = '',
			[LIVES] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Self_Insured_Lives],CHAR(13),' '),CHAR(10),' ') )),                   /* State-PBM specific */

			[PLAN_TYPE] =	REPLACE(REPLACE(

								ISNULL( CASE LTRIM(RTRIM([MCoid]))                  /* State-PBM specific */
									WHEN '90028000'  THEN 'Medicaid'
									WHEN '900252005' THEN 'Medicaid'
									WHEN '900332000' THEN 'Medicaid'
								ELSE 
									'Commercial'
								END,
								'')

							,CHAR(13),' '),CHAR(10),' '),

			[TIER] =	REPLACE(REPLACE(

							ISNULL( CASE LTRIM(RTRIM([Novartis_Healthplan_Management])) 
								WHEN 'To PI or Better'       THEN 'Covered'
								WHEN 'Non-Preferred'         THEN 'Covered'
								WHEN 'Step Edit - Preferred' THEN 'Covered'
							ELSE 
								LTRIM(RTRIM([Novartis_Healthplan_Management]))
							END,
							'')

						,CHAR(13),' '),CHAR(10),' '),

			[TIER_NUMBER] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[INDICATION] = 'BT',                                                        /* BT-NTDT specific */
			[DW_INS_DT] = LTRIM(RTRIM(REPLACE(REPLACE([dbo].[udf_ConvertToDate]([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[RETAIL_COPAY_MIN] = '',
			[RETAIL_COPAY_MAX] = '',
			[MO_COPAY_MIN] = '',
			[MO_COPAY_MAX] = '',

			[PA_URL] =	REPLACE(REPLACE(

							ISNULL( CASE LTRIM(RTRIM([URL_to_PA_Form])) 
										WHEN 'Data Not Available' THEN LTRIM(RTRIM([General_PA_Form]))
									ELSE 
										LTRIM(RTRIM([URL_to_PA_Form]))
									END,
									'')

						,CHAR(13),' '),CHAR(10),' '),

			[RESTRICTION_CODE] = REPLACE(REPLACE(

									[dbo].[udf_CF_NOVARTIS_JADENU_GetRestrictionCode](                  /* BT-NTDT specific */
										[PA_Required],
										[Quantity_Limitation],
										[TIO_Specific_Iron_Chelating_Agent_Failure]
									 )

								,CHAR(13),' '),CHAR(10),' '),

			[RESTRICTION_DETAIL_TEXT] = REPLACE(REPLACE(

											[dbo].[udf_CF_NOVARTIS_JADENU_GetRestrictionDetailText](     /* BT-NTDT specific */
												[TIO_Specific_Iron_Chelating_Agent_Failure],
												[TIO_Age_Restrictions],
												[TIO_Blood_Transfusion_Minimum_Description],
												[TIO_Serum_Ferritin_Levels_Description],
												[Lab_Requirements],
												[Proof_of_Effectiveness_Required_Summary],
												[Specialist_Appr]
											)

										,CHAR(13),' '),CHAR(10),' '),

			[PROD_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') ))
		FROM @PbmRaw

/*
	-- Fill PBM NTDT
	INSERT INTO @PbmNTDT
		SELECT
			[STATE_CD] = '',                                                    /* State-PBM specific */
			[STATE_NAME] = '',                                                  /* State-PBM specific */
			[PLAN_ID] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_RANK] = '',
			[LIVES] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Self_Insured_Lives],CHAR(13),' '),CHAR(10),' ') )),                   /* State-PBM specific */

			[PLAN_TYPE] =	REPLACE(REPLACE(

								ISNULL( CASE LTRIM(RTRIM([MCoid]))                  /* State-PBM specific */
									WHEN '90028000'  THEN 'Medicaid'
									WHEN '900252005' THEN 'Medicaid'
									WHEN '900332000' THEN 'Medicaid'
								ELSE 
									'Commercial'
								END,
								'')

							,CHAR(13),' '),CHAR(10),' '),

			[TIER] =	REPLACE(REPLACE(

							ISNULL( CASE LTRIM(RTRIM([Novartis_Healthplan_Management])) 
								WHEN 'To PI or Better'       THEN 'Covered'
								WHEN 'Non-Preferred'         THEN 'Covered'
								WHEN 'Step Edit - Preferred' THEN 'Covered'
							ELSE 
								LTRIM(RTRIM([Novartis_Healthplan_Management]))
							END,
							'')

						,CHAR(13),' '),CHAR(10),' '),

			[TIER_NUMBER] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[INDICATION] = 'NTDT',                                                        /* BT-NTDT specific */
			[DW_INS_DT] = LTRIM(RTRIM(REPLACE(REPLACE([dbo].[udf_ConvertToDate]([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[RETAIL_COPAY_MIN] = '',
			[RETAIL_COPAY_MAX] = '',
			[MO_COPAY_MIN] = '',
			[MO_COPAY_MAX] = '',

			[PA_URL] =	REPLACE(REPLACE(

							ISNULL( CASE LTRIM(RTRIM([URL_to_PA_Form])) 
										WHEN 'Data Not Available' THEN LTRIM(RTRIM([General_PA_Form]))
									ELSE 
										LTRIM(RTRIM([URL_to_PA_Form]))
									END,
									'')

						,CHAR(13),' '),CHAR(10),' '),

			[RESTRICTION_CODE] = REPLACE(REPLACE(

									[dbo].[udf_CF_NOVARTIS_JADENU_GetRestrictionCode](                  /* BT-NTDT specific */
										[PA_Required],
										[Quantity_Limitation],
										[NTDT_Specific_Iron_Chelating_Agent_Failure]
									 )

								,CHAR(13),' '),CHAR(10),' '),

			[RESTRICTION_DETAIL_TEXT] = REPLACE(REPLACE(

											[dbo].[udf_CF_NOVARTIS_JADENU_GetRestrictionDetailText](     /* BT-NTDT specific */
												[NTDT_Specific_Iron_Chelating_Agent_Failure],
												[NTDT_IO_Age_Restrictions],
												[NTDT_Liver_Iron_Concentration_Required_Description],
												[NTDT_Serum_Ferritin_Levels_Description],
												[Lab_Requirements],
												[Proof_of_Effectiveness_Required_Summary],
												[Specialist_Appr]
											)

										,CHAR(13),' '),CHAR(10),' '),

			[PROD_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') ))
		FROM @PbmRaw
*/

																						--SELECT * FROM @PbmBT
																						--SELECT * FROM @PbmNTDT


	------------------------ Fill States ------------------------
	INSERT INTO @States
		SELECT DISTINCT
			[State] = [State],
			[StateName] = [StateName]
		FROM @StateRaw
		ORDER BY [State]
	
																						--SELECT * FROM @States


	------------------------ Fill PMBs from a loop into the Union output table ------------------------

	-- Cursor Loop to add PBM's for each state
	DECLARE @LoopCount INT = 1
	DECLARE @Count INT
	DECLARE @SINGLE_STATE VARCHAR(155)	 
	DECLARE @SINGLE_STATE_NAME VARCHAR(155)	 

	SELECT @Count = MAX(ID) FROM @States 

	WHILE @LoopCount <= @Count
		BEGIN
			SELECT @SINGLE_STATE = State FROM @States  WHERE ID = @LoopCount
			SELECT @SINGLE_STATE_NAME = StateName FROM @States  WHERE ID = @LoopCount

			INSERT INTO @OutputUnion
			SELECT
				[STATE_CD] = LTRIM(RTRIM(REPLACE(REPLACE(@SINGLE_STATE,CHAR(13),' '),CHAR(10),' ') )),
				[STATE_NAME] = LTRIM(RTRIM(REPLACE(REPLACE(@SINGLE_STATE_NAME,CHAR(13),' '),CHAR(10),' ') )),
				[PLAN_ID] = [PLAN_ID],
				[PLAN_NAME] = [PLAN_NAME],
				[PLAN_RANK] = [PLAN_RANK],
				[LIVES] = [LIVES],
				[PLAN_TYPE] = [PLAN_TYPE],
				[TIER] = [TIER],
				[TIER_NUMBER] = [TIER_NUMBER],
				[INDICATION] = [INDICATION],
				[DW_INS_DT] = [DW_INS_DT],
				[RETAIL_COPAY_MIN] = [RETAIL_COPAY_MIN],
				[RETAIL_COPAY_MAX] = [RETAIL_COPAY_MAX],
				[MO_COPAY_MIN] = [MO_COPAY_MIN],
				[MO_COPAY_MAX] = [MO_COPAY_MAX],
				[PA_URL] = [PA_URL],
				[RESTRICTION_CODE] = [RESTRICTION_CODE],
				[RESTRICTION_DETAIL_TEXT] = [RESTRICTION_DETAIL_TEXT],
				[PROD_NAME] = [PROD_NAME]
			FROM @PbmBT
/*
			INSERT INTO @OutputUnion
			SELECT
				[STATE_CD] = LTRIM(RTRIM(REPLACE(REPLACE(@SINGLE_STATE,CHAR(13),' '),CHAR(10),' ') )),
				[STATE_NAME] = LTRIM(RTRIM(REPLACE(REPLACE(@SINGLE_STATE_NAME,CHAR(13),' '),CHAR(10),' ') )),
				[PLAN_ID] = [PLAN_ID],
				[PLAN_NAME] = [PLAN_NAME],
				[PLAN_RANK] = [PLAN_RANK],
				[LIVES] = [LIVES],
				[PLAN_TYPE] = [PLAN_TYPE],
				[TIER] = [TIER],
				[TIER_NUMBER] = [TIER_NUMBER],
				[INDICATION] = [INDICATION],
				[DW_INS_DT] = [DW_INS_DT],
				[RETAIL_COPAY_MIN] = [RETAIL_COPAY_MIN],
				[RETAIL_COPAY_MAX] = [RETAIL_COPAY_MAX],
				[MO_COPAY_MIN] = [MO_COPAY_MIN],
				[MO_COPAY_MAX] = [MO_COPAY_MAX],
				[PA_URL] = [PA_URL],
				[RESTRICTION_CODE] = [RESTRICTION_CODE],
				[RESTRICTION_DETAIL_TEXT] = [RESTRICTION_DETAIL_TEXT],
				[PROD_NAME] = [PROD_NAME]
			FROM @PbmNTDT
*/
			SET @LoopCount = @LoopCount + 1;
		END		 



	SELECT * FROM @OutputUnion 
END



GO


