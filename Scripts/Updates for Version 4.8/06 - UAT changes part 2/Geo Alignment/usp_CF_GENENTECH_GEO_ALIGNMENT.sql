--USE [CWP_3.0]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_GENENTECH_GEO_ALIGNMENT]    Script Date: 10/10/2018 3:49:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- EXEC usp_CF_GENENTECH_GEO_ALIGNMENT
ALTER PROCEDURE [dbo].[usp_CF_GENENTECH_GEO_ALIGNMENT]
AS
BEGIN

	SELECT DISTINCT
            m.mcoid AS Mcoid,
            s.State,
            CONVERT(VARCHAR, CURRENT_TIMESTAMP, 101) AS Data_Extraction_Date
    FROM [MCO].Production.MCO m
    INNER JOIN [MCO].Production.Lives l ON l.mcoId = m.mcoId
    INNER JOIN [MCO].dbo.State s ON s.StateCode = l.GeoCode
    WHERE l.GeoTypeID = 2
    AND l.Source='hlis' AND l.lives>100
    ORDER by 1,2

END

GO


