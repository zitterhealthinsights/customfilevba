--USE [CWP_3.0]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_LILLY_STATE_BC]    Script Date: 10/11/2018 5:30:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- EXEC usp_CF_LILLY_STATE_BC 4026, 'BC'
ALTER PROCEDURE [dbo].[usp_CF_LILLY_STATE_BC]
	@Userid INT,
	@IndOrDrug VARCHAR(50)
AS
BEGIN
		
		DECLARE @Get_CF_LILLY_STATE_BC as CF_LILLY_STATE_BC
		
		DECLARE @selectedFields as varchar(max)
		SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_LILLY_STATE_BC','');
		
		INSERT INTO @Get_CF_LILLY_STATE_BC
		EXEC dbo.usp_Get_Plans
			@UserID = @Userid, 
			@IndicationorDrug = @IndOrDrug,
			@SelectFields = @selectedFields,
			@Level = 'State'
				
		SELECT
			[MCoid] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([TZG Payer Name],CHAR(13),' '),CHAR(10),' ') )),
			[PlanName] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[StateName] = LTRIM(RTRIM(REPLACE(REPLACE([StateName],CHAR(13),' '),CHAR(10),' ') )),
			[State] = LTRIM(RTRIM(REPLACE(REPLACE([State],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[MarketSharePercentage] = LTRIM(RTRIM(REPLACE(REPLACE([MarketSharePercentage],CHAR(13),' '),CHAR(10),' ') )),
			[PlanLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanLivesInState],CHAR(13),' '),CHAR(10),' ') )),
			[Self Insured Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Commercial ASO],CHAR(13),' '),CHAR(10),' ') )),
			[Fully Insured Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Commercial Fully Insured],CHAR(13),' '),CHAR(10),' ') )),
			[Medical Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Medical],CHAR(13),' '),CHAR(10),' ') )),
			[Pharmacy Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Pharmacy],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[On_Formulary] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
			[Lilly_HealthPlan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Lilly_HealthPlan_Management],CHAR(13),' '),CHAR(10),' ') )),
			[Uniform_Status_Language_Lilly_Restrictions] = LTRIM(RTRIM(REPLACE(REPLACE([Uniform_Status_Language_Lilly_Restrictions],CHAR(13),' '),CHAR(10),' ') )),
			[Prior_Authorization_Lilly_Restrictions] = LTRIM(RTRIM(REPLACE(REPLACE([Prior_Authorization_Lilly_Restrictions],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limit_Lilly_Restrictions] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limit_Lilly_Restrictions],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Edit_Lilly_Restrictions] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Edit_Lilly_Restrictions],CHAR(13),' '),CHAR(10),' ') )),
			[Age_Edit_Lilly_Restrictions] = LTRIM(RTRIM(REPLACE(REPLACE([Age_Edit_Lilly_Restrictions],CHAR(13),' '),CHAR(10),' ') )),
			[Preferred_Agent_Lilly_Restrictions] = LTRIM(RTRIM(REPLACE(REPLACE([Preferred_Agent_Lilly_Restrictions],CHAR(13),' '),CHAR(10),' ') )),
			[Medical_Benefit_Lilly_Restrictions] = LTRIM(RTRIM(REPLACE(REPLACE([Medical_Benefit_Lilly_Restrictions],CHAR(13),' '),CHAR(10),' ') )),
			[Specialty_Pharmacy_Lilly_Restrictions] = LTRIM(RTRIM(REPLACE(REPLACE([Specialty_Pharmacy_Lilly_Restrictions],CHAR(13),' '),CHAR(10),' ') )),
			[Age_Limit] = LTRIM(RTRIM(REPLACE(REPLACE([Age_Limit],CHAR(13),' '),CHAR(10),' ') )),
			[Exception_Form_URL] = LTRIM(RTRIM(REPLACE(REPLACE([Exception_Form_URL],CHAR(13),' '),CHAR(10),' ') )),
			[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[General_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([General_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[URL_To_Draft_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_To_Draft_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Management] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Management],CHAR(13),' '),CHAR(10),' ') )),
			[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
			[Compendium_Referenced] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Referenced],CHAR(13),' '),CHAR(10),' ') )),
			[Evidence_Req_for_Off-Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Evidence_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
			[Compendium_Req_for_Off-Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
			[Compendium_Evidence_Lvl_Req_for_Off-Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Evidence_Lvl_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
			[PR_Journal_Articles_Req_for_Off-Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([PR_Journal_Articles_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
			[Clinical_Pathway_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Clinical_Pathway_Utilized],CHAR(13),' '),CHAR(10),' ') )),
			[OffLabel_Use_Limitations] = LTRIM(RTRIM(REPLACE(REPLACE([OffLabel_Use_Limitations],CHAR(13),' '),CHAR(10),' ') )),
			[Off_Label_Use_Count] = LTRIM(RTRIM(REPLACE(REPLACE([Off_Label_Use_Count],CHAR(13),' '),CHAR(10),' ') )),
			[Off_Label_Use_Indications] = LTRIM(RTRIM(REPLACE(REPLACE([Off_Label_Use_Indications],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Steps_Before_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Steps_Before_Approval],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Notes] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_PA_Criteria_details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
			[Limitation_on_Cancer_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Cancer_Type],CHAR(13),' '),CHAR(10),' ') )),
			[Limitation_on_Cancer_Type_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Cancer_Type_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Place_in_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Therapy],CHAR(13),' '),CHAR(10),' ') )),
			[Place_in_Therapy_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Therapy_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Place_in_Maintenance_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Maintenance_Therapy],CHAR(13),' '),CHAR(10),' ') )),
			[Place_in_Maintenance_Therapy_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Maintenance_Therapy_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Mandates_on_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
			[Mandates_on_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_Combination_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Combination_Therapy],CHAR(13),' '),CHAR(10),' ') )),
			[Prohibits_Specified_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
			[Prohibits_Specified_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Conventional_Chemo_Courses_Ct] = LTRIM(RTRIM(REPLACE(REPLACE([Conventional_Chemo_Courses_Ct],CHAR(13),' '),CHAR(10),' ') )),
			[Conventional_Chemo_Len] = LTRIM(RTRIM(REPLACE(REPLACE([Conventional_Chemo_Len],CHAR(13),' '),CHAR(10),' ') )),
			[Conventional_Chemo_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Conventional_Chemo_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Targeted_Therapies_Courses_Ct] = LTRIM(RTRIM(REPLACE(REPLACE([Targeted_Therapies_Courses_Ct],CHAR(13),' '),CHAR(10),' ') )),
			[Targeted_Therapies_Len] = LTRIM(RTRIM(REPLACE(REPLACE([Targeted_Therapies_Len],CHAR(13),' '),CHAR(10),' ') )),
			[Targeted_Therapies_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Targeted_Therapies_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[SpecifiC_Targeted_Failure] = LTRIM(RTRIM(REPLACE(REPLACE([SpecifiC_Targeted_Failure],CHAR(13),' '),CHAR(10),' ') )),
			[Prior_Hormone_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Prior_Hormone_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Prior_Hormone_Therapy_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Prior_Hormone_Therapy_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Neoadjuvant_Use_Covered] = LTRIM(RTRIM(REPLACE(REPLACE([Neoadjuvant_Use_Covered],CHAR(13),' '),CHAR(10),' ') )),
			[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy_Criteria] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_Criteria],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy_URL] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_URL],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Req_for_Continued_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Req_for_Continued_Appr],CHAR(13),' '),CHAR(10),' ') )),
			[Limitation_on_Visceral_Status] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Visceral_Status],CHAR(13),' '),CHAR(10),' ') )),
			[Node_Status_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Node_Status_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Prior_Chemo_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Prior_Chemo_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Hormone_Receptor_Status_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Hormone_Receptor_Status_Req],CHAR(13),' '),CHAR(10),' ') )),
			[MUGA_Scan_Req] = LTRIM(RTRIM(REPLACE(REPLACE([MUGA_Scan_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Nbr_of_MUGA_Scans_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_of_MUGA_Scans_Req],CHAR(13),' '),CHAR(10),' ') )),
			[HER2_Testing_Req] = LTRIM(RTRIM(REPLACE(REPLACE([HER2_Testing_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Type_of_HER2_Test] = LTRIM(RTRIM(REPLACE(REPLACE([Type_of_HER2_Test],CHAR(13),' '),CHAR(10),' ') )),
			[IHC_Numerical_Limit] = LTRIM(RTRIM(REPLACE(REPLACE([IHC_Numerical_Limit],CHAR(13),' '),CHAR(10),' ') )),
			[FISH_Numerical_Limit] = LTRIM(RTRIM(REPLACE(REPLACE([FISH_Numerical_Limit],CHAR(13),' '),CHAR(10),' ') )),
			[Limitation_on_Recycling] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Recycling],CHAR(13),' '),CHAR(10),' ') )),
			[Competitor_Drugs] = LTRIM(RTRIM(REPLACE(REPLACE([Competitor_Drugs],CHAR(13),' '),CHAR(10),' ') )),
			[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[Split_Fill_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Split_Fill_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Specialist_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Appr],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
			[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
			[Name_Of_Specialty_Drug_Distributer_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
			[Name_Of_Specialty_Drug_Distributer_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Policy_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Policy_Utilized],CHAR(13),' '),CHAR(10),' ') )),
			[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
			[Change_To_Entry] = LTRIM(RTRIM(REPLACE(REPLACE([Change_To_Entry],CHAR(13),' '),CHAR(10),' ') )),
			[Reason_for_Change] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change],CHAR(13),' '),CHAR(10),' ') )),
			[Reason_for_Change_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change_Details],CHAR(13),' '),CHAR(10),' ') )),
			[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[No_Policy_Date_Found] = LTRIM(RTRIM(REPLACE(REPLACE([No_Policy_Date_Found],CHAR(13),' '),CHAR(10),' ') )),
			[Entry_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
			[Note_2] = LTRIM(RTRIM(REPLACE(REPLACE([Note_2],CHAR(13),' '),CHAR(10),' ') )),
			[Note_3] = LTRIM(RTRIM(REPLACE(REPLACE([Note_3],CHAR(13),' '),CHAR(10),' ') ))/*,
			[StepTherapy_Footnote] = LTRIM(RTRIM(REPLACE(REPLACE([StepTherapy_Footnote],CHAR(13),' '),CHAR(10),' ') )),
			[preferred_status] = LTRIM(RTRIM(REPLACE(REPLACE([preferred_status],CHAR(13),' '),CHAR(10),' ') ))*/

		FROM @Get_CF_LILLY_STATE_BC

END


GO


