--USE [MCMM_Prod]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_GENENTECH_BRIDGE_FILE]    Script Date: 9/3/2018 7:22:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- EXEC usp_CF_GENENTECH_BRIDGE_FILE '7/12/2018', '8/13/2018'
CREATE PROCEDURE [dbo].[usp_CF_GENENTECH_BRIDGE_FILE]
	@StartDate as Date,
	@EndDate as Date
AS
BEGIN
	
	-- Create temp table for holding semi-results, ID will be needed only for transformations
	DECLARE @Results AS TABLE (
		ID INT IDENTITY(1,1) NOT NULL,
		[Survey Completed] VARCHAR(10),     -- date mm/dd/yyyy
		[MCOid] VARCHAR(255),               -- empty for now
		[PayerName] VARCHAR(300),           -- from view [TA - completed surveys PLANS SQL]
		[PlanName] VARCHAR(300),            -- from view [TA - completed surveys PLANS SQL]
		[Segment] VARCHAR(255),             -- empty for now
		[Last P&T] VARCHAR(10),             -- mm/yyyy
		[Next P&T] VARCHAR(10),             -- mm/yyyy
		[Therapeutic Area] VARCHAR(128),    -- from view [TA - completed surveys PLANS SQL]
		[DATA_EXTRACTION_DATE] VARCHAR(10)  -- date mm/dd/yyyy
	)

	-- Fill results from a view:
	-- 1. Filter by start date and end dates. Filter by therap areas
	-- 2. Last P&T, Next P&T: format as dash if NULL, otherwise format as MM/YYYY
	-- 3. Order data
	INSERT INTO @Results
		SELECT
			[Survey Completed] = CONVERT(char(10), CompleteDate, 101), -- date mm/dd/yyyy
			MCOid = '',
			PayerName,
			PlanName,
			Segment = '',
			[Last P&T] =	CASE -- MM/YYYY or dash
								WHEN (RecentMeetingMonth IS NULL) OR (RecentMeetingYear IS NULL) THEN
									'�'
								ELSE
									RIGHT('00' + RecentMeetingMonth, 2) + '/' + RecentMeetingYear
							END,
			[Next P&T] =	CASE -- MM/YYYY or dash
								WHEN (NextMeetingMonth IS NULL) OR (NextMeetingYear IS NULL) THEN
									'�'
								ELSE
									RIGHT('00' + NextMeetingMonth, 2) + '/' + NextMeetingYear
							END,
			[Therapeutic Area] = TherapArea,
			DATA_EXTRACTION_DATE = CONVERT(char(10), GetDate(), 101) -- date mm/dd/yyyy
		FROM [dbo].[TA - completed surveys PLANS SQL]
		WHERE (CompleteDate >= @StartDate) AND (CompleteDate <= @EndDate)
		AND TherapAreaID IN (1390, 1392, 1407, 14, 1521, 41, 45)
		ORDER BY TherapArea ASC, PayerName ASC, PlanName ASC, CompleteDate DESC


	-- Now, if some of the rows have the same: PayerName, PlanName, [Therapeutic Area]
	-- then leave only the 1st row from that group
	DELETE FROM @Results
	WHERE ID NOT IN 
	(
		SELECT MIN(ID) FROM @Results
		GROUP BY PayerName, PlanName, [Therapeutic Area]
	)

	-- We should not show the exact survey date. Instead, we display the first of the month in which the information was captured
	UPDATE @Results
		SET [Survey Completed] = CONVERT(char(10), DATEADD(DAY, 1, EOMONTH([Survey Completed], -1)), 101)

	-- Put a dash for [Last P&T] older than two years
	UPDATE @Results
		SET [Last P&T] =	CASE
								WHEN [Last P&T] = '�' THEN
									'�'
								ELSE
									CASE 
										WHEN CONVERT(date, '01/' + [Last P&T], 103) < DATEADD(YEAR, -2, GetDate()) THEN
											'�'
										ELSE
											[Last P&T]
									END
							END
	
	-- Select rthe esult, without ID column
	SELECT
		[Survey Completed],
		[MCOid],
		[PayerName],
		[PlanName],
		[Segment],
		[Last P&T],
		[Next P&T],
		[Therapeutic Area],
		[DATA_EXTRACTION_DATE]
	FROM @Results

END




GO


