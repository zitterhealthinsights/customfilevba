--USE [MCMM_Prod]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_CustomFile]    Script Date: 9/3/2018 9:44:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






/*

**** Genentech ****
	-- 'Bridge File'
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 32, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
*/
 

CREATE PROCEDURE [dbo].[usp_CF_CustomFile]  
	@ClientID INT,
	@CategoryID INT,
	@IndicationArray VARCHAR(255),	
	@DrugArray VARCHAR(255),
	@ScheduleTypeID INT
AS
BEGIN

DECLARE @PACKING_SLIP TABLE ([TABNAME] VARCHAR(MAX))

	IF  @ClientID = 9 -- Genentech
	BEGIN
		--- Get the Admin User ID
        --SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;

		IF @CategoryID = 32 -- Bridge File
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('P&T Dates')
				SELECT * FROM @PACKING_SLIP;

				DECLARE @StartDate AS Date
				DECLARE @EndDate AS Date
				SET @EndDate = DATEADD(DAY, -1, GetDate())
				SET @StartDate = DATEADD(MONTH, -1, @EndDate)
				EXEC usp_CF_GENENTECH_BRIDGE_FILE @StartDate, @EndDate
			END
	END

END

GO


