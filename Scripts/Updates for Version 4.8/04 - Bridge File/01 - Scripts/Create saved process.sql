--USE [CWP_3.0]

EXEC usp_CF_SaveProcess
    @ProcessID = 0, -- Add, not Update
    @ProcessName = 'Bridge File',
    @ScheduleTypeID = 2, -- Monthly
    @LastRunUserName = 'Milan', 
    @CategoryID = 32, -- Bridge File
    @ClientID = 9, -- Genentech
    @ExportFilePath = 'C:\Temp\'
GO
EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 41, -- the one we just created
    @ClientID = 9, -- Genentech
    @StepDescription = 'Bridge File',
    @IndicationNames = 'Asthma', -- just a placeholder
    @DrugNames = 'Cinqair' -- just a placeholder
GO

SELECT * FROM Process ORDER BY ProcessID DESC
SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC