--USE [CWP_3.0]
--GO
/****** Object:  StoredProcedure [dbo].[usp_CF_SANDOZ_PBM_NEU]    Script Date: 10/22/2018 10:50:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC usp_CF_SANDOZ_PBM_NEU 3236, 'NEU'
ALTER PROCEDURE [dbo].[usp_CF_SANDOZ_PBM_NEU]
	@Userid INT,
	@IndOrDrug varchar(50)
AS
BEGIN

		DECLARE @Get_PbmNEU as CF_SANDOZ_PBM_NEU
		DECLARE @selectedFields as varchar(max)
		SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_SANDOZ_PBM_NEU','');
		Insert into @Get_PbmNEU
		EXEC	dbo.usp_Get_Plans
			@UserID = @Userid, 
			@IndicationorDrug = @IndOrDrug,
			@SelectFields = @selectedFields,
			@isPBMOnly = 1
		SELECT
			[PBM_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_TotalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Total_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_ControlledFullyInsuredPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Controlled_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_ClaimsLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Claims_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_SelfInsuredPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Self_Insured_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[Sandoz_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
			[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Management] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Management],CHAR(13),' '),CHAR(10),' ') )),
			[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Notes] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
			[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Nbr_of_Conventional_Chemo_Courses] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_of_Conventional_Chemo_Courses],CHAR(13),' '),CHAR(10),' ') )),
			[Conventional_Chemo_Therapy_Trial_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Conventional_Chemo_Therapy_Trial_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Conventional_Chemo_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Conventional_Chemo_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Transplantation_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Transplantation_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Transplantation_Required_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Transplantation_Required_Desc],CHAR(13),' '),CHAR(10),' ') )),
			--[Transplantation_Required_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Transplantation_Required_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Febrile_Neutropenia_is_associated_with_chemotherapy] = LTRIM(RTRIM(REPLACE(REPLACE([Febrile_Neutropenia_is_associated_with_chemotherapy],CHAR(13),' '),CHAR(10),' ') )),
			[Febrile_Neutropenia_is_associated_with_chemotherapy_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Febrile_Neutropenia_is_associated_with_chemotherapy_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Age_Limit] = LTRIM(RTRIM(REPLACE(REPLACE([Age_Limit],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Risk_Factor_Stated] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Risk_Factor_Stated],CHAR(13),' '),CHAR(10),' ') )),
			[Risk_Factor_Stated_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Risk_Factor_Stated_Details],CHAR(13),' '),CHAR(10),' ') )),
			[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Primary_Prophylaxis_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Primary_Prophylaxis_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Secondary_Prophylaxis_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Secondary_Prophylaxis_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			--[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Req_for_Continued_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Req_for_Continued_Appr],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy_Criteria] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_Criteria],CHAR(13),' '),CHAR(10),' ') )),
			[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[Specialist_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Appr],CHAR(13),' '),CHAR(10),' ') )),
			[Split_Fill_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Split_Fill_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[AML_Required] = LTRIM(RTRIM(REPLACE(REPLACE([AML_Required],CHAR(13),' '),CHAR(10),' ') )),
			[AML_Required_Description] = LTRIM(RTRIM(REPLACE(REPLACE([AML_Required_Description],CHAR(13),' '),CHAR(10),' ') )),
			--[AML_Required_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([AML_Required_Opr],CHAR(13),' '),CHAR(10),' ') )),
			--[Febrile_Neutropenia_associated_with_chemotherapy_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Febrile_Neutropenia_associated_with_chemotherapy_Opr],CHAR(13),' '),CHAR(10),' ') )),
			--[GCSF_Agent_Before_Neupogen_and/or_Neulasta_Required] = LTRIM(RTRIM(REPLACE(REPLACE([GCSF_Agent_Before_Neupogen_and/or_Neulasta_Required],CHAR(13),' '),CHAR(10),' ') )),
			--[GCSF_Agent_Before_Neupogen_and/or_Neulasta_Required_Description] = LTRIM(RTRIM(REPLACE(REPLACE([GCSF_Agent_Before_Neupogen_and/or_Neulasta_Required_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Hematopoietic_Syndrome_of_ARS_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Hematopoietic_Syndrome_of_ARS_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Hematopoietic_Syndrome_of_ARS_Required_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Hematopoietic_Syndrome_of_ARS_Required_Description],CHAR(13),' '),CHAR(10),' ') )),
			--[Hematopoietic_Syndrome_of_ARS_Required_Description_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Hematopoietic_Syndrome_of_ARS_Required_Description_Opr],CHAR(13),' '),CHAR(10),' ') )),
			--[Long_Acting_GCSF_Failure_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Long_Acting_GCSF_Failure_Required],CHAR(13),' '),CHAR(10),' ') )),
			--[Nbr_of_GCSF_Agent_Before_Neupogen_and/or_Neulasta_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_of_GCSF_Agent_Before_Neupogen_and/or_Neulasta_Required],CHAR(13),' '),CHAR(10),' ') )),
			--[Nbr_of_Long_Acting_GCSF_Failures_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_of_Long_Acting_GCSF_Failures_Required],CHAR(13),' '),CHAR(10),' ') )),
			--[Nbr_of_Short_Acting_GCSF_Failures_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_of_Short_Acting_GCSF_Failures_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Progenitor_Cell_Collection_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Progenitor_Cell_Collection_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Progenitor_Cell_Collection_Required_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Progenitor_Cell_Collection_Required_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Differentiation_Scale] = LTRIM(RTRIM(REPLACE(REPLACE([Differentiation_Scale],CHAR(13),' '),CHAR(10),' ') )),
			[Label_Indications_Not_Covered] = LTRIM(RTRIM(REPLACE(REPLACE([Label_Indications_Not_Covered],CHAR(13),' '),CHAR(10),' ') )),
			--[Progenitor_Cell_Collection_Required_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Progenitor_Cell_Collection_Required_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Q_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([Q_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[Severe_Chronic_Neutropenia_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Severe_Chronic_Neutropenia_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Severe_Chronic_Neutropenia_Required_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Severe_Chronic_Neutropenia_Required_Description],CHAR(13),' '),CHAR(10),' ') )),
			--[Severe_Chronic_Neutropenia_Required_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Severe_Chronic_Neutropenia_Required_Opr],CHAR(13),' '),CHAR(10),' ') )),
			--[Short_Acting_GCSF_Failure_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Short_Acting_GCSF_Failure_Required],CHAR(13),' '),CHAR(10),' ') )),
			--[Short_Acting_GCSF_Required_Prior_to_Long_Acting_GCSF] = LTRIM(RTRIM(REPLACE(REPLACE([Short_Acting_GCSF_Required_Prior_to_Long_Acting_GCSF],CHAR(13),' '),CHAR(10),' ') )),
			[On_Formulary] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
			[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
			[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[No_Policy_Date_Found] = LTRIM(RTRIM(REPLACE(REPLACE([No_Policy_Date_Found],CHAR(13),' '),CHAR(10),' ') )),
			[Entry_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
			[Note_2] = LTRIM(RTRIM(REPLACE(REPLACE([Note_2],CHAR(13),' '),CHAR(10),' ') )),
			[Note_3] = LTRIM(RTRIM(REPLACE(REPLACE([Note_3],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
			[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
			[Name_Of_Specialty_Drug_Distributer_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
			[Name_Of_Specialty_Drug_Distributer_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
			[Reason_for_Change] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change],CHAR(13),' '),CHAR(10),' ') )),
			[Reason_for_Change_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change_Details],CHAR(13),' '),CHAR(10),' ') )),
			--[Original_Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Original_Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Policy_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Policy_Utilized],CHAR(13),' '),CHAR(10),' ') )),
			[Change_To_Entry] = LTRIM(RTRIM(REPLACE(REPLACE([Change_To_Entry],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') ))
		FROM @Get_PbmNEU

END









