--USE [CWP_3.0]
--GO

--These are the scripts for removing and adding the columns for Sandoz/NEU.  I�ve already run them in the CWP_VBA_DEV database.
--We are removing and adding columns for this client/ta.

--These columns are not configurable, so you�ll have to remove them in you CF code
--'StepTherapy_Footnote'
--'preferred_status'

--These are the scripts

delete FROM dbo.tblQueryMasterUnion
WHERE IndicationDrug='neu'
AND fieldname IN
(
'Transplantation_Required_Opr',
'AML_Required_Opr',
'Progenitor_Cell_Collection_Required_Opr',
'Severe_Chronic_Neutropenia_Required_Opr',
'Hematopoietic_Syndrome_of_ARS_Required_Description_Opr',
'Short_Acting_GCSF_Failure_Required',
'Short_Acting_GCSF_Required_Prior_to_Long_Acting_GCSF',
'Nbr_of_Short_Acting_GCSF_Failures_Required',
'Long_Acting_GCSF_Failure_Required',
'Nbr_of_Long_Acting_GCSF_Failures_Required',
'GCSF_Agent_Before_Neupogen_and/or_Neulasta_Required',
'GCSF_Agent_Before_Neupogen_and/or_Neulasta_Required_Description',
'Nbr_of_GCSF_Agent_Before_Neupogen_and/or_Neulasta_Required',
'Febrile_Neutropenia_associated_with_chemotherapy_Opr',
'Generic_First_Preferred',
'Generic_First_Description'
)

INSERT INTO [dbo].[tblQueryMasterUnion]
           ([IndicationDrug]
           ,[Client]
           ,[LabelName]
           ,[FieldName]
           ,[ColumnType]
           ,[SortOrder])
     VALUES
           ('NEU','Sandoz','Label Indications Not Covered','Label_Indications_Not_Covered',NULL,8.1)

GO