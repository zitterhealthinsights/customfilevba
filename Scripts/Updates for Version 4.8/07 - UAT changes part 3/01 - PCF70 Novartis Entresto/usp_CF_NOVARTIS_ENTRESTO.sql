--USE [CWP_3.0]
--GO
/****** Object:  StoredProcedure [dbo].[usp_CF_NOVARTIS_ENTRESTO]    Script Date: 10/25/2018 5:40:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--EXEC usp_CF_NOVARTIS_ENTRESTO @Userid = 2635, @IndOrDrug = 'CHF'
ALTER PROCEDURE [dbo].[usp_CF_NOVARTIS_ENTRESTO]
	@Userid INT,
	@IndOrDrug VARCHAR(50)
AS
BEGIN

DECLARE @selectedFields AS VARCHAR(MAX)
DECLARE @StateRaw AS CF_NOVARTIS_STATE_CHF
DECLARE @PbmRaw AS CF_NOVARTIS_PBM_CHF
DECLARE @StateRemapped AS CF_NOVARTIS_STATE_CHF_REMAPPED
DECLARE @PbmRemapped AS CF_NOVARTIS_PBM_CHF_REMAPPED
DECLARE @StateFiltered AS CF_NOVARTIS_STATE_CHF_REMAPPED
DECLARE @PbmFiltered AS CF_NOVARTIS_PBM_CHF_REMAPPED
DECLARE @States TABLE(ID INT IDENTITY(1,1) NOT NULL, [State] VARCHAR(255))
DECLARE @OutputUnion AS CF_NOVARTIS_STATE_VIEW

	------------------------ Get Raw tables from usp_Get_Plans
	-- Get State File
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_STATE_CHF','');
	INSERT INTO @StateRaw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = @IndOrDrug, 							
			@Userid = @Userid, 		
			@SelectFields = @selectedFields,	-- TODO: is this precaution from future changes of 	usp_Get_Plans ?					
			@Level = 'State'

																						--SELECT * FROM @StateRaw
	-- Get PBM File
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_PBM_CHF','');
	INSERT INTO @PbmRaw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = @IndOrDrug, 							
			@Userid = @Userid, 				
			@SelectFields = @selectedFields,					
			@isPBMOnly = 1
																						--SELECT * FROM @PbmRaw



	------------------------ Remove unwanted columns, rename columns. INTERNAL file, sheets "CHF State Flat File - Full" and "CHF PBM File"
	INSERT INTO @StateRemapped
		SELECT
			[MCoid] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([TZG Payer Name],CHAR(13),' '),CHAR(10),' ') )),
			[PlanName] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[StateName] = LTRIM(RTRIM(REPLACE(REPLACE([StateName],CHAR(13),' '),CHAR(10),' ') )),
			[State] = LTRIM(RTRIM(REPLACE(REPLACE([State],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[MarketSharePercentage] = LTRIM(RTRIM(REPLACE(REPLACE([MarketSharePercentage],CHAR(13),' '),CHAR(10),' ') )),
			[Self Insured Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Commercial ASO],CHAR(13),' '),CHAR(10),' ') )),
			[PlanLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanLivesInState],CHAR(13),' '),CHAR(10),' ') )),
			[Fully Insured Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Commercial Fully Insured],CHAR(13),' '),CHAR(10),' ') )),
			[Medical Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Medical],CHAR(13),' '),CHAR(10),' ') )),
			[Pharmacy Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Pharmacy],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[On_Formulary] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
			[preferred_agents] = LTRIM(RTRIM(REPLACE(REPLACE([preferred_agents],CHAR(13),' '),CHAR(10),' ') )),
			[Preferred_Status] = LTRIM(RTRIM(REPLACE(REPLACE([Preferred_Status],CHAR(13),' '),CHAR(10),' ') )),
			[Preferred_Implementation] = LTRIM(RTRIM(REPLACE(REPLACE([Preferred_Implementation],CHAR(13),' '),CHAR(10),' ') )),
			[Novartis_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Novartis_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
			[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
			[Split_Fill_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Split_Fill_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE(PA_Required,CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[General_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([General_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Management] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Management],CHAR(13),' '),CHAR(10),' ') )),
			[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
			[Age_Limit] = LTRIM(RTRIM(REPLACE(REPLACE([Age_Limit],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Notes] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_PA_Criteria_details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
			[Mandates_on_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
			[Mandates_on_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_Combination_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Combination_Therapy],CHAR(13),' '),CHAR(10),' ') )),
			[Prohibits_Specified_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
			[Prohibits_Specified_Concomitant_Therapies_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Nbr_Beta_Blocker_Trials_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_Beta_Blocker_Trials_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Beta_Blocker_Required_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Beta_Blocker_Required_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Beta_Blocker_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Beta_Blocker_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Nbr_ACE_Inhibitor_Trials_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_ACE_Inhibitor_Trials_Required],CHAR(13),' '),CHAR(10),' ') )),
			[ACE_Inhibitor_Required_Length] = LTRIM(RTRIM(REPLACE(REPLACE([ACE_Inhibitor_Required_Length],CHAR(13),' '),CHAR(10),' ') )),
			[ACE_Inhibitor_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([ACE_Inhibitor_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Nbr_Diuretics_Trials_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_Diuretics_Trials_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Diuretics_Required_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Diuretics_Required_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Diuretics_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Diuretics_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Nbr_ARBs_Trials_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_ARBs_Trials_Required],CHAR(13),' '),CHAR(10),' ') )),
			[ARBs_Required_Length] = LTRIM(RTRIM(REPLACE(REPLACE([ARBs_Required_Length],CHAR(13),' '),CHAR(10),' ') )),
			[ARBs_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([ARBs_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[NYHA_Classification_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([NYHA_Classification_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Heart_Rate_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Heart_Rate_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Heart_Rate_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Heart_Rate_Description],CHAR(13),' '),CHAR(10),' ') )),
			[LVEF_status_Required] = LTRIM(RTRIM(REPLACE(REPLACE([LVEF_status_Required],CHAR(13),' '),CHAR(10),' ') )),
			[LVEF_status_Required_Description] = LTRIM(RTRIM(REPLACE(REPLACE([LVEF_status_Required_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy_Criteria] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_Criteria],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Required_for_Continued_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_for_Continued_Approval],CHAR(13),' '),CHAR(10),' ') )),
			[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[Specialist_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Approval],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
			[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
			[Name_of_Specialty_Drug_Distributer_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
			[Name_of_Specialty_Drug_Distributer_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Policy_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Policy_Utilized],CHAR(13),' '),CHAR(10),' ') )),
			[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
			[Change_to_Entry] = LTRIM(RTRIM(REPLACE(REPLACE([Change_to_Entry],CHAR(13),' '),CHAR(10),' ') )),
			[Reason_for_Change] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change],CHAR(13),' '),CHAR(10),' ') )),
			[Reason_for_Change_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change_Details],CHAR(13),' '),CHAR(10),' ') )),
			[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
			--[Original_Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Original_Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[No_policy_date_found] = LTRIM(RTRIM(REPLACE(REPLACE([No_policy_date_found],CHAR(13),' '),CHAR(10),' ') )),
			[Entry_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
			[Note_2] = LTRIM(RTRIM(REPLACE(REPLACE([Note_2],CHAR(13),' '),CHAR(10),' ') )),
			[Note_3] = LTRIM(RTRIM(REPLACE(REPLACE([Note_3],CHAR(13),' '),CHAR(10),' ') ))
		FROM @StateRaw
																						--SELECT * FROM @StateRemapped

	INSERT INTO @PbmRemapped
		SELECT
			[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_TotalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Total_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_ControlledFullyInsuredPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Controlled_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_ClaimsLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Claims_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_SelfInsuredPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Self_Insured_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[preferred_agents] = LTRIM(RTRIM(REPLACE(REPLACE([preferred_agents],CHAR(13),' '),CHAR(10),' ') )),
			[Preferred_Status] = LTRIM(RTRIM(REPLACE(REPLACE([Preferred_Status],CHAR(13),' '),CHAR(10),' ') )),
			[Preferred_Implementation] = LTRIM(RTRIM(REPLACE(REPLACE([Preferred_Implementation],CHAR(13),' '),CHAR(10),' ') )),
			[Novartis_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Novartis_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
			[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE(PA_Required,CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Management] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Management],CHAR(13),' '),CHAR(10),' ') )),
			[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
			[Age_Limit] = LTRIM(RTRIM(REPLACE(REPLACE([Age_Limit],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Notes] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
			[Mandates_on_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
			[Mandates_on_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_Combination_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Combination_Therapy],CHAR(13),' '),CHAR(10),' ') )),
			[Prohibits_Specified_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
			[Prohibits_Specified_Concomitant_Therapies_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_PA_Criteria_details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
			[Nbr_Beta_Blocker_Trials_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_Beta_Blocker_Trials_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Beta_Blocker_Required_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Beta_Blocker_Required_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Beta_Blocker_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Beta_Blocker_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Nbr_ACE_Inhibitor_Trials_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_ACE_Inhibitor_Trials_Required],CHAR(13),' '),CHAR(10),' ') )),
			[ACE_Inhibitor_Required_Length] = LTRIM(RTRIM(REPLACE(REPLACE([ACE_Inhibitor_Required_Length],CHAR(13),' '),CHAR(10),' ') )),
			[ACE_Inhibitor_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([ACE_Inhibitor_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Nbr_Diuretics_Trials_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_Diuretics_Trials_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Diuretics_Required_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Diuretics_Required_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Diuretics_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Diuretics_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Nbr_ARBs_Trials_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_ARBs_Trials_Required],CHAR(13),' '),CHAR(10),' ') )),
			[ARBs_Required_Length] = LTRIM(RTRIM(REPLACE(REPLACE([ARBs_Required_Length],CHAR(13),' '),CHAR(10),' ') )),
			[ARBs_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([ARBs_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[NYHA_Classification_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([NYHA_Classification_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Heart_Rate_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Heart_Rate_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Heart_Rate_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Heart_Rate_Description],CHAR(13),' '),CHAR(10),' ') )),
			[LVEF_status_Required] = LTRIM(RTRIM(REPLACE(REPLACE([LVEF_status_Required],CHAR(13),' '),CHAR(10),' ') )),
			[LVEF_status_Required_Description] = LTRIM(RTRIM(REPLACE(REPLACE([LVEF_status_Required_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy_Criteria] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_Criteria],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Required_for_Continued_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_for_Continued_Approval],CHAR(13),' '),CHAR(10),' ') )),
			[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[Specialist_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Approval],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
			[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
			[Name_of_Specialty_Drug_Distributer_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
			[Name_of_Specialty_Drug_Distributer_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Policy_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Policy_Utilized],CHAR(13),' '),CHAR(10),' ') )),
			[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
			[Change_to_Entry] = LTRIM(RTRIM(REPLACE(REPLACE([Change_to_Entry],CHAR(13),' '),CHAR(10),' ') )),
			[Reason_for_Change] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change],CHAR(13),' '),CHAR(10),' ') )),
			[Reason_for_Change_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change_Details],CHAR(13),' '),CHAR(10),' ') )),
			[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
			--[Original_Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Original_Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[No_policy_date_found] = LTRIM(RTRIM(REPLACE(REPLACE([No_policy_date_found],CHAR(13),' '),CHAR(10),' ') )),
			[Entry_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
			[Note_2] = LTRIM(RTRIM(REPLACE(REPLACE([Note_2],CHAR(13),' '),CHAR(10),' ') )),
			[Note_3] = LTRIM(RTRIM(REPLACE(REPLACE([Note_3],CHAR(13),' '),CHAR(10),' ') ))
		FROM @PbmRaw
																						--SELECT * FROM @PbmRemapped


	------------------------ Apply Filters
	
	-- Filter State File by Drug_Name = 'Entresto'
	INSERT INTO @StateFiltered
		SELECT * FROM @StateRemapped WHERE Drug_Name = 'Entresto'
		-- 2017-11-14 Milan: v3.7 Exclude the following MCOID's { 313003001, 313000002, 313003000, 630014900 }
		AND [MCoid] NOT IN ('313003001', '313000002', '313003000', '630014900')
																						--SELECT * FROM @StateFiltered

	-- Filter PBM File by Drug_Name = 'Entresto' AND McoID IN (...)
	INSERT INTO @PbmFiltered
		SELECT * FROM @PbmRemapped WHERE Drug_Name = 'Entresto'
		AND [Mcoid] IN ('900282000', '900590000', '900560000', '900560010', '900520000', '900350000', '900270010', 
		                '900270000', '900360000', '900660000', '900252005', '900260000', '900790000', '900060000', 
						'900800000', '900150020', '900150010', '900150000', '900300000', '900690000', '900080000', 
						'900090000', '900332000'
						-- 2017-11-14 Milan: v3.7 Include the following MCOID { 900081005 }
						,'900081005'
						-- 2017-11-22 Milan: v3.8 Include the following MCOID { 900151005 }
						,'900151005')
   
																						--SELECT * FROM @PbmFiltered




	------------------------ Do the Translations
	BEGIN -- Translations (just for collapse/expand)
	---------------------------------------------------------- State Name
	-- output: [State] (State only), Rename State Name

	UPDATE @StateFiltered SET [StateName] = 'Washington, DC' WHERE [StateName] = 'District of Columbia'


	UPDATE @StateFiltered SET [Segment] = 'Commercial' WHERE [Segment] = 'Commercial MCO'
	UPDATE @PbmFiltered   SET [Segment] = 'Commercial' WHERE [Segment] = 'Commercial MCO'

	UPDATE @StateFiltered SET [Segment] = 'Medicare' WHERE [Segment] = 'Managed Medicare'
	UPDATE @PbmFiltered   SET [Segment] = 'Medicare' WHERE [Segment] = 'Managed Medicare'

	-- 2017-11-14 Milan: v3.7 When field value is “Not Applicable”, user should see “No”
	--Mandates_on_Concomitant_Therapies_Desc, Dosing_Limitation, Quantity_Limitation, Quantity_Limitation_Desc, Length of Initial Authorization, LVEF_status_Required
	UPDATE @StateFiltered SET [Mandates_on_Concomitant_Therapies_Desc] = 'No' WHERE [Mandates_on_Concomitant_Therapies_Desc] = 'Not Applicable'
	UPDATE @PbmFiltered   SET [Mandates_on_Concomitant_Therapies_Desc] = 'No' WHERE [Mandates_on_Concomitant_Therapies_Desc] = 'Not Applicable'

	UPDATE @StateFiltered SET [Dosing_Limitation_Desc] = 'No' WHERE [Dosing_Limitation_Desc] = 'Not Applicable'
	UPDATE @PbmFiltered   SET [Dosing_Limitation_Desc] = 'No' WHERE [Dosing_Limitation_Desc] = 'Not Applicable'

	UPDATE @StateFiltered SET [Quantity_Limitation] = 'No' WHERE [Quantity_Limitation] = 'Not Applicable'
	UPDATE @PbmFiltered   SET [Quantity_Limitation] = 'No' WHERE [Quantity_Limitation] = 'Not Applicable'

	UPDATE @StateFiltered SET [Quantity_Limitation_Desc] = 'No' WHERE [Quantity_Limitation_Desc] = 'Not Applicable'
	UPDATE @PbmFiltered   SET [Quantity_Limitation_Desc] = 'No' WHERE [Quantity_Limitation_Desc] = 'Not Applicable'

	UPDATE @StateFiltered SET [Initial_Auth_Time_Length] = 'No' WHERE [Initial_Auth_Time_Length] = 'Not Applicable'
	UPDATE @PbmFiltered   SET [Initial_Auth_Time_Length] = 'No' WHERE [Initial_Auth_Time_Length] = 'Not Applicable'

	UPDATE @StateFiltered SET [LVEF_status_Required_Description] = 'No' WHERE [LVEF_status_Required_Description] = 'Not Applicable'
	UPDATE @PbmFiltered   SET [LVEF_status_Required_Description] = 'No' WHERE [LVEF_status_Required_Description] = 'Not Applicable'

	-- 2017-11-14 Milan: v3.7 When field value is “Not Applicable”, user should see “Meets below requirements”
	--PA_Required
	UPDATE @StateFiltered SET [PA_Required] = 'Meets below requirements' WHERE [PA_Required] = 'Not Applicable'
	UPDATE @PbmFiltered   SET [PA_Required] = 'Meets below requirements' WHERE [PA_Required] = 'Not Applicable'



	UPDATE @StateFiltered SET       [Dosing_Limitation_Desc] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Dosing_Limitation_Desc] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Dosing_Limitation_Desc] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Dosing_Limitation_Desc] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Lab_Requirements] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Lab_Requirements] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Lab_Requirements] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Lab_Requirements] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Initial_Auth_Time_Length] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Initial_Auth_Time_Length] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Initial_Auth_Time_Length] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Initial_Auth_Time_Length] = 'Data Not Available'

	UPDATE @StateFiltered SET       [LVEF_status_Required_Description] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [LVEF_status_Required_Description] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [LVEF_status_Required_Description] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [LVEF_status_Required_Description] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Mandates_on_Concomitant_Therapies_Desc] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Mandates_on_Concomitant_Therapies_Desc] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Mandates_on_Concomitant_Therapies_Desc] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Mandates_on_Concomitant_Therapies_Desc] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Number_of_Tiers] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Number_of_Tiers] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Number_of_Tiers] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Number_of_Tiers] = 'Data Not Available'

	UPDATE @StateFiltered SET       [NYHA_Classification_Requirements] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [NYHA_Classification_Requirements] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [NYHA_Classification_Requirements] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [NYHA_Classification_Requirements] = 'Data Not Available'

	UPDATE @StateFiltered SET       [PA_Required] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [PA_Required] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [PA_Required] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [PA_Required] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Specific_PA_Criteria_details] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Specific_PA_Criteria_details] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Specific_PA_Criteria_details] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Specific_PA_Criteria_details] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Proof_of_Effectiveness_Required_for_Continued_Approval] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Proof_of_Effectiveness_Required_for_Continued_Approval] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Proof_of_Effectiveness_Required_for_Continued_Approval] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Proof_of_Effectiveness_Required_for_Continued_Approval] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Quantity_Limitation_Desc] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Quantity_Limitation_Desc] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Quantity_Limitation_Desc] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Quantity_Limitation_Desc] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Specialist_Approval] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Specialist_Approval] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Specialist_Approval] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Specialist_Approval] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Step_Therapy_Placement] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Step_Therapy_Placement] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Step_Therapy_Placement] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Step_Therapy_Placement] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Step_Therapy_Req] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Step_Therapy_Req] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Step_Therapy_Req] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Step_Therapy_Req] = 'Data Not Available'

	UPDATE @StateFiltered SET       [Tier_Placement] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Tier_Placement] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [Tier_Placement] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [Tier_Placement] = 'Data Not Available'

	UPDATE @StateFiltered SET       [URL_to_PA_Form] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [URL_to_PA_Form] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [URL_to_PA_Form] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [URL_to_PA_Form] = 'Data Not Available'

	UPDATE @StateFiltered SET       [URL_to_PA_Policy] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [URL_to_PA_Policy] = 'Data Not Available'
	UPDATE @PbmFiltered   SET       [URL_to_PA_Policy] = 'See Managed Medicaid plans in NJ' 
	WHERE [MCoid] = '500000311' AND [URL_to_PA_Policy] = 'Data Not Available'


	-- 2018-10-25 Milan: v4.8 removed on user request PCF-70
	---- 2017-11-14 Milan: v3.7 URL_to_PA_Form: IF plan MCOID = 900660000 THEN display this hyperlink: https://www.entresto-coverage.com/siteassets/entresto-sacubitril-valsartan-46---22532.pdf
	--UPDATE @StateFiltered SET [URL_to_PA_Form] = 'https://www.entresto-coverage.com/siteassets/entresto-sacubitril-valsartan-46---22532.pdf' 
	--WHERE [MCoid] = '900660000'
	--UPDATE @PbmFiltered   SET [URL_to_PA_Form] = 'https://www.entresto-coverage.com/siteassets/entresto-sacubitril-valsartan-46---22532.pdf' 
	--WHERE [MCoid] = '900660000'

	-- 2017-11-14 Milan: v3.7 Plan_Name: IF plan MCOID = 602001031 THEN display the plan name as "UNITED HEALTHCARE - COMMUNITY PLAN OF NEW JERSEY/AMERICHOICE"
	UPDATE @StateFiltered SET [PlanName] = 'UNITED HEALTHCARE - COMMUNITY PLAN OF NEW JERSEY/AMERICHOICE' 
	WHERE [MCoid] = '602001031'
	UPDATE @PbmFiltered   SET [PBM_Name] = 'UNITED HEALTHCARE - COMMUNITY PLAN OF NEW JERSEY/AMERICHOICE' 
	WHERE [MCoid] = '602001031'

	-- 2017-11-14 Milan: v3.7 IF plan MCOID = 476000001 THEN 
		--If Tier Placement value = 1, the display "Preferred". 
		--If value = 2, the client should see "Non-Preferred"	
		--Else
		--	Tier Placement value
		--End
	UPDATE @StateFiltered SET [Tier_Placement] =	CASE	WHEN [Tier_Placement] = '1' THEN 'Preferred'
															WHEN [Tier_Placement] = '2' THEN 'Non-Preferred'
															ELSE [Tier_Placement]
													END
	WHERE [MCoid] = '476000001'

	UPDATE @PbmFiltered   SET [Tier_Placement] =	CASE	WHEN [Tier_Placement] = '1' THEN 'Preferred'
															WHEN [Tier_Placement] = '2' THEN 'Non-Preferred'
															ELSE [Tier_Placement]
													END
	WHERE [MCoid] = '476000001'






	UPDATE @StateFiltered SET [Tier_Placement] = 'Preferred' WHERE [Tier_Placement] = '1' AND [Segment] = 'State Medicaid'
	UPDATE @PbmFiltered   SET [Tier_Placement] = 'Preferred' WHERE [Tier_Placement] = '1' AND [Segment] = 'State Medicaid'

	UPDATE @StateFiltered SET [Tier_Placement] = 'Non-Preferred' WHERE [Tier_Placement] = '2' AND [Segment] = 'State Medicaid'
	UPDATE @PbmFiltered   SET [Tier_Placement] = 'Non-Preferred' WHERE [Tier_Placement] = '2' AND [Segment] = 'State Medicaid'


	UPDATE @StateFiltered
		SET [PlanName] = LTRIM(RTRIM(SUBSTRING([PlanName], 1, CHARINDEX('[', [PlanName]) - 1)))
	WHERE CHARINDEX('[', [PlanName]) > 0

	UPDATE @PbmFiltered
		SET [PBM_Name] = LTRIM(RTRIM(SUBSTRING([PBM_Name], 1, CHARINDEX('[', [PBM_Name]) - 1)))
	WHERE CHARINDEX('[', [PBM_Name]) > 0

	-- This will be done on all columns, not just [PA_Required]. It will be done in VBA (faster)
	--UPDATE @StateFiltered SET [PA_Required] = 'PA required: Plan details are unavailable at this time' WHERE [PA_Required] = 'PA Required No Criteria Exist'
	--UPDATE @PbmFiltered   SET [PA_Required] = 'PA required: Plan details are unavailable at this time' WHERE [PA_Required] = 'PA Required No Criteria Exist'

	UPDATE @StateFiltered SET [Segment] = 'Medicaid' WHERE [Segment] = 'Managed Medicaid'
	UPDATE @PbmFiltered   SET [Segment] = 'Medicaid' WHERE [Segment] = 'Managed Medicaid'

	UPDATE @StateFiltered SET [Segment] = 'Medicaid' WHERE [Segment] = 'State Medicaid'
	UPDATE @PbmFiltered   SET [Segment] = 'Medicaid' WHERE [Segment] = 'State Medicaid'


	UPDATE @StateFiltered
		SET [Segment] = CASE WHEN [MCoid] = '90028000' OR [MCoid] = '900252005' OR [MCoid] = '900332000' THEN
							'Medicaid'
						ELSE
							'Commercial'
						END
	WHERE [Segment] = 'PBM'

	UPDATE @PbmFiltered
		SET [Segment] = CASE WHEN [MCoid] = '90028000' OR [MCoid] = '900252005' OR [MCoid] = '900332000' THEN
							'Medicaid'
						ELSE
							'Commercial'
						END
	WHERE [Segment] = 'PBM'

	--4D Pharmacy
	/*
		-- From Marie
	One more thing – 4D Pharmacy (MCOID = 900282000) should be Medicaid in “Type of Plan” column (column A).  Otherwise, looks good. 
	*/
	UPDATE @StateFiltered SET [Segment] = 'Medicaid' WHERE [MCoid] = '900282000'  
	UPDATE @PbmFiltered   SET [Segment] = 'Medicaid' WHERE [MCoid] = '900282000'  


	UPDATE @StateFiltered SET [PlanName] = 'HIGHMARK BCBS/KEYSTONE WEST' WHERE [MCoid] = '293007002'
	UPDATE @PbmFiltered   SET [PBM_Name] = 'HIGHMARK BCBS/KEYSTONE WEST' WHERE [MCoid] = '293007002'
	
	-- 2018-10-25 Milan: v4.8 removed on user request PCF-70
	--UPDATE @StateFiltered SET [URL_to_PA_Form] = N'https://www.entresto-coverage.com/siteassets/entresto-sacubitril-valsartan-46---22532.pdf' WHERE [MCoid] = '298000000' OR [MCoid] = '298000002'
	--UPDATE @PbmFiltered   SET [URL_to_PA_Form] = N'https://www.entresto-coverage.com/siteassets/entresto-sacubitril-valsartan-46---22532.pdf' WHERE [MCoid] = '298000000' OR [MCoid] = '298000002'

	UPDATE @StateFiltered SET [PlanName] = 'EXCELLUS BCBS/UNIVERA HEALTHCARE' WHERE [MCoid] = '319001000' OR [MCoid] = '319001002'
	UPDATE @PbmFiltered   SET [PBM_Name] = 'EXCELLUS BCBS/UNIVERA HEALTHCARE' WHERE [MCoid] = '319001000' OR [MCoid] = '319001002'

	UPDATE @StateFiltered SET [Tier_Placement] = '3 Brand Preferred' WHERE [Tier_Placement] = '3' AND [Segment] = 'Medicare'
	UPDATE @PbmFiltered   SET [Tier_Placement] = '3 Brand Preferred' WHERE [Tier_Placement] = '3' AND [Segment] = 'Medicare'

	-- Change requested by users 2016-11-11
	--UPDATE @StateFiltered SET [PlanName] = 'Federal' WHERE [MCoid] = '899001000' OR [MCoid] = '899000000'
	--UPDATE @PbmFiltered   SET [PBM_Name] = 'Federal' WHERE [MCoid] = '899001000' OR [MCoid] = '899000000'
	UPDATE @StateFiltered SET [Segment] = 'Federal' WHERE [MCoid] = '899001000' OR [MCoid] = '899000000'
	UPDATE @PbmFiltered   SET [Segment] = 'Federal' WHERE [MCoid] = '899001000' OR [MCoid] = '899000000'

	-- First request 2016-11-11 : [PlanName] should be copied from [Name of Payer]
	--                            for STATE that means [PlanName] <- [PayerName]
	--                            for PBM do nothing. Both [PlanName] and [Name of Payer] are already same, coming from the same source (PBM_Name)

	-- Second request 2016-11-16 : For MCoid = 899000000, make an exception. Force it to constant 'TRICARE (ACTIVE DUTY, DEPENDENTS, AND RETIREES)'
	UPDATE @StateFiltered SET [PlanName] = [PayerName] WHERE [MCoid] = '899001000'
	UPDATE @StateFiltered SET [PlanName] = 'TRICARE (ACTIVE DUTY, DEPENDENTS, AND RETIREES)' WHERE [MCoid] = '899000000'
	--UPDATE @PbmFiltered   SET [PBM_Name] = [PBM_Name] WHERE [MCoid] = '899001000' OR [MCoid] = '899000000' -- It comes from the same source

	-- 2017-11-28 Milan: JIRA request, for 900151005 - force Segment = 'Medicare'
	UPDATE @StateFiltered SET [Segment] = 'Medicare' WHERE [MCoid] = '900151005'
	UPDATE @PbmFiltered   SET [Segment] = 'Medicare' WHERE [MCoid] = '900151005'

	END -- Translations





	------------------------ Fill States
	INSERT INTO @States
		SELECT DISTINCT
			[State] = [StateName]
		FROM @StateFiltered
		ORDER BY [StateName]


	------------------------ Fill the Union output table
	-- Load Up State Data
	INSERT INTO @OutputUnion
		SELECT
			[Type of Plan] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[PlanName] = LTRIM(RTRIM(REPLACE(REPLACE([PlanName],CHAR(13),' '),CHAR(10),' ') )),
			[State] = LTRIM(RTRIM(REPLACE(REPLACE([StateName],CHAR(13),' '),CHAR(10),' ') )),
			[Name of Payer] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
			[PA Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Step Therapy Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Step Therapy Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[PA Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
			[Mandates on Concomitant Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Tier] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number of Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
			[URL to PA Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[URL to PA Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing Limitations] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity Limitations] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Length of Initial Authorization] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Lab Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[NYHA Classification Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([NYHA_Classification_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[LVEF Status Required] = LTRIM(RTRIM(REPLACE(REPLACE([LVEF_status_Required_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Proof of Effectiveness Required] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_for_Continued_Approval],CHAR(13),' '),CHAR(10),' ') )),
			[Specialist Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Approval],CHAR(13),' '),CHAR(10),' ') )),
			[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE([Policy_Date],CHAR(13),' '),CHAR(10),' ') )),
			[MCOID] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') ))
		FROM @StateFiltered
	 

	-- Cursor Loop to add PBM's for each state
	DECLARE @LoopCount INT = 1
	DECLARE @Count INT
	DECLARE @SINGLE_STATE VARCHAR(155)	 

	SELECT @Count = MAX(ID) FROM @States 

	WHILE @LoopCount <= @Count
		BEGIN
			SELECT @SINGLE_STATE = State FROM @States  WHERE ID = @LoopCount

			INSERT INTO @OutputUnion
			SELECT
				[Type of Plan] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
				[PlanName] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Name],CHAR(13),' '),CHAR(10),' ') )),
				[State] = LTRIM(RTRIM(REPLACE(REPLACE(@SINGLE_STATE,CHAR(13),' '),CHAR(10),' ') )),
				[Name of Payer] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Name],CHAR(13),' '),CHAR(10),' ') )),
				[PA Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
				[Step Therapy Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Step Therapy Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[PA Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
				[Mandates on Concomitant Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Tier] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number of Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
				[URL to PA Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[URL to PA Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing Limitations] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity Limitations] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Length of Initial Authorization] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Lab Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[NYHA Classification Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([NYHA_Classification_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[LVEF Status Required] = LTRIM(RTRIM(REPLACE(REPLACE([LVEF_status_Required_Description],CHAR(13),' '),CHAR(10),' ') )),
				[Proof of Effectiveness Required] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_for_Continued_Approval],CHAR(13),' '),CHAR(10),' ') )),
				[Specialist Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Approval],CHAR(13),' '),CHAR(10),' ') )),
				[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE([Policy_Date],CHAR(13),' '),CHAR(10),' ') )),
				[MCOID] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') ))
			FROM @PbmFiltered

			SET @LoopCount = @LoopCount + 1;
		END		 

	-- Add Puerto Rico at the end of the Union Table
	INSERT INTO @OutputUnion ([Type of Plan], [State]) VALUES('Commercial', 'Puerto Rico' )
	INSERT INTO @OutputUnion ([Type of Plan], [State]) VALUES('Medicare', 'Puerto Rico' )
	INSERT INTO @OutputUnion ([Type of Plan], [State]) VALUES('Medicaid', 'Puerto Rico' )
	




	------------------------ Output recordsets

	-- 'State view'
	SELECT [Type of Plan] ,
           PlanName ,
           [State] ,
           [Name of Payer] ,
           [PA Required] ,
           [Step Therapy Requirements] ,
           [Step Therapy Placement] ,
           [PA Requirements] ,
           [Mandates on Concomitant Therapies] ,
           Tier ,
           [Number of Tiers] ,
           [URL to PA Policy] ,
           [URL to PA Form] ,
           [Dosing Limitations] ,
           [Quantity Limitations] ,
           [Length of Initial Authorization] ,
           [Lab Requirements] ,
           [NYHA Classification Requirements] ,
           [LVEF Status Required] ,
           [Proof of Effectiveness Required] ,
           [Specialist Approval] ,
           Policy_Date ,			  
           MCOID  --= CASE WHEN MCOID = 99999999999 THEN '' ELSE  MCOID END
		   FROM @OutputUnion   
	ORDER BY PlanName, [State]

	-- 'States'
	--SELECT * FROM @States        
	SELECT 'Puerto Rico' AS 'States'

	-- 'CHF State Flat File – Full'
	SELECT * FROM @StateFiltered 

	-- 'CHF PBM File – Full' (to be removed)
	--SELECT * FROM @PbmFiltered   
END



