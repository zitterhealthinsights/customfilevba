--USE [CWP_3.0]
--GO

SELECT * FROM Process WHERE ProcessID = 27
SELECT * FROM ProcessDetail WHERE ProcessID = 27
EXEC usp_CF_Get_ProcessSteps_ByProcessID @ProcessID = 27
/*
EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 27, -- Monthly Files for Genentech
    @ClientID = 9, -- Genentech
    @StepDescription = 'EXCEPTION REPORT',
    @IndicationNames = '',
    @DrugNames = ''
-- New record is created with ProcessDetailID = 85

EXEC usp_CF_SaveStep_SequenceNumber @ProcessDetailID = 85, @SequenceNumber = 1000
*/
SELECT * FROM ProcessDetail WHERE ProcessID = 27