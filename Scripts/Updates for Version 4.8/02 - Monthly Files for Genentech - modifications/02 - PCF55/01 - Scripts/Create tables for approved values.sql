--USE [CWP_3.0]
--GO

CREATE TABLE CF_FieldName
(
	[FieldNameID] [INT] IDENTITY(1,1) NOT NULL,
	[FieldName] [varchar](255) NOT NULL
)

INSERT INTO CF_FieldName VALUES ('DNA_Healthplan_Management')
INSERT INTO CF_FieldName VALUES ('Step_Therapy_Placement')
INSERT INTO CF_FieldName VALUES ('Step_Therapy_Req')

CREATE TABLE CF_ApprovedFieldValues
(
	[AFVID] [INT] IDENTITY(1,1) NOT NULL,
	[FieldNameID] [INT] NOT NULL,
	[FieldValue] [varchar](255) NOT NULL
)

-- DNA_Healthplan_Management
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Bio Managed 1')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Bio Managed 2')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Bio Managed 3')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Clinical Criteria Required')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Data Not Available')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Drug Covered with No PA')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Highly Managed')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Less Managed')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Less Managed than PI')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Managed to PI')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Moderately Managed')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Narrower than PI')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'Not Covered')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'PA Required No Criteria Exist')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'To PI or Better')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (1, 'To PI with Criteria')

-- Step_Therapy_Placement
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'B vs D')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'Covered for All Medically Accepted Indications')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'Data Not Available')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'Drug Covered with No PA')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'Follows FDA Guidelines')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'Follows NCCN Guidelines')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'Non-Formulary')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'Not Applicable')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'Not Covered')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'PA Required No Criteria Exist')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (2, 'Step Therapy Required No Criteria Exist')

-- Step_Therapy_Req
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'B vs D')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'Covered for All Medically Accepted Indications')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'Data Not Available')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'Drug Covered with No PA')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'Follows FDA Guidelines')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'Follows NCCN Guidelines')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'No')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'Not Covered')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'PA Required No Criteria Exist')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'Step Edit Only')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'Treatment Option')
INSERT INTO CF_ApprovedFieldValues (FieldNameID, FieldValue) VALUES (3, 'Yes, Enforced via PA')

SELECT * FROM CF_FieldName
SELECT * FROM CF_ApprovedFieldValues