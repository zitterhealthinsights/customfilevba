--USE [CWP_3.0]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_GENENTECH_FLAT_MCNV]    Script Date: 8/24/2018 7:14:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- Data:    EXEC usp_CF_GENENTECH_FLAT_MCNV 2630, 'mCNV'
-- Data:    EXEC usp_CF_GENENTECH_FLAT_MCNV 2630, 'mCNV', 0
-- Errors:  EXEC usp_CF_GENENTECH_FLAT_MCNV 2630, 'mCNV', 1
ALTER PROCEDURE [dbo].[usp_CF_GENENTECH_FLAT_MCNV]
	@Userid INT,
	@IndOrDrug VARCHAR(50),
	@ReturnErrors BIT = 0
AS
BEGIN

	DECLARE @Get_CF_GENENTECH_FLAT_MCNV as CF_GENENTECH_FLAT_MCNV
		
	DECLARE @selectedFields as varchar(max)
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_GENENTECH_FLAT_MCNV','');
		
	INSERT INTO @Get_CF_GENENTECH_FLAT_MCNV
	EXEC dbo.usp_Get_Plans
		@IndicationorDrug = @IndOrDrug,
		@Userid = @Userid,
		@isPBMOnly = 0,
		@SelectFields = @selectedFields
				




	IF @ReturnErrors = 0
		BEGIN
			SELECT
				[Data_Extraction_Date] = CONVERT(char(10), GetDate(),126), -- ISO8601	yyyy-mm-ddThh:mi:ss.mmm (no spaces)
				[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
				[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
				--[payerlives] = LTRIM(RTRIM(REPLACE(REPLACE([payerlives],CHAR(13),' '),CHAR(10),' ') )),
				--[PayerMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
				--[PayerPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
				--[PayerFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
				--[PayerSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
				[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
				--[plansegmentlives] = LTRIM(RTRIM(REPLACE(REPLACE([plansegmentlives],CHAR(13),' '),CHAR(10),' ') )),
				--[PlanSegmentMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
				--[PlanSegmentPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
				--[PlanSegmentFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
				--[PlanSegmentSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
				[Plan_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
				--[planlives] = LTRIM(RTRIM(REPLACE(REPLACE([planlives],CHAR(13),' '),CHAR(10),' ') )),
				--[PlanMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
				--[PlanPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
				--[PlanFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanFullyInsuredLIves],CHAR(13),' '),CHAR(10),' ') )),
				--[PlanSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
				[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
				[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
				[On_Formulary] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
				[Preferred_Status] = LTRIM(RTRIM(REPLACE(REPLACE([Preferred_Status],CHAR(13),' '),CHAR(10),' ') )),
				[Preferred_implementation] = LTRIM(RTRIM(REPLACE(REPLACE([Preferred_implementation],CHAR(13),' '),CHAR(10),' ') )),
				[DNA_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([DNA_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
				[Journal_Articles_Referenced] = LTRIM(RTRIM(REPLACE(REPLACE([Journal_Articles_Referenced],CHAR(13),' '),CHAR(10),' ') )),
				[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
				[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[General_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([General_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[URL_To_Draft_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_To_Draft_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[PA_Management] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Management],CHAR(13),' '),CHAR(10),' ') )),
				[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
				--[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				--[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				--[Initial_Auth_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				--[Recert_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_PA_Criteria_details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
				[Eye_Limitation_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Eye_Limitation_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Age_Limit] = LTRIM(RTRIM(REPLACE(REPLACE([Age_Limit],CHAR(13),' '),CHAR(10),' ') )),
				[OCT_Req] = LTRIM(RTRIM(REPLACE(REPLACE([OCT_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Fluorescein_Angiography_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Fluorescein_Angiography_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Visual_Acuity_Test_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Visual_Acuity_Test_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Visual_Acuity_Maint/Improvement_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Visual_Acuity_Maint/Improvement_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Visual_Acuity_Maint/Improvement_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Visual_Acuity_Maint/Improvement_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Nbr_of_Bio_Injectable_Trials] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_of_Bio_Injectable_Trials],CHAR(13),' '),CHAR(10),' ') )),
				[BioInject_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([BioInject_Opr],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_Biologic_Failure] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Biologic_Failure],CHAR(13),' '),CHAR(10),' ') )),
				[Laser_Photocoagulation_Trial_Ct] = LTRIM(RTRIM(REPLACE(REPLACE([Laser_Photocoagulation_Trial_Ct],CHAR(13),' '),CHAR(10),' ') )),
				[Laser_Photocoagulation_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Laser_Photocoagulation_Opr],CHAR(13),' '),CHAR(10),' ') )),
				[Intraocular_Corticosteroid_Trial_Ct] = LTRIM(RTRIM(REPLACE(REPLACE([Intraocular_Corticosteroid_Trial_Ct],CHAR(13),' '),CHAR(10),' ') )),
				[Intraocular_Corticosteroid_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Intraocular_Corticosteroid_Opr],CHAR(13),' '),CHAR(10),' ') )),
				[Drug_covered] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_covered],CHAR(13),' '),CHAR(10),' ') )),
				[Documentation_for_Dx] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_for_Dx],CHAR(13),' '),CHAR(10),' ') )),
				[Must_submit_doc_for_Dx] = LTRIM(RTRIM(REPLACE(REPLACE([Must_submit_doc_for_Dx],CHAR(13),' '),CHAR(10),' ') )),
				[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Separate_Pharmacy_and_Medical_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[Separate_Pharmacy_and_Medical_Policy_Criteria] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_Criteria],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
				[Separate_Pharmacy_and_Medical_Policy_URL] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_URL],CHAR(13),' '),CHAR(10),' ') )),
				[ProofOfEfficacy] = LTRIM(RTRIM(REPLACE(REPLACE([ProofOfEfficacy],CHAR(13),' '),CHAR(10),' ') )),
				[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[Specialist_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
				[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
				[Name_Of_Specialty_Drug_Distributer_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
				[Name_Of_Specialty_Drug_Distributer_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
				[Other_Policy_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Policy_Utilized],CHAR(13),' '),CHAR(10),' ') )),
				[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
				[Change_To_Entry] = LTRIM(RTRIM(REPLACE(REPLACE([Change_To_Entry],CHAR(13),' '),CHAR(10),' ') )),
				--[Reason_for_Change] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change],CHAR(13),' '),CHAR(10),' ') )),
				[Change_in_Coverage] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change_Details],CHAR(13),' '),CHAR(10),' ') )),
				[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[No_Policy_Date_Found] = LTRIM(RTRIM(REPLACE(REPLACE([No_Policy_Date_Found],CHAR(13),' '),CHAR(10),' ') )),
				[Entry_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
				[Note_2] = LTRIM(RTRIM(REPLACE(REPLACE([Note_2],CHAR(13),' '),CHAR(10),' ') )),
				[Note_3] = LTRIM(RTRIM(REPLACE(REPLACE([Note_3],CHAR(13),' '),CHAR(10),' ') )),
				[Categorization_Detail] = LTRIM(RTRIM(REPLACE(REPLACE([Categorization_Detail],CHAR(13),' '),CHAR(10),' ') )),
				[StepTherapy_Footnote] = LTRIM(RTRIM(REPLACE(REPLACE([StepTherapy_Footnote],CHAR(13),' '),CHAR(10),' ') ))
			FROM @Get_CF_GENENTECH_FLAT_MCNV
			WHERE 
				(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				AND

				(	
					( (ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) >= 2) )
					OR 
					( (ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))
				)

				AND

				(Step_Therapy_Req COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END
		ELSE
			BEGIN
				
				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'DNA_Healthplan_Management',
					InvalidValue = DNA_Healthplan_Management
				FROM @Get_CF_GENENTECH_FLAT_MCNV
				WHERE	(DNA_Healthplan_Management IS NULL)
						OR
						(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Placement',
					InvalidValue = Step_Therapy_Placement
				FROM @Get_CF_GENENTECH_FLAT_MCNV
				WHERE	(Step_Therapy_Placement IS NULL)
						OR
						((ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) < 2))
						OR 
						((ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Req',
					InvalidValue = Step_Therapy_Req
				FROM @Get_CF_GENENTECH_FLAT_MCNV
				WHERE	(Step_Therapy_Req IS NULL)
						OR
						(Step_Therapy_Req COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

				
			END

END

GO