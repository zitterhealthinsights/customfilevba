--USE [CWP_3.0]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_GENENTECH_PBM_HEMO]    Script Date: 8/24/2018 8:58:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Data:    EXEC usp_CF_GENENTECH_PBM_HEMO 2630, 'Hemo'
-- Data:    EXEC usp_CF_GENENTECH_PBM_HEMO 2630, 'Hemo', 0
-- Errors:  EXEC usp_CF_GENENTECH_PBM_HEMO 2630, 'Hemo', 1
ALTER PROCEDURE [dbo].[usp_CF_GENENTECH_PBM_HEMO]
	@Userid INT,
	@IndOrDrug VARCHAR(50),
	@ReturnErrors BIT = 0
AS
BEGIN

	DECLARE @Get_CF_GENENTECH_PBM_HEMO as CF_GENENTECH_PBM_HEMO
		
	DECLARE @selectedFields as varchar(max)
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_GENENTECH_PBM_HEMO','');
		
	INSERT INTO @Get_CF_GENENTECH_PBM_HEMO
	EXEC dbo.usp_Get_Plans
		@IndicationorDrug = @IndOrDrug,
		@Userid = @Userid,
		@isPBMOnly = 1,
		@SelectFields = @selectedFields




	IF @ReturnErrors = 0
		BEGIN
			SELECT
				[Data_Extraction_Date] = CONVERT(char(10), GetDate(),126), -- ISO8601	yyyy-mm-ddThh:mi:ss.mmm (no spaces)

				[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
				--[PBM_TotalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Total_Lives],CHAR(13),' '),CHAR(10),' ') )),
				--[PBM_ControlledFullyInsuredPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Controlled_Lives],CHAR(13),' '),CHAR(10),' ') )),
				--[PBM_ClaimsLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Claims_Lives],CHAR(13),' '),CHAR(10),' ') )),
				--[PBM_SelfInsuredPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Self_Insured_Lives],CHAR(13),' '),CHAR(10),' ') )),
				[Segment] = dbo.udf_CF_GetSegmentFromPbmName(LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') ))),
				[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
				[DRUG_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
				[ON_FORMULARY] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
				[PREFERRED_IMPLEMENTATION] = LTRIM(RTRIM(REPLACE(REPLACE([Preferred_Implementation],CHAR(13),' '),CHAR(10),' ') )),
				[PREFERRED_STATUS_BY_ADMIN_METHOD] = LTRIM(RTRIM(REPLACE(REPLACE([Preferred_Status_By_Admin_Method],CHAR(13),' '),CHAR(10),' ') )),
				[PREFERRED_IMPLEMENTATION_BY_ADMIN_METHOD] = LTRIM(RTRIM(REPLACE(REPLACE([Preferred_implementation_By_Admin_Method],CHAR(13),' '),CHAR(10),' ') )),
				[DNA_HEALTHPLAN_MANAGEMENT] = LTRIM(RTRIM(REPLACE(REPLACE([DNA_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
				[BENEFIT_TYPE] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
				[PA_REQUIRED] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
				[URL_TO_PA_POLICY] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[URL_TO_PA_FORM] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[GENERAL_PA_FORM] = LTRIM(RTRIM(REPLACE(REPLACE([General_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[URL_TO_DRAFT_POLICY] = LTRIM(RTRIM(REPLACE(REPLACE([URL_To_Draft_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[PA_MANAGEMENT] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Management],CHAR(13),' '),CHAR(10),' ') )),
				[TIER_PLACEMENT] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[NUMBER_OF_TIERS] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
				[PLAN_CLASSIFICATION] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Classification],CHAR(13),' '),CHAR(10),' ') )),
				--[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Description_In_Bethesda_units_ml_if_available],CHAR(13),' '),CHAR(10),' ') )),
				--[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				--[Initial_Auth_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
				[INITIAL_AUTH_TIME_LENGTH] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				--[Recert_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
				[RECERT_TIME_LENGTH] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[STEP_THERAPY_REQ] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
				[STEP_THERAPY_PLACEMENT] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[NUMBER_OF_STEPS] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
				[SPECIFIC_PA_CRITERIA_DETAILS] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
				[AGE_LIMIT] = LTRIM(RTRIM(REPLACE(REPLACE([Age_Limit],CHAR(13),' '),CHAR(10),' ') )),
				[HEMOPHILIA_TYPE_LIMITATIONS] = LTRIM(RTRIM(REPLACE(REPLACE([Hemophilia_Type_limitations],CHAR(13),' '),CHAR(10),' ') )),
				[HEMOPHILIA_TYPE_DESCRIPTIONS] = LTRIM(RTRIM(REPLACE(REPLACE([Hemophilia_Type_Description],CHAR(13),' '),CHAR(10),' ') )),
				[FACTOR_TYPE] = LTRIM(RTRIM(REPLACE(REPLACE([Factor_Type],CHAR(13),' '),CHAR(10),' ') )),
				[PERIOPERATIVE_MANAGEMENT_COVERED] = LTRIM(RTRIM(REPLACE(REPLACE([Perioperative_management_covered],CHAR(13),' '),CHAR(10),' ') )),
				[PERIOPERATIVE_MANAGEMENT_DESCRIPTION] = LTRIM(RTRIM(REPLACE(REPLACE([Perioperative_Management_Description],CHAR(13),' '),CHAR(10),' ') )),
				[PRIMARY_PROPHYLACTIC_THERAPY_COVERED] = LTRIM(RTRIM(REPLACE(REPLACE([Primary_Prophylactic_Therapy_Covered],CHAR(13),' '),CHAR(10),' ') )),
				[PRIMARY_PROPHYLACTIC_THERAPY_DESCRIPTION] = LTRIM(RTRIM(REPLACE(REPLACE([Primary_Prophylactic_Therapy_Description],CHAR(13),' '),CHAR(10),' ') )),
				[SECONDARY_PROPHYLACTIC_THERAPY_COVERED] = LTRIM(RTRIM(REPLACE(REPLACE([Secondary_Prophylactic_Therapy_Covered],CHAR(13),' '),CHAR(10),' ') )),
				[SECONDARY_PROPHYLACTIC_THERAPY_DESCRIPTION] = LTRIM(RTRIM(REPLACE(REPLACE([Secondary_Prophylactic_Therapy_Description],CHAR(13),' '),CHAR(10),' ') )),
				[SECONDARY_SHORT-TERM_PROPHYLAXIS_COVERED] = LTRIM(RTRIM(REPLACE(REPLACE([Secondary_Short_Term_Prophylaxis_covered],CHAR(13),' '),CHAR(10),' ') )),
				[OTHER_INDICATIONS_COVERED] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Indications_Covered],CHAR(13),' '),CHAR(10),' ') )),
				[OTHER_INDICATIONS_DESCRIPTION] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Indications_Description],CHAR(13),' '),CHAR(10),' ') )),
				[MANDATES_ON_CONCOMITANT_THERAPIES] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
				[MANDATES_ON_CONCOMITANT_THERAPIES_DESC] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[PROHIBITS_SPECIFIED_CONCOMITANT_THERAPIES] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
				[PROHIBITS_SPECIFIED_CONCOMITANT_THERAPIES_DESC] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies_Description],CHAR(13),' '),CHAR(10),' ') )),
				[INHIBITOR_LEVEL_RESTRICTIONS] = LTRIM(RTRIM(REPLACE(REPLACE([Inhibitor_Level_Restrictions],CHAR(13),' '),CHAR(10),' ') )),
				[INHIBITOR_LEVEL_RESTRICTIONS_DESCRIPTION] = LTRIM(RTRIM(REPLACE(REPLACE([Inhibitor_Level_Restrictions_Description],CHAR(13),' '),CHAR(10),' ') )),
				[DOCUMENTATION_OF_INHIBITOR_LEVELS_REQUIRED] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_of_Inhibitor_Levels_Required],CHAR(13),' '),CHAR(10),' ') )),
				[DOCUMENTATION_OF_INHIBITOR_LEVELS_DESCRIPTION] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_of_Inhibitor_Levels_Description],CHAR(13),' '),CHAR(10),' ') )),
				[DOCUMENTATION_OF_FVIII_LEVELS_REQUIRED] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_of_FVIII_Levels_Required],CHAR(13),' '),CHAR(10),' ') )),
				[DOCUMENTATION_OF_FVIII_LEVELS_REQUIRED_DESCRIPTION] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_of_FVIII_Levels_Description],CHAR(13),' '),CHAR(10),' ') )),
				[OTHER_LAB_REQUIREMENTS] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[ADMINISTRATIVE_REQUIREMENTS] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[SEPARATE_PHARMACY_AND_MEDICAL_POLICY] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[SEPARATE_PHARMACY_AND_MEDICAL_POLICY_CRITERIA] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_Criteria],CHAR(13),' '),CHAR(10),' ') )),
				[SEPARATE_PHARMACY_AND_MEDICAL_POLICY_URL] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_URL],CHAR(13),' '),CHAR(10),' ') )),
				[PROOF_OF_EFFECTIVENESS_REQUIRED_SUMMARY] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
				[PROOFOFEFFICACY] = LTRIM(RTRIM(REPLACE(REPLACE([ProofOfEfficacy],CHAR(13),' '),CHAR(10),' ') )),
				[J_CODES_APPROVED] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[ICD_CODES_APPROVED] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[SPECIALIST_APPROVAL] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Approval],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_INDICATED] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_CONTROL] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
				[DISTRIBUTION_LIMITATIONS_ENFORCED] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
				[NAME_OF_SPECIALTY_DRUG_DISTRIBUTER_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
				[NAME_OF_SPECIALTY_DRUG_DISTRIBUTER_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
				[OTHER_POLICY_UTILIZED] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Policy_Utilized],CHAR(13),' '),CHAR(10),' ') )),
				[DOCUMENTATION_SOURCE] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
				[CHANGE_TO_ENTRY] = LTRIM(RTRIM(REPLACE(REPLACE([Change_to_Entry],CHAR(13),' '),CHAR(10),' ') )),
				--[Reason_for_Change] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change],CHAR(13),' '),CHAR(10),' ') )),
				[Change_in_Coverage] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change_Details],CHAR(13),' '),CHAR(10),' ') )),
				[POLICY_DATE] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[RENEWAL_DATE] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[NO_POLICY_DATE_FOUND] = LTRIM(RTRIM(REPLACE(REPLACE([No_policy_date_found],CHAR(13),' '),CHAR(10),' ') )),
				[ENTRY_DATE] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[NOTE_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
				[NOTE_2] = LTRIM(RTRIM(REPLACE(REPLACE([Note_2],CHAR(13),' '),CHAR(10),' ') )),
				[NOTE_3] = LTRIM(RTRIM(REPLACE(REPLACE([Note_3],CHAR(13),' '),CHAR(10),' ') )),
				[CATEGORIZATION_DETAIL] = LTRIM(RTRIM(REPLACE(REPLACE([Categorization_Detail],CHAR(13),' '),CHAR(10),' ') ))
			FROM @Get_CF_GENENTECH_PBM_HEMO
			WHERE 
				(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				AND

				(	
					( (ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) >= 2) )
					OR 
					( (ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))
				)

				AND

				(Step_Therapy_Req COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END
		ELSE
			BEGIN
				
				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'DNA_Healthplan_Management',
					InvalidValue = DNA_Healthplan_Management
				FROM @Get_CF_GENENTECH_PBM_HEMO
				WHERE	(DNA_Healthplan_Management IS NULL)
						OR
						(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Placement',
					InvalidValue = Step_Therapy_Placement
				FROM @Get_CF_GENENTECH_PBM_HEMO
				WHERE	(Step_Therapy_Placement IS NULL)
						OR
						((ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) < 2))
						OR 
						((ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Req',
					InvalidValue = Step_Therapy_Req
				FROM @Get_CF_GENENTECH_PBM_HEMO
				WHERE	(Step_Therapy_Req IS NULL)
						OR
						(Step_Therapy_Req COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

				
			END

END

GO