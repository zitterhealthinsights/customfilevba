CREATE PROCEDURE [dbo].[usp_CF_NOVARTIS_CONSOLIDATED_STATE]
AS
BEGIN

	DECLARE @BatchID INT
	DECLARE @ErrorsPresent BIT = 0

	DECLARE @IndicationAbbreviation VARCHAR(20)
	DECLARE @Option VARCHAR(20)
	DECLARE @SpName NVARCHAR(100)

	DECLARE @LogID INT
	DECLARE @OldRowsCount INT
	DECLARE @NewRowsCount INT

	
	
	-- Delete results table
	DELETE FROM NovartisConsolidatedStateResults

	-- Get ID for this run
	SET @BatchID = [dbo].[udf_CF_NOVARTIS_CONSOLIDATED_STATE_GetNextBatchID] ()



	-- Loop by Indication and Flat/Pbm
	DECLARE crs CURSOR FOR
			SELECT [IndicationAbbreviation], [Option]
			FROM NovartisConsolidatedStateIndicationsToExecute
			ORDER BY [SeqNo] ASC

	open crs
	FETCH NEXT FROM crs INTO @IndicationAbbreviation, @Option
	WHILE @@FETCH_STATUS = 0
		BEGIN
			
			-- Insert starting record to Log
			INSERT INTO NovartisConsolidatedStateLog (BatchID, RunDate, IndicationAbbreviation, [Option], Status)
				VALUES (@BatchID, GETDATE(), @IndicationAbbreviation, @Option, 'Started')
			SELECT @LogID = IDENT_CURRENT('NovartisConsolidatedStateLog')

			-- Keep count of records in the result table
			SELECT @OldRowsCount = [dbo].[udf_GetTablesRowCount]('NovartisConsolidatedStateResults')

			BEGIN TRY
				-- Execute SP by name
				SET @SpName = 'usp_CF_NOVARTIS_CONSOLIDATED_' + @Option + '_' + @IndicationAbbreviation
				EXECUTE sp_executesql @SpName
				
				SELECT @NewRowsCount = [dbo].[udf_GetTablesRowCount]('NovartisConsolidatedStateResults')
		
				-- Update Log: success
				UPDATE NovartisConsolidatedStateLog
					SET ResultRowsCount = @NewRowsCount - @OldRowsCount,
						Status = 'OK',
						StatusDetail = ''
				WHERE LogID = @LogID
			END TRY

			BEGIN CATCH

				SELECT @NewRowsCount = [dbo].[udf_GetTablesRowCount]('NovartisConsolidatedStateResults')

				-- Update Log: error
				UPDATE NovartisConsolidatedStateLog
					SET ResultRowsCount = @NewRowsCount - @OldRowsCount,
						Status = 'Error',
						StatusDetail = ERROR_MESSAGE()
				WHERE LogID = @LogID

				SET @ErrorsPresent = 1
			END CATCH

			FETCH NEXT FROM crs INTO @IndicationAbbreviation, @Option
		END 
	CLOSE crs
	DEALLOCATE crs


	
	-- Final output, depending on success
	IF @ErrorsPresent = 0
		BEGIN
			SELECT * FROM NovartisConsolidatedStateResults
		END
	ELSE
		BEGIN
			RAISERROR('Results are invalid', 16, 1,'','');
		END

END