CREATE TABLE NovartisConsolidatedStateIndicationsToExecute (
	[IndicationAbbreviation] [varchar](20) NOT NULL,
	[Option] [varchar](20) NOT NULL,
	[SeqNo] INT NOT NULL,
)

INSERT INTO NovartisConsolidatedStateIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('AML', 'STATE', 10)
INSERT INTO NovartisConsolidatedStateIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('BC',  'STATE', 20)
INSERT INTO NovartisConsolidatedStateIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('CHF', 'STATE', 30)
INSERT INTO NovartisConsolidatedStateIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('CML', 'STATE', 40)
INSERT INTO NovartisConsolidatedStateIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('ITP', 'STATE', 50)
INSERT INTO NovartisConsolidatedStateIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('MEL', 'STATE', 60)
INSERT INTO NovartisConsolidatedStateIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('RCC', 'STATE', 70)
INSERT INTO NovartisConsolidatedStateIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('SAA', 'STATE', 80)
INSERT INTO NovartisConsolidatedStateIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('TSC', 'STATE', 90)


SELECT * FROM NovartisConsolidatedStateIndicationsToExecute