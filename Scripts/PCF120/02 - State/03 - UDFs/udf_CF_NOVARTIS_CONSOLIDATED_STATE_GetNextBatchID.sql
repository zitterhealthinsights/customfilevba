CREATE FUNCTION [dbo].[udf_CF_NOVARTIS_CONSOLIDATED_STATE_GetNextBatchID] ()

RETURNS INT

AS

BEGIN

	DECLARE @MaxBatchID INT

	IF EXISTS (SELECT 1 FROM NovartisConsolidatedStateLog)
		BEGIN
			SELECT @MaxBatchID = MAX(BatchID) FROM NovartisConsolidatedStateLog
		END
	ELSE
		BEGIN
			SET @MaxBatchID = 0
		END

	RETURN @MaxBatchID + 1

END

GO


