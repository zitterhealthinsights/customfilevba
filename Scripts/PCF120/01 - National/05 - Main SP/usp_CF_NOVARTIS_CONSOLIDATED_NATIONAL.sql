CREATE PROCEDURE [dbo].[usp_CF_NOVARTIS_CONSOLIDATED_NATIONAL]
AS
BEGIN

	DECLARE @BatchID INT
	DECLARE @ErrorsPresent BIT = 0

	DECLARE @IndicationAbbreviation VARCHAR(20)
	DECLARE @Option VARCHAR(20)
	DECLARE @SpName NVARCHAR(100)

	DECLARE @LogID INT
	DECLARE @OldRowsCount INT
	DECLARE @NewRowsCount INT

	
	
	-- Delete results table
	DELETE FROM NovartisConsolidatedNationalResults

	-- Get ID for this run
	SET @BatchID = [dbo].[udf_CF_NOVARTIS_CONSOLIDATED_NATIONAL_GetNextBatchID] ()



	-- Loop by Indication and Flat/Pbm
	DECLARE crs CURSOR FOR
			SELECT [IndicationAbbreviation], [Option]
			FROM NovartisConsolidatedNationalIndicationsToExecute
			ORDER BY [SeqNo] ASC

	open crs
	FETCH NEXT FROM crs INTO @IndicationAbbreviation, @Option
	WHILE @@FETCH_STATUS = 0
		BEGIN
			
			-- Insert starting record to Log
			INSERT INTO NovartisConsolidatedNationalLog (BatchID, RunDate, IndicationAbbreviation, [Option], Status)
				VALUES (@BatchID, GETDATE(), @IndicationAbbreviation, @Option, 'Started')
			SELECT @LogID = IDENT_CURRENT('NovartisConsolidatedNationalLog')

			-- Keep count of records in the result table
			SELECT @OldRowsCount = [dbo].[udf_GetTablesRowCount]('NovartisConsolidatedNationalResults')

			BEGIN TRY
				-- Execute SP by name
				SET @SpName = 'usp_CF_NOVARTIS_CONSOLIDATED_NATIONAL_' + @Option + '_' + @IndicationAbbreviation
				EXECUTE sp_executesql @SpName
				
				SELECT @NewRowsCount = [dbo].[udf_GetTablesRowCount]('NovartisConsolidatedNationalResults')
		
				-- Update Log: success
				UPDATE NovartisConsolidatedNationalLog
					SET ResultRowsCount = @NewRowsCount - @OldRowsCount,
						Status = 'OK',
						StatusDetail = ''
				WHERE LogID = @LogID
			END TRY

			BEGIN CATCH

				SELECT @NewRowsCount = [dbo].[udf_GetTablesRowCount]('NovartisConsolidatedNationalResults')

				-- Update Log: error
				UPDATE NovartisConsolidatedNationalLog
					SET ResultRowsCount = @NewRowsCount - @OldRowsCount,
						Status = 'Error',
						StatusDetail = ERROR_MESSAGE()
				WHERE LogID = @LogID

				SET @ErrorsPresent = 1
			END CATCH

			FETCH NEXT FROM crs INTO @IndicationAbbreviation, @Option
		END 
	CLOSE crs
	DEALLOCATE crs


	
	-- Final output, depending on success
	IF @ErrorsPresent = 0
		BEGIN
			SELECT * FROM NovartisConsolidatedNationalResults
		END
	ELSE
		BEGIN
			---SELECT * FROM NovartisConsolidatedNationalLog WHERE (BatchID = @BatchID) AND (Status <> 'OK')
			RAISERROR('Results are invalid', 16, 1,'','');
		END

END