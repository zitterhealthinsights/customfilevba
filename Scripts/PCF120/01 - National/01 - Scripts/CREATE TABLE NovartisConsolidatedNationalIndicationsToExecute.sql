CREATE TABLE NovartisConsolidatedNationalIndicationsToExecute (
	[IndicationAbbreviation] [varchar](20) NOT NULL,
	[Option] [varchar](20) NOT NULL,
	[SeqNo] INT NOT NULL,
)

INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('AML', 'FLAT', 10)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('AML', 'PBM',  20)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('BC',  'FLAT', 30)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('BC',  'PBM',  40)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('CHF', 'FLAT', 50)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('CHF', 'PBM',  60)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('CML', 'FLAT', 70)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('CML', 'PBM',  80)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('ITP', 'FLAT', 90)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('ITP', 'PBM',  100)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('MEL', 'FLAT', 110)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('MEL', 'PBM',  120)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('RCC', 'FLAT', 130)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('RCC', 'PBM',  140)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('SAA', 'FLAT', 150)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('SAA', 'PBM',  160)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('TSC', 'FLAT', 170)
INSERT INTO NovartisConsolidatedNationalIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('TSC', 'PBM',  180)


SELECT * FROM NovartisConsolidatedNationalIndicationsToExecute