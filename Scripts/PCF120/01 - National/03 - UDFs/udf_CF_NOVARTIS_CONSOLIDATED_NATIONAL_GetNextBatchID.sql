CREATE FUNCTION [dbo].[udf_CF_NOVARTIS_CONSOLIDATED_NATIONAL_GetNextBatchID] ()

RETURNS INT

AS

BEGIN

	DECLARE @MaxBatchID INT

	IF EXISTS (SELECT 1 FROM NovartisConsolidatedNationalLog)
		BEGIN
			SELECT @MaxBatchID = MAX(BatchID) FROM NovartisConsolidatedNationalLog
		END
	ELSE
		BEGIN
			SET @MaxBatchID = 0
		END

	RETURN @MaxBatchID + 1

END

GO


