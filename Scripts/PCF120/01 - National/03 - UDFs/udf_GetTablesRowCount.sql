CREATE FUNCTION [dbo].[udf_GetTablesRowCount] (
@TableName sysname -- It is basically the same as using nvarchar(128) NOT NULL
)

RETURNS INT

AS

BEGIN

	DECLARE @RowsCount INT = 0
	
	--https://www.brentozar.com/archive/2014/02/count-number-rows-table-sql-server/
	SELECT TOP 1 @RowsCount = x.[rows] FROM 
		(
			SELECT SUM(row_count) AS rows
			FROM sys.dm_db_partition_stats
			WHERE object_id = OBJECT_ID(@TableName)
			AND index_id < 2
			GROUP BY OBJECT_NAME(object_id)
		) x

	RETURN @RowsCount

END

GO


