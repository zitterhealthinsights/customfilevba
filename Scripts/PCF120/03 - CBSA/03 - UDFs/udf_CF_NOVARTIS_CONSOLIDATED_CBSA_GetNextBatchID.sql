CREATE FUNCTION [dbo].[udf_CF_NOVARTIS_CONSOLIDATED_CBSA_GetNextBatchID] ()

RETURNS INT

AS

BEGIN

	DECLARE @MaxBatchID INT

	IF EXISTS (SELECT 1 FROM NovartisConsolidatedCbsaLog)
		BEGIN
			SELECT @MaxBatchID = MAX(BatchID) FROM NovartisConsolidatedCbsaLog
		END
	ELSE
		BEGIN
			SET @MaxBatchID = 0
		END

	RETURN @MaxBatchID + 1

END

GO


