CREATE TABLE NovartisConsolidatedCbsaIndicationsToExecute (
	[IndicationAbbreviation] [varchar](20) NOT NULL,
	[Option] [varchar](20) NOT NULL,
	[SeqNo] INT NOT NULL,
)

INSERT INTO NovartisConsolidatedCbsaIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('AML', 'CBSA', 10)
INSERT INTO NovartisConsolidatedCbsaIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('BC',  'CBSA', 20)
INSERT INTO NovartisConsolidatedCbsaIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('CHF', 'CBSA', 30)
INSERT INTO NovartisConsolidatedCbsaIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('CML', 'CBSA', 40)
INSERT INTO NovartisConsolidatedCbsaIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('ITP', 'CBSA', 50)
INSERT INTO NovartisConsolidatedCbsaIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('MEL', 'CBSA', 60)
INSERT INTO NovartisConsolidatedCbsaIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('RCC', 'CBSA', 70)
INSERT INTO NovartisConsolidatedCbsaIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('SAA', 'CBSA', 80)
INSERT INTO NovartisConsolidatedCbsaIndicationsToExecute (IndicationAbbreviation, [Option], SeqNo) VALUES('TSC', 'CBSA', 90)


SELECT * FROM NovartisConsolidatedCbsaIndicationsToExecute