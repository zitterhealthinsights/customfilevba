CREATE TABLE NovartisConsolidatedCbsaLog (
	[LogID] INT IDENTITY(1,1) NOT NULL,
	[BatchID] INT NOT NULL,
	[RunDate] DATE NOT NULL,
	[IndicationAbbreviation] [varchar](20) NOT NULL,
	[Option] [varchar](20) NOT NULL,
	[ResultRowsCount] INT NULL,
	[Status] [varchar](50) NULL,
	[StatusDetail] [varchar](max) NULL
)