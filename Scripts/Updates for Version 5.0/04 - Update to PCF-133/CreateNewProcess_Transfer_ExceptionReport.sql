-- Create Category
--Insert into Category VALUES ('Exception Report', 1)
--Category 35
--select * from category order by 1 desc



--Create Process
--Insert into Process 
--Values ('Genentech Exception Report',2,NULL, NULL, 35, 9,1,'C:\Temp\')

--select * from process order by 1 desc
--Process ID = 44

--Process Detail 
---(Only and update is required,
-- as we can simply move the existing EXCEPTION REPORT to another 
--process id in this case # 44

--Update processdetail 
--set ProcessID = 44
--where processdetailid = 85

--Verify
select * from ProcessDetail where processid = 44
 order by SequenceNumber


 --Old Process no longer has it
 select * from ProcessDetail 
 where processid = 27 order by SequenceNumber