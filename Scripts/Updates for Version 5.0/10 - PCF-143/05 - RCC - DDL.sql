--------------------------------------------------------------1. UDTs------------------------------------------------------------------
IF TYPE_ID(N'CF_GENENTECH_FLAT_RCC') IS NOT NULL
	DROP TYPE [dbo].[CF_GENENTECH_FLAT_RCC]
GO

CREATE TYPE [dbo].[CF_GENENTECH_FLAT_RCC] AS TABLE(
	[Mcoid] [varchar](max) NULL,
	[PayerName] [varchar](max) NULL,
	[Segment] [varchar](max) NULL,
	[Plan_Name] [varchar](max) NULL,
	[Indication] [varchar](max) NULL,
	[Drug_Name] [varchar](max) NULL,
	[On_Formulary] [varchar](max) NULL,
	[DNA_Healthplan_Management] [varchar](max) NULL,
	[Benefit_Type] [varchar](max) NULL,
	[PA_Required] [varchar](max) NULL,
	[URL_to_PA_Policy] [varchar](max) NULL,
	[URL_to_PA_Form] [varchar](max) NULL,
	[General_PA_Form] [varchar](max) NULL,
	[Tier_Placement] [varchar](max) NULL,
	[Number_of_Tiers] [varchar](max) NULL,
	[Compendium_Referenced] [varchar](max) NULL,
	[Evidence_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Compendium_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Compendium_Evidence_Lvl_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[PR_Journal_Articles_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Clinical_Pathway_Utilized] [varchar](max) NULL,
	[Off_Label_Use_Indications] [varchar](max) NULL,
	[Dosing_Limitation] [varchar](max) NULL,
	[Dosing_Limitation_Desc] [varchar](max) NULL,
	[Quantity_Limitation] [varchar](max) NULL,
	[Quantity_Limitation_Desc] [varchar](max) NULL,
	[Initial_Auth_Time_Length] [varchar](max) NULL,
	[Recert_Time_Length] [varchar](max) NULL,
	[Step_Therapy_Req] [varchar](max) NULL,
	[Step_Therapy_Placement] [varchar](max) NULL,
	[Number_of_Steps] [varchar](max) NULL,
	[Step_Therapy_Notes] [varchar](max) NULL,
	[Specific_PA_Criteria_details] [varchar](max) NULL,
	[Limitation_on_Cancer_Type] [varchar](max) NULL,
	[Limitation_on_Cancer_Type_Desc] [varchar](max) NULL,
	[Place_in_Therapy] [varchar](max) NULL,
	[Mandates_on_Concomitant_Therapies] [varchar](max) NULL,
	[Mandates_on_Concomitant_Therapies_Desc] [varchar](max) NULL,
	[Specific_Combination_Therapy] [varchar](max) NULL,
	[Prohibits_Specified_Concomitant_Therapies] [varchar](max) NULL,
	[Prohibits_Specified_Concomitant_Therapies_Desc] [varchar](max) NULL,
	[Lab_Requirements] [varchar](max) NULL,
	[Administrative_Requirements] [varchar](max) NULL,
	[Proof_of_Effectiveness_Required_Summary] [varchar](max) NULL,
	[Proof_of_Effectiveness_Req_for_Continued_Appr] [varchar](max) NULL,
	[Competitor_Drugs] [varchar](max) NULL,
	[J_Codes_Approved] [varchar](max) NULL,
	[ICD_Codes_Approved] [varchar](max) NULL,
	[Specialist_Appr] [varchar](max) NULL,
	[PBM_Indicated] [varchar](max) NULL,
	[PBM_Control] [varchar](max) NULL,
	[Distribution_Limitations_Enforced] [varchar](max) NULL,
	[Name_of_Specialty_Drug_Distributer_1] [varchar](max) NULL,
	[Name_of_Specialty_Drug_Distributer_2] [varchar](max) NULL,
	[Documentation_Source] [varchar](max) NULL,
	[Policy_Date] [varchar](max) NULL,
	[Renewal_Date] [varchar](max) NULL,
	[Note_1] [varchar](max) NULL,
	[Categorization_Detail] [varchar](max) NULL
)
GO



--------------------------------------------------------------2. SPs------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM sys.objects WHERE type = 'P' AND name = 'usp_CF_GENENTECH_FLAT_RCC')
	DROP PROCEDURE [dbo].[usp_CF_GENENTECH_FLAT_RCC]
GO

-- Data:    EXEC usp_CF_GENENTECH_FLAT_RCC 2630, 'RCC'
-- Data:    EXEC usp_CF_GENENTECH_FLAT_RCC 2630, 'RCC', 0
-- Errors:  EXEC usp_CF_GENENTECH_FLAT_RCC 2630, 'RCC', 1
CREATE PROCEDURE [dbo].[usp_CF_GENENTECH_FLAT_RCC]
	@Userid INT,
	@IndOrDrug VARCHAR(50),
	@ReturnErrors BIT = 0
AS
BEGIN

	DECLARE @Get_CF_GENENTECH_FLAT_RCC as CF_GENENTECH_FLAT_RCC
		
	DECLARE @selectedFields as varchar(max)
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_GENENTECH_FLAT_RCC','');
		
	INSERT INTO @Get_CF_GENENTECH_FLAT_RCC
	EXEC dbo.usp_Get_Plans
		@IndicationorDrug = @IndOrDrug,
		@Userid = @Userid,
		@isPBMOnly = 0,
		@SelectFields = @selectedFields

	IF @ReturnErrors = 0
		BEGIN	
			SELECT
				[Data_Extraction_Date] = CONVERT(char(10), GetDate(),126), -- ISO8601	yyyy-mm-ddThh:mi:ss.mmm (no spaces)
				[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
				[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
				[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
				[Plan_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
				[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
				[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
				[On_Formulary] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
				[DNA_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([DNA_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
				[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
				[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[General_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([General_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Referenced] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Referenced],CHAR(13),' '),CHAR(10),' ') )),
				[Evidence_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Evidence_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Evidence_Lvl_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Evidence_Lvl_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[PR_Journal_Articles_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([PR_Journal_Articles_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Clinical_Pathway_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Clinical_Pathway_Utilized],CHAR(13),' '),CHAR(10),' ') )),
				[Off_Label_Use_Indications] = LTRIM(RTRIM(REPLACE(REPLACE([Off_Label_Use_Indications],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_PA_Criteria_details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Cancer_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Cancer_Type],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Cancer_Type_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Cancer_Type_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Therapy],CHAR(13),' '),CHAR(10),' ') )),
				[Mandates_on_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
				[Mandates_on_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_Combination_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Combination_Therapy],CHAR(13),' '),CHAR(10),' ') )),
				[Prohibits_Specified_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
				[Prohibits_Specified_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Req_for_Continued_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Req_for_Continued_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Competitor_Drugs] = LTRIM(RTRIM(REPLACE(REPLACE([Competitor_Drugs],CHAR(13),' '),CHAR(10),' ') )),
				[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[Specialist_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
				[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
				[Name_Of_Specialty_Drug_Distributer_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
				[Name_Of_Specialty_Drug_Distributer_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
				[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
				[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
				[Categorization_Detail] = LTRIM(RTRIM(REPLACE(REPLACE([Categorization_Detail],CHAR(13),' '),CHAR(10),' ') ))
			FROM @Get_CF_GENENTECH_FLAT_RCC
			WHERE 
				(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				AND

				(	
					( (ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) >= 2) )
					OR 
					( (ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))
				)

				AND

				(Step_Therapy_Req COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END
		ELSE
			BEGIN
				
				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'DNA_Healthplan_Management',
					InvalidValue = DNA_Healthplan_Management
				FROM @Get_CF_GENENTECH_FLAT_RCC
				WHERE	(DNA_Healthplan_Management IS NULL)
						OR
						(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Placement',
					InvalidValue = Step_Therapy_Placement
				FROM @Get_CF_GENENTECH_FLAT_RCC
				WHERE	(Step_Therapy_Placement IS NULL)
						OR
						((ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) < 2))
						OR 
						((ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Req',
					InvalidValue = Step_Therapy_Req
				FROM @Get_CF_GENENTECH_FLAT_RCC
				WHERE	(Step_Therapy_Req IS NULL)
						OR
						(Step_Therapy_Req COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END

END

GO