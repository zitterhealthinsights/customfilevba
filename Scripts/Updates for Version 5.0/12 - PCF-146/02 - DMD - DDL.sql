--------------------------------------------------------------1. UDTs------------------------------------------------------------------
IF TYPE_ID(N'CF_GENENTECH_PBM_DMD') IS NOT NULL
	DROP TYPE [dbo].[CF_GENENTECH_PBM_DMD]
GO

CREATE TYPE [dbo].[CF_GENENTECH_PBM_DMD] AS TABLE(
	[Mcoid] [varchar](max) NULL,
	[Segment] [varchar](max) NULL,
	[Plan_Name] [varchar](max) NULL,
	[Indication] [varchar](max) NULL,
	[Drug_Name] [varchar](max) NULL,
	[On_Formulary] [varchar](max) NULL,
	[DNA_Healthplan_Management] [varchar](max) NULL,
	[Benefit_Type] [varchar](max) NULL,
	[PA_Required] [varchar](max) NULL,
	[URL_to_PA_Policy] [varchar](max) NULL,
	[URL_to_PA_Form] [varchar](max) NULL,
	[General_PA_Form] [varchar](max) NULL,
	[PA_Management] [varchar](max) NULL,
	[Tier_Placement] [varchar](max) NULL,
	[Number_of_Tiers] [varchar](max) NULL,
	[Dosing_Limitation] [varchar](max) NULL,
	[Dosing_Limitation_Desc] [varchar](max) NULL,
	[Quantity_Limitation] [varchar](max) NULL,
	[Quantity_Limitation_Desc] [varchar](max) NULL,
	[Initial_Auth_Time_Limit_Req] [varchar](max) NULL,
	[Initial_Auth_Time_Length] [varchar](max) NULL,
	[Recert_Time_Limit_Req] [varchar](max) NULL,
	[Recert_Time_Length] [varchar](max) NULL,
	[Step_Therapy_Req] [varchar](max) NULL,
	[Step_Therapy_Placement] [varchar](max) NULL,
	[Number_of_Steps] [varchar](max) NULL,
	[Step_Therapy_Notes] [varchar](max) NULL,
	[Specific_PA_Criteria_Details] [varchar](max) NULL,
	[Nbr_of_Steroid_Trials] [varchar](max) NULL,
	[Steroid_Trial_Length] [varchar](max) NULL,
	[Steroid_Opr] [varchar](max) NULL,
	[Age_Limit] [varchar](max) NULL,
	[Age_at_Initiation] [varchar](max) NULL,
	[Lab_Requirements] [varchar](max) NULL,
	[Ambulatory_Requirement] [varchar](max) NULL,
	[Ambulatory_Requirement_Description] [varchar](max) NULL,
	[Administrative_Requirements] [varchar](max) NULL,
	[Separate_Pharmacy_and_Medical_Policy] [varchar](max) NULL,
	[Separate_Pharmacy_and_Medical_Policy_Criteria] [varchar](max) NULL,
	[Separate_Pharmacy_and_Medical_Policy_URL] [varchar](max) NULL,
	[URL_To_Draft_Policy] [varchar](max) NULL,
	[Proof_of_Effectiveness_Required_Summary] [varchar](max) NULL,
	[Proof_of_Effectiveness_Required_for_Continued_Approval] [varchar](max) NULL,
	[J_Codes_Approved] [varchar](max) NULL,
	[ICD_Codes_Approved] [varchar](max) NULL,
	[Specialist_Approval] [varchar](max) NULL,
	[PBM_Indicated] [varchar](max) NULL,
	[Distribution_Limitations_Enforced] [varchar](max) NULL,
	[Name_Of_Specialty_Drug_Distributer_1] [varchar](max) NULL,
	[Name_Of_Specialty_Drug_Distributer_2] [varchar](max) NULL,
	[Other_Policy_Utilized] [varchar](max) NULL,
	[Documentation_Source] [varchar](max) NULL,
	[Change_to_Entry] [varchar](max) NULL,
	[Reason_For_Change] [varchar](max) NULL,
	[Reason_for_Change_Details] [varchar](max) NULL,
	[Policy_Date] [varchar](max) NULL,
	[Renewal_Date] [varchar](max) NULL,
	[No_Policy_Date_Found] [varchar](max) NULL,
	[Entry_Date] [varchar](max) NULL,
	[Note_1] [varchar](max) NULL,
	[Note_2] [varchar](max) NULL,
	[Note_3] [varchar](max) NULL

)
GO


--------------------------------------------------------------2. SPs------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM sys.objects WHERE type = 'P' AND name = 'usp_CF_GENENTECH_PBM_DMD')
	DROP PROCEDURE [dbo].[usp_CF_GENENTECH_PBM_DMD]
GO

-- Data:    EXEC usp_CF_GENENTECH_PBM_DMD 2630, 'DMD'
-- Data:    EXEC usp_CF_GENENTECH_PBM_DMD 2630, 'DMD', 0
-- Errors:  EXEC usp_CF_GENENTECH_PBM_DMD 2630, 'DMD', 1

CREATE PROCEDURE [dbo].[usp_CF_GENENTECH_PBM_DMD]
	@Userid INT,
	@IndOrDrug VARCHAR(50),
	@ReturnErrors BIT = 0
AS
BEGIN

	DECLARE @Get_CF_GENENTECH_PBM_DMD as CF_GENENTECH_PBM_DMD
		
	DECLARE @selectedFields as varchar(max)
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_GENENTECH_PBM_DMD','');
		
	INSERT INTO @Get_CF_GENENTECH_PBM_DMD
	EXEC dbo.usp_Get_Plans
		@IndicationorDrug = @IndOrDrug,
		@Userid = @Userid,
		@isPBMOnly = 1,
		@SelectFields = @selectedFields

	IF @ReturnErrors = 0
		BEGIN	
			SELECT
				[Data_Extraction_Date] = CONVERT(char(10), GetDate(),126), -- ISO8601	yyyy-mm-ddThh:mi:ss.mmm (no spaces)
				[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
				[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
				[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
				[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
				[On_Formulary] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
				[Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([DNA_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
				[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
				[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[General_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([General_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[PA_Management] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Management],CHAR(13),' '),CHAR(10),' ') )),
				[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Initial_Auth_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Recert_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Notes] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_PA_Criteria_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
				[Nbr_of_Steroid_Trials] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_of_Steroid_Trials],CHAR(13),' '),CHAR(10),' ') )),
				[Steroid_Trial_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Steroid_Trial_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Steroid_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Steroid_Opr],CHAR(13),' '),CHAR(10),' ') )),
				[Age_Limit] = LTRIM(RTRIM(REPLACE(REPLACE([Age_Limit],CHAR(13),' '),CHAR(10),' ') )),
				[Age_at_Initiation] = LTRIM(RTRIM(REPLACE(REPLACE([Age_at_Initiation],CHAR(13),' '),CHAR(10),' ') )),
				[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Ambulatory_Requirement] = LTRIM(RTRIM(REPLACE(REPLACE([Ambulatory_Requirement],CHAR(13),' '),CHAR(10),' ') )),
				[Ambulatory_Requirement_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Ambulatory_Requirement_Description],CHAR(13),' '),CHAR(10),' ') )),
				[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Separate_Pharmacy_and_Medical_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[Separate_Pharmacy_and_Medical_Policy_Criteria] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_Criteria],CHAR(13),' '),CHAR(10),' ') )),
				[Separate_Pharmacy_and_Medical_Policy_URL] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_URL],CHAR(13),' '),CHAR(10),' ') )),
				[URL_To_Draft_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_To_Draft_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Required_for_Continued_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_for_Continued_Approval],CHAR(13),' '),CHAR(10),' ') )),
				[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[Specialist_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Approval],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
				[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
				[Name_Of_Specialty_Drug_Distributer_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
				[Name_Of_Specialty_Drug_Distributer_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
				[Other_Policy_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Policy_Utilized],CHAR(13),' '),CHAR(10),' ') )),
				[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
				[Change_to_Entry] = LTRIM(RTRIM(REPLACE(REPLACE([Change_to_Entry],CHAR(13),' '),CHAR(10),' ') )),
				[Reason_For_Change] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_For_Change],CHAR(13),' '),CHAR(10),' ') )),
				[Reason_for_Change_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change_Details],CHAR(13),' '),CHAR(10),' ') )),
				[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[No_Policy_Date_Found] = LTRIM(RTRIM(REPLACE(REPLACE([No_Policy_Date_Found],CHAR(13),' '),CHAR(10),' ') )),
				[Entry_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
				[Note_2] = LTRIM(RTRIM(REPLACE(REPLACE([Note_2],CHAR(13),' '),CHAR(10),' ') )),
				[Note_3] = LTRIM(RTRIM(REPLACE(REPLACE([Note_3],CHAR(13),' '),CHAR(10),' ') ))

			FROM @Get_CF_GENENTECH_PBM_DMD
			WHERE 
				(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				AND

				(	
					( (ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) >= 2) )
					OR 
					( (ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))
				)

				AND

				(Step_Therapy_Req COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END
		ELSE
			BEGIN
				
				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'DNA_Healthplan_Management',
					InvalidValue = DNA_Healthplan_Management
				FROM @Get_CF_GENENTECH_PBM_DMD
				WHERE	(DNA_Healthplan_Management IS NULL)
						OR
						(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Placement',
					InvalidValue = Step_Therapy_Placement
				FROM @Get_CF_GENENTECH_PBM_DMD
				WHERE	(Step_Therapy_Placement IS NULL)
						OR
						((ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) < 2))
						OR 
						((ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Req',
					InvalidValue = Step_Therapy_Req
				FROM @Get_CF_GENENTECH_PBM_DMD
				WHERE	(Step_Therapy_Req IS NULL)
						OR
						(Step_Therapy_Req COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END

END

GO
