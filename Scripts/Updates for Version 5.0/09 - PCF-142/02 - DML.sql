
DECLARE @ProcessID INT = 27
DECLARE @IndicationUC VARCHAR(10) = 'UC'
DECLARE @IndicationCD VARCHAR(10) = 'CD'

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.Tables WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'ProcessDetail')
	AND EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.Tables WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'ClientIndication')
	AND EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.Tables WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'ClientDrugs')
BEGIN

	DECLARE @ProcessDetailIdUC INT = (SELECT ProcessDetailID FROM dbo.ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = @IndicationUC)
	DECLARE @ProcessDetailIdCD INT = (SELECT ProcessDetailID FROM dbo.ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = @IndicationCD)

	DECLARE @ClientIndicationIdUC INT = (SELECT ClientIndicationID FROM dbo.ClientIndication WHERE ProcessDetailID = @ProcessDetailIdUC)
	DECLARE @ClientIndicationIdCD INT = (SELECT ClientIndicationID FROM dbo.ClientIndication WHERE ProcessDetailID = @ProcessDetailIdCD)

	-- Delete records from ClientDrugs table
	DELETE FROM dbo.ClientDrugs WHERE ClientIndicationID IN (@ClientIndicationIdUC, @ClientIndicationIdCD)

	-- Delete records from ClientIndication table
	DELETE FROM dbo.ClientIndication WHERE ProcessDetailID IN (@ProcessDetailIdUC, @ProcessDetailIdCD)
		
	-- Delete UC and CD as separate steps
	DELETE FROM dbo.ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription IN (@IndicationUC, @IndicationCD)
END


