
DECLARE @IndicationID_HD INT = (SELECT IndicationID FROM Indication WHERE Abbreviation = 'HD')
DECLARE @ProcessID INT = 27

IF NOT EXISTS (SELECT 1 FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'HD')
	INSERT INTO ProcessDetail VALUES(@ProcessID, 110, 'HD', 1)


DECLARE @PdID_HD INT = (SELECT ProcessDetailID FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'HD')

IF NOT EXISTS (SELECT 1 FROM ClientIndication WHERE ClientID = 9 AND IndicationID = @IndicationID_HD)
	INSERT INTO ClientIndication VALUES (@PdID_HD, 9, @IndicationID_HD)


IF NOT EXISTS (SELECT 1 FROM ClientDrugs cd
INNER JOIN ClientIndication ci ON cd.ClientIndicationID = ci.ClientIndicationID
WHERE ci.ClientID = 9 AND ci.IndicationId = @IndicationID_HD)
	INSERT INTO ClientDrugs
	SELECT DISTINCT ci.ClientIndicationID, d.DrugID FROM ClientDrugIndication cdi
	INNER JOIN ClientIndication ci ON ci.ClientID = cdi.ClientID AND ci.IndicationID = cdi.IndicationID
	INNER JOIN DRUG d ON d.DrugID = cdi.DrugID
	WHERE cdi.ClientID = 9 AND cdi.IndicationId = @IndicationID_HD
	

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'tblQueryMasterUnion')
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Advisor_Recruitment_Needed' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Advisor_Recruitment_Needed', 'Advisor_Recruitment_Needed', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Advisor_Specific_Notes' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Advisor_Specific_Notes', 'Advisor_Specific_Notes', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Collection_date' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Collection_date', 'Collection_date', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Dashboard_HP' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Dashboard_HP', 'Dashboard_HP', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Default_Entry_Utilized' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Default_Entry_Utilized', 'Default_Entry_Utilized', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Evolution_Phase' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Evolution_Phase', 'Evolution_Phase', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Subtype_Varies' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Subtype_Varies', 'Subtype_Varies', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Trends' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Trends', 'Trends', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Use_for_Spotlights' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Use_for_Spotlights', 'Use_for_Spotlights', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Launch_Tracker_Flag' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Launch_Tracker_Flag', 'Launch_Tracker_Flag', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Manager_QC' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Manager_QC', 'Manager_QC', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Nbr_Of_Tiers_Reference' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Nbr_Of_Tiers_Reference', 'Nbr_Of_Tiers_Reference', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'NDLP_Used' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'NDLP_Used', 'NDLP_Used', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'New_drug_policy_details' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'New_drug_policy_details', 'New_drug_policy_details', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Off_Label_Policy_URL' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Off_Label_Policy_URL', 'Off_Label_Policy_URL', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Plan_Linkages' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Plan_Linkages', 'Plan_Linkages', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Preferred_HP_Mgt' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Preferred_HP_Mgt', 'Preferred_HP_Mgt', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'QC_Comment' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'QC_Comment', 'QC_Comment', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'QC_Flag' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'QC_Flag', 'QC_Flag', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Reason_for_PBM_Influence' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Reason_for_PBM_Influence', 'Reason_for_PBM_Influence', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Response_Type' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Response_Type', 'Response_Type', NULL, 5000)
	END
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'HD' AND FieldName = 'Genentech_Launch_Tracker_Healthplan_Management' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('HD', 'Genentech', 'Genentech_Launch_Tracker_Healthplan_Management', 'Genentech_Launch_Tracker_Healthplan_Management', NULL, 5000)
	END

END