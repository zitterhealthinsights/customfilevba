
DECLARE @IndicationID_DMD INT = (SELECT IndicationID FROM Indication WHERE Abbreviation = 'DMD')
DECLARE @ProcessID INT = 27

IF NOT EXISTS (SELECT 1 FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'DMD')
	INSERT INTO ProcessDetail VALUES(@ProcessID, 100, 'DMD', 1)


DECLARE @PdID_DMD INT = (SELECT ProcessDetailID FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'DMD')

IF NOT EXISTS (SELECT 1 FROM ClientIndication WHERE ClientID = 9 AND IndicationID = @IndicationID_DMD)
	INSERT INTO ClientIndication VALUES (@PdID_DMD, 9, @IndicationID_DMD)


IF NOT EXISTS (SELECT 1 FROM ClientDrugs cd
INNER JOIN ClientIndication ci ON cd.ClientIndicationID = ci.ClientIndicationID
WHERE ci.ClientID = 9 AND ci.IndicationId = @IndicationID_DMD)
	INSERT INTO ClientDrugs
	SELECT DISTINCT ci.ClientIndicationID, d.DrugID FROM ClientDrugIndication cdi
	INNER JOIN ClientIndication ci ON ci.ClientID = cdi.ClientID AND ci.IndicationID = cdi.IndicationID
	INNER JOIN DRUG d ON d.DrugID = cdi.DrugID
	WHERE cdi.ClientID = 9 AND cdi.IndicationId = @IndicationID_DMD


IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'tblQueryMasterUnion')
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE IndicationDrug = 'DMD' AND FieldName = 'DNA_Healthplan_Management' AND (Client = 'Genentech' OR Client = 'All'))
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('DMD', 'Genentech', 'Healthplan Management', 'DNA_Healthplan_Management', NULL, 2)
	END
END