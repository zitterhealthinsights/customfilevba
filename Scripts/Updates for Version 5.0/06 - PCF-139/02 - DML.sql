IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'tblQueryMasterUnion')
BEGIN
	IF NOT EXISTS (SELECT 1 FROM dbo.tblQueryMasterUnion WHERE Client = 'Genentech' AND IndicationDrug = 'RCC' AND FieldName = 'Categorization_Detail')
	BEGIN
		INSERT INTO dbo.tblQueryMasterUnion VALUES ('RCC', 'Genentech', 'Categorization Detail', 'Categorization_Detail', NULL, 10000)
	END
END