--------------------------------------------------------------1. UDTs------------------------------------------------------------------

-----------------------------------MM------------------------------
IF TYPE_ID(N'CF_GENENTECH_FLAT_MM') IS NOT NULL
	DROP TYPE [dbo].[CF_GENENTECH_FLAT_MM]
GO

CREATE TYPE [dbo].[CF_GENENTECH_FLAT_MM] AS TABLE(
	[Mcoid] [varchar](max) NULL,
	[PayerName] [varchar](max) NULL,
	[Segment] [varchar](max) NULL,
	[Plan_Name] [varchar](max) NULL,
	[Indication] [varchar](max) NULL,
	[Drug_Name] [varchar](max) NULL,
	[On_Formulary] [varchar](max) NULL,
	[DNA_Healthplan_Management] [varchar](max) NULL,
	[Benefit_Type] [varchar](max) NULL,
	[PA_Required] [varchar](max) NULL,
	[URL_to_PA_Policy] [varchar](max) NULL,
	[URL_to_PA_Form] [varchar](max) NULL,
	[Tier_Placement] [varchar](max) NULL,
	[Number_of_Tiers] [varchar](max) NULL,
	[Compendium_Referenced] [varchar](max) NULL,
	[Evidence_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Compendium_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Compendium_Evidence_Lvl_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[PR_Journal_Articles_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Clinical_Pathway_Utilized] [varchar](max) NULL,
	[Off_Label_Use_Indications] [varchar](max) NULL,
	[Dosing_Limitation] [varchar](max) NULL,
	[Dosing_Limitation_Desc] [varchar](max) NULL,
	[Quantity_Limitation] [varchar](max) NULL,
	[Quantity_Limitation_Desc] [varchar](max) NULL,
	[Initial_Auth_Time_Length] [varchar](max) NULL,
	[Recert_Time_Length] [varchar](max) NULL,
	[Step_Therapy_Req] [varchar](max) NULL,
	[Step_Therapy_Placement] [varchar](max) NULL,
	[Number_of_Steps] [varchar](max) NULL,
	[Step_Therapy_Notes] [varchar](max) NULL,
	[Specific_PA_Criteria_details] [varchar](max) NULL,
	[Limitation_on_Cancer_Type] [varchar](max) NULL,
	[Limitation_on_Cancer_Type_Desc] [varchar](max) NULL,
	[Place_in_Therapy] [varchar](max) NULL,
	[Place_in_Therapy_Desc] [varchar](max) NULL,
	[Place_in_Maintenance_Therapy] [varchar](max) NULL,
	[Place_in_Maintenance_Therapy_Desc] [varchar](max) NULL,
	[Mandates_on_Concomitant_Therapies] [varchar](max) NULL,
	[Mandates_on_Concomitant_Therapies_Desc] [varchar](max) NULL,
	[Specific_Combination_Therapy] [varchar](max) NULL,
	[Prohibits_Specified_Concomitant_Therapies] [varchar](max) NULL,
	[Prohibits_Specified_Concomitant_Therapies_Desc] [varchar](max) NULL,
	[Lab_Requirements] [varchar](max) NULL,
	[Administrative_Requirements] [varchar](max) NULL,
	[Proof_of_Effectiveness_Required_Summary] [varchar](max) NULL,
	[Proof_of_Effectiveness_Req_for_Continued_Appr] [varchar](max) NULL,
	[Limitation_on_Recycling] [varchar](max) NULL,
	[Competitor_Drugs] [varchar](max) NULL,
	[J_Codes_Approved] [varchar](max) NULL,
	[ICD_Codes_Approved] [varchar](max) NULL,
	[Specialist_Appr] [varchar](max) NULL,
	[PBM_Indicated] [varchar](max) NULL,
	[PBM_Control] [varchar](max) NULL,
	[Distribution_Limitations_Enforced] [varchar](max) NULL,
	[Name_of_Specialty_Drug_Distributer_1] [varchar](max) NULL,
	[Name_of_Specialty_Drug_Distributer_2] [varchar](max) NULL,
	[Documentation_Source] [varchar](max) NULL,
	[Policy_Date] [varchar](max) NULL,
	[Renewal_Date] [varchar](max) NULL,
	[Note_1] [varchar](max) NULL,
	[Categorization_Detail] [varchar](max) NULL
)
GO

IF TYPE_ID(N'CF_GENENTECH_PBM_MM') IS NOT NULL
	DROP TYPE [dbo].[CF_GENENTECH_PBM_MM]
GO

CREATE TYPE [dbo].[CF_GENENTECH_PBM_MM] AS TABLE(
	[Mcoid] [varchar](max) NULL,
	[Plan_Name] [varchar](max) NULL,
	[Segment] [varchar](max) NULL,
	[Indication] [varchar](max) NULL,
	[Drug_Name] [varchar](max) NULL,
	[On_Formulary] [varchar](max) NULL,
	[DNA_Healthplan_Management] [varchar](max) NULL,
	[Benefit_Type] [varchar](max) NULL,
	[PA_Required] [varchar](max) NULL,
	[URL_to_PA_Policy] [varchar](max) NULL,
	[URL_to_PA_Form] [varchar](max) NULL,
	[Tier_Placement] [varchar](max) NULL,
	[Number_of_Tiers] [varchar](max) NULL,
	[Compendium_Referenced] [varchar](max) NULL,
	[Evidence_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Compendium_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Compendium_Evidence_Lvl_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[PR_Journal_Articles_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Clinical_Pathway_Utilized] [varchar](max) NULL,
	[Off_Label_Use_Indications] [varchar](max) NULL,
	[Dosing_Limitation] [varchar](max) NULL,
	[Dosing_Limitation_Desc] [varchar](max) NULL,
	[Quantity_Limitation] [varchar](max) NULL,
	[Quantity_Limitation_Desc] [varchar](max) NULL,
	[Initial_Auth_Time_Length] [varchar](max) NULL,
	[Recert_Time_Length] [varchar](max) NULL,
	[Step_Therapy_Req] [varchar](max) NULL,
	[Step_Therapy_Placement] [varchar](max) NULL,
	[Number_of_Steps] [varchar](max) NULL,
	[Step_Therapy_Notes] [varchar](max) NULL,
	[Specific_PA_Criteria_details] [varchar](max) NULL,
	[Limitation_on_Cancer_Type] [varchar](max) NULL,
	[Limitation_on_Cancer_Type_Desc] [varchar](max) NULL,
	[Place_in_Therapy] [varchar](max) NULL,
	[Place_in_Therapy_Desc] [varchar](max) NULL,
	[Place_in_Maintenance_Therapy] [varchar](max) NULL,
	[Place_in_Maintenance_Therapy_Desc] [varchar](max) NULL,
	[Mandates_on_Concomitant_Therapies] [varchar](max) NULL,
	[Mandates_on_Concomitant_Therapies_Desc] [varchar](max) NULL,
	[Specific_Combination_Therapy] [varchar](max) NULL,
	[Prohibits_Specified_Concomitant_Therapies] [varchar](max) NULL,
	[Prohibits_Specified_Concomitant_Therapies_Desc] [varchar](max) NULL,
	[Lab_Requirements] [varchar](max) NULL,
	[Administrative_Requirements] [varchar](max) NULL,
	[Proof_of_Effectiveness_Required_Summary] [varchar](max) NULL,
	[Proof_of_Effectiveness_Req_for_Continued_Appr] [varchar](max) NULL,
	[Limitation_on_Recycling] [varchar](max) NULL,
	[Competitor_Drugs] [varchar](max) NULL,
	[J_Codes_Approved] [varchar](max) NULL,
	[ICD_Codes_Approved] [varchar](max) NULL,
	[Specialist_Appr] [varchar](max) NULL,
	[PBM_Indicated] [varchar](max) NULL,
	[PBM_Control] [varchar](max) NULL,
	[Distribution_Limitations_Enforced] [varchar](max) NULL,
	[Name_Of_Specialty_Drug_Distributer_1] [varchar](max) NULL,
	[Name_Of_Specialty_Drug_Distributer_2] [varchar](max) NULL,
	[Documentation_Source] [varchar](max) NULL,
	[Policy_Date] [varchar](max) NULL,
	[Renewal_Date] [varchar](max) NULL,
	[Note_1] [varchar](max) NULL,
	[Categorization_Detail] [varchar](max) NULL
)
GO

-----------------------------------RCC------------------------------
IF TYPE_ID(N'CF_GENENTECH_FLAT_RCC') IS NOT NULL
	DROP TYPE [dbo].[CF_GENENTECH_FLAT_RCC]
GO

CREATE TYPE [dbo].[CF_GENENTECH_FLAT_RCC] AS TABLE(
	[Mcoid] [varchar](max) NULL,
	[PayerName] [varchar](max) NULL,
	[Segment] [varchar](max) NULL,
	[Plan_Name] [varchar](max) NULL,
	[Indication] [varchar](max) NULL,
	[Drug_Name] [varchar](max) NULL,
	[On_Formulary] [varchar](max) NULL,
	[DNA_Healthplan_Management] [varchar](max) NULL,
	[Benefit_Type] [varchar](max) NULL,
	[PA_Required] [varchar](max) NULL,
	[URL_to_PA_Policy] [varchar](max) NULL,
	[URL_to_PA_Form] [varchar](max) NULL,
	[Tier_Placement] [varchar](max) NULL,
	[Number_of_Tiers] [varchar](max) NULL,
	[Compendium_Referenced] [varchar](max) NULL,
	[Evidence_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Compendium_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Compendium_Evidence_Lvl_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[PR_Journal_Articles_Req_for_Off-Label_Appr] [varchar](max) NULL,
	[Clinical_Pathway_Utilized] [varchar](max) NULL,
	[Off_Label_Use_Indications] [varchar](max) NULL,
	[Dosing_Limitation] [varchar](max) NULL,
	[Dosing_Limitation_Desc] [varchar](max) NULL,
	[Quantity_Limitation] [varchar](max) NULL,
	[Quantity_Limitation_Desc] [varchar](max) NULL,
	[Initial_Auth_Time_Length] [varchar](max) NULL,
	[Recert_Time_Length] [varchar](max) NULL,
	[Step_Therapy_Req] [varchar](max) NULL,
	[Step_Therapy_Placement] [varchar](max) NULL,
	[Number_of_Steps] [varchar](max) NULL,
	[Step_Therapy_Notes] [varchar](max) NULL,
	[Specific_PA_Criteria_details] [varchar](max) NULL,
	[Limitation_on_Cancer_Type] [varchar](max) NULL,
	[Limitation_on_Cancer_Type_Desc] [varchar](max) NULL,
	[Place_in_Therapy] [varchar](max) NULL,
	[Place_in_Therapy_Desc] [varchar](max) NULL,
	[Mandates_on_Concomitant_Therapies] [varchar](max) NULL,
	[Mandates_on_Concomitant_Therapies_Desc] [varchar](max) NULL,
	[Specific_Combination_Therapy] [varchar](max) NULL,
	[Prohibits_Specified_Concomitant_Therapies] [varchar](max) NULL,
	[Prohibits_Specified_Concomitant_Therapies_Desc] [varchar](max) NULL,
	[Lab_Requirements] [varchar](max) NULL,
	[Administrative_Requirements] [varchar](max) NULL,
	[Proof_of_Effectiveness_Required_Summary] [varchar](max) NULL,
	[Proof_of_Effectiveness_Req_for_Continued_Appr] [varchar](max) NULL,
	[Limitation_on_Recycling] [varchar](max) NULL,
	[Competitor_Drugs] [varchar](max) NULL,
	[J_Codes_Approved] [varchar](max) NULL,
	[ICD_Codes_Approved] [varchar](max) NULL,
	[Specialist_Appr] [varchar](max) NULL,
	[PBM_Indicated] [varchar](max) NULL,
	[PBM_Control] [varchar](max) NULL,
	[Distribution_Limitations_Enforced] [varchar](max) NULL,
	[Name_of_Specialty_Drug_Distributer_1] [varchar](max) NULL,
	[Name_of_Specialty_Drug_Distributer_2] [varchar](max) NULL,
	[Documentation_Source] [varchar](max) NULL,
	[Policy_Date] [varchar](max) NULL,
	[Renewal_Date] [varchar](max) NULL,
	[Note_1] [varchar](max) NULL,
	[Categorization_Detail] [varchar](max) NULL
)
GO

--------------------------------------------------------------------------------------------------------------
------------------------------------------------------2. SPs--------------------------------------------------

-----------------------------------MM------------------------------
IF EXISTS (SELECT 1 FROM sys.objects WHERE type = 'P' AND name = 'usp_CF_GENENTECH_FLAT_MM')
	DROP PROCEDURE [dbo].[usp_CF_GENENTECH_FLAT_MM]
GO

-- Data:    EXEC usp_CF_GENENTECH_FLAT_MM 2630, 'MM'
-- Data:    EXEC usp_CF_GENENTECH_FLAT_MM 2630, 'MM', 0
-- Errors:  EXEC usp_CF_GENENTECH_FLAT_MM 2630, 'MM', 1
CREATE PROCEDURE [dbo].[usp_CF_GENENTECH_FLAT_MM]
	@Userid INT,
	@IndOrDrug VARCHAR(50),
	@ReturnErrors BIT = 0
AS
BEGIN

	DECLARE @Get_CF_GENENTECH_FLAT_MM as CF_GENENTECH_FLAT_MM
		
	DECLARE @selectedFields as varchar(max)
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_GENENTECH_FLAT_MM','');
		
	INSERT INTO @Get_CF_GENENTECH_FLAT_MM
	EXEC dbo.usp_Get_Plans
		@IndicationorDrug = @IndOrDrug,
		@Userid = @Userid,
		@isPBMOnly = 0,
		@SelectFields = @selectedFields

	IF @ReturnErrors = 0
		BEGIN	
			SELECT
				[Data_Extraction_Date] = CONVERT(char(10), GetDate(),126), -- ISO8601	yyyy-mm-ddThh:mi:ss.mmm (no spaces)
				[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
				[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
				[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
				[Plan_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
				[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
				[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
				[On_Formulary] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
				[DNA_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([DNA_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
				[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
				[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Referenced] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Referenced],CHAR(13),' '),CHAR(10),' ') )),
				[Evidence_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Evidence_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Evidence_Lvl_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Evidence_Lvl_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[PR_Journal_Articles_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([PR_Journal_Articles_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Clinical_Pathway_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Clinical_Pathway_Utilized],CHAR(13),' '),CHAR(10),' ') )),
				[Off_Label_Use_Indications] = LTRIM(RTRIM(REPLACE(REPLACE([Off_Label_Use_Indications],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_PA_Criteria_details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Cancer_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Cancer_Type],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Cancer_Type_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Cancer_Type_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Therapy],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Therapy_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Therapy_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Maintenance_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Maintenance_Therapy],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Maintenance_Therapy_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Maintenance_Therapy_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Mandates_on_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
				[Mandates_on_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_Combination_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Combination_Therapy],CHAR(13),' '),CHAR(10),' ') )),
				[Prohibits_Specified_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
				[Prohibits_Specified_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Req_for_Continued_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Req_for_Continued_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Recycling] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Recycling],CHAR(13),' '),CHAR(10),' ') )),
				[Competitor_Drugs] = LTRIM(RTRIM(REPLACE(REPLACE([Competitor_Drugs],CHAR(13),' '),CHAR(10),' ') )),
				[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[Specialist_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
				[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
				[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
				[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
				[Categorization_Detail] = LTRIM(RTRIM(REPLACE(REPLACE([Categorization_Detail],CHAR(13),' '),CHAR(10),' ') ))
			FROM @Get_CF_GENENTECH_FLAT_MM
			WHERE 
				(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				AND

				(	
					( (ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) >= 2) )
					OR 
					( (ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))
				)

				AND

				(Step_Therapy_Req COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END
		ELSE
			BEGIN
				
				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'DNA_Healthplan_Management',
					InvalidValue = DNA_Healthplan_Management
				FROM @Get_CF_GENENTECH_FLAT_MM
				WHERE	(DNA_Healthplan_Management IS NULL)
						OR
						(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Placement',
					InvalidValue = Step_Therapy_Placement
				FROM @Get_CF_GENENTECH_FLAT_MM
				WHERE	(Step_Therapy_Placement IS NULL)
						OR
						((ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) < 2))
						OR 
						((ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Req',
					InvalidValue = Step_Therapy_Req
				FROM @Get_CF_GENENTECH_FLAT_MM
				WHERE	(Step_Therapy_Req IS NULL)
						OR
						(Step_Therapy_Req COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END

END

GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE type = 'P' AND name = 'usp_CF_GENENTECH_PBM_MM')
	DROP PROCEDURE [dbo].[usp_CF_GENENTECH_PBM_MM]
GO

-- Data:    EXEC usp_CF_GENENTECH_PBM_MM 2630, 'MM'
-- Data:    EXEC usp_CF_GENENTECH_PBM_MM 2630, 'MM', 0
-- Errors:  EXEC usp_CF_GENENTECH_PBM_MM 2630, 'MM', 1
CREATE PROCEDURE [dbo].[usp_CF_GENENTECH_PBM_MM]
	@Userid INT,
	@IndOrDrug VARCHAR(50),
	@ReturnErrors BIT = 0
AS
BEGIN

	DECLARE @Get_CF_GENENTECH_PBM_MM as CF_GENENTECH_PBM_MM
		
	DECLARE @selectedFields as varchar(max)
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_GENENTECH_PBM_MM','');
		
	INSERT INTO @Get_CF_GENENTECH_PBM_MM
	EXEC dbo.usp_Get_Plans
		@IndicationorDrug = @IndOrDrug,
		@Userid = @Userid,
		@isPBMOnly = 1,
		@SelectFields = @selectedFields

	IF @ReturnErrors = 0
		BEGIN		
			SELECT
				[Data_Extraction_Date] = CONVERT(char(10), GetDate(),126), -- ISO8601	yyyy-mm-ddThh:mi:ss.mmm (no spaces)
				[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
				[Segment] = dbo.udf_CF_GetSegmentFromPbmName(LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') ))),
				[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
				[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
				[On_Formulary] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
				[DNA_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([DNA_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
				[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
				[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Referenced] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Referenced],CHAR(13),' '),CHAR(10),' ') )),
				[Evidence_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Evidence_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Evidence_Lvl_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Evidence_Lvl_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[PR_Journal_Articles_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([PR_Journal_Articles_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Clinical_Pathway_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Clinical_Pathway_Utilized],CHAR(13),' '),CHAR(10),' ') )),
				[Off_Label_Use_Indications] = LTRIM(RTRIM(REPLACE(REPLACE([Off_Label_Use_Indications],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_PA_Criteria_details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Cancer_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Cancer_Type],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Cancer_Type_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Cancer_Type_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Therapy],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Therapy_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Therapy_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Maintenance_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Maintenance_Therapy],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Maintenance_Therapy_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Maintenance_Therapy_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Mandates_on_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
				[Mandates_on_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_Combination_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Combination_Therapy],CHAR(13),' '),CHAR(10),' ') )),
				[Prohibits_Specified_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
				[Prohibits_Specified_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Req_for_Continued_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Req_for_Continued_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Recycling] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Recycling],CHAR(13),' '),CHAR(10),' ') )),
				[Competitor_Drugs] = LTRIM(RTRIM(REPLACE(REPLACE([Competitor_Drugs],CHAR(13),' '),CHAR(10),' ') )),
				[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[Specialist_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
				[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
				[Name_Of_Specialty_Drug_Distributer_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
				[Name_Of_Specialty_Drug_Distributer_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
				[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
				[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
				[Categorization_Detail] = LTRIM(RTRIM(REPLACE(REPLACE([Categorization_Detail],CHAR(13),' '),CHAR(10),' ') ))
			FROM @Get_CF_GENENTECH_PBM_MM
			WHERE 
				(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				AND

				(	
					( (ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) >= 2) )
					OR 
					( (ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))
				)

				AND

				(Step_Therapy_Req COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END
		ELSE
			BEGIN
				
				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'DNA_Healthplan_Management',
					InvalidValue = DNA_Healthplan_Management
				FROM @Get_CF_GENENTECH_PBM_MM
				WHERE	(DNA_Healthplan_Management IS NULL)
						OR
						(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Placement',
					InvalidValue = Step_Therapy_Placement
				FROM @Get_CF_GENENTECH_PBM_MM
				WHERE	(Step_Therapy_Placement IS NULL)
						OR
						((ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) < 2))
						OR 
						((ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Req',
					InvalidValue = Step_Therapy_Req
				FROM @Get_CF_GENENTECH_PBM_MM
				WHERE	(Step_Therapy_Req IS NULL)
						OR
						(Step_Therapy_Req COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

				
			END

END

GO

-----------------------------------RCC------------------------------
IF EXISTS (SELECT 1 FROM sys.objects WHERE type = 'P' AND name = 'usp_CF_GENENTECH_FLAT_RCC')
	DROP PROCEDURE [dbo].[usp_CF_GENENTECH_FLAT_RCC]
GO

-- Data:    EXEC usp_CF_GENENTECH_FLAT_RCC 2630, 'RCC'
-- Data:    EXEC usp_CF_GENENTECH_FLAT_RCC 2630, 'RCC', 0
-- Errors:  EXEC usp_CF_GENENTECH_FLAT_RCC 2630, 'RCC', 1
CREATE PROCEDURE [dbo].[usp_CF_GENENTECH_FLAT_RCC]
	@Userid INT,
	@IndOrDrug VARCHAR(50),
	@ReturnErrors BIT = 0
AS
BEGIN

	DECLARE @Get_CF_GENENTECH_FLAT_RCC as CF_GENENTECH_FLAT_RCC
		
	DECLARE @selectedFields as varchar(max)
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_GENENTECH_FLAT_RCC','');
		
	INSERT INTO @Get_CF_GENENTECH_FLAT_RCC
	EXEC dbo.usp_Get_Plans
		@IndicationorDrug = @IndOrDrug,
		@Userid = @Userid,
		@isPBMOnly = 0,
		@SelectFields = @selectedFields

	IF @ReturnErrors = 0
		BEGIN	
			SELECT
				[Data_Extraction_Date] = CONVERT(char(10), GetDate(),126), -- ISO8601	yyyy-mm-ddThh:mi:ss.mmm (no spaces)
				[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
				[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
				[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
				[Plan_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
				[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
				[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
				[On_Formulary] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
				[DNA_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([DNA_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
				[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
				[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
				[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
				[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Referenced] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Referenced],CHAR(13),' '),CHAR(10),' ') )),
				[Evidence_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Evidence_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Compendium_Evidence_Lvl_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Compendium_Evidence_Lvl_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[PR_Journal_Articles_Req_for_Off_Label_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([PR_Journal_Articles_Req_for_Off-Label_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Clinical_Pathway_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Clinical_Pathway_Utilized],CHAR(13),' '),CHAR(10),' ') )),
				[Off_Label_Use_Indications] = LTRIM(RTRIM(REPLACE(REPLACE([Off_Label_Use_Indications],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
				[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
				[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
				[Step_Therapy_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_PA_Criteria_details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Cancer_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Cancer_Type],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Cancer_Type_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Cancer_Type_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Therapy],CHAR(13),' '),CHAR(10),' ') )),
				[Place_in_Therapy_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Place_in_Therapy_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Mandates_on_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
				[Mandates_on_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Specific_Combination_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Combination_Therapy],CHAR(13),' '),CHAR(10),' ') )),
				[Prohibits_Specified_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
				[Prohibits_Specified_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
				[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
				[Proof_of_Effectiveness_Req_for_Continued_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Req_for_Continued_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[Limitation_on_Recycling] = LTRIM(RTRIM(REPLACE(REPLACE([Limitation_on_Recycling],CHAR(13),' '),CHAR(10),' ') )),
				[Competitor_Drugs] = LTRIM(RTRIM(REPLACE(REPLACE([Competitor_Drugs],CHAR(13),' '),CHAR(10),' ') )),
				[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
				[Specialist_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Appr],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
				[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
				[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
				[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
				[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
				[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
				[Categorization_Detail] = LTRIM(RTRIM(REPLACE(REPLACE([Categorization_Detail],CHAR(13),' '),CHAR(10),' ') ))
			FROM @Get_CF_GENENTECH_FLAT_RCC
			WHERE 
				(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				AND

				(	
					( (ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) >= 2) )
					OR 
					( (ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))
				)

				AND

				(Step_Therapy_Req COLLATE Latin1_General_CS_AS IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END
		ELSE
			BEGIN
				
				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'DNA_Healthplan_Management',
					InvalidValue = DNA_Healthplan_Management
				FROM @Get_CF_GENENTECH_FLAT_RCC
				WHERE	(DNA_Healthplan_Management IS NULL)
						OR
						(DNA_Healthplan_Management COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 1))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Placement',
					InvalidValue = Step_Therapy_Placement
				FROM @Get_CF_GENENTECH_FLAT_RCC
				WHERE	(Step_Therapy_Placement IS NULL)
						OR
						((ISNUMERIC(Step_Therapy_Placement) = 1) AND (CAST(Step_Therapy_Placement AS INT) < 2))
						OR 
						((ISNUMERIC(Step_Therapy_Placement) <> 1) AND (Step_Therapy_Placement COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 2)))

				UNION ALL

				SELECT
					Mcoid,
					Plan_Name,
					Drug_Name,
					Indication,
					InvalidColumnName = 'Step_Therapy_Req',
					InvalidValue = Step_Therapy_Req
				FROM @Get_CF_GENENTECH_FLAT_RCC
				WHERE	(Step_Therapy_Req IS NULL)
						OR
						(Step_Therapy_Req COLLATE Latin1_General_CS_AS NOT IN (SELECT FieldValue FROM CF_ApprovedFieldValues WHERE FieldNameID = 3))

			END

END

GO

-----------------------------------Custom File------------------------------
IF EXISTS (SELECT 1 FROM sys.objects WHERE type = 'P' AND name = 'usp_CF_CustomFile')
	DROP PROCEDURE [dbo].[usp_CF_CustomFile]
GO


/*

**** Merck **** deactivated

**** Janssen **** deactivated

**** Sandoz **** 
  -- 'Sandoz Custom File' : EXEC usp_CF_CustomFile @ClientID = 67, @CategoryID = 8, @IndicationArray = 'NEU', @DrugArray = '', @ScheduleTypeID = 2
                            EXEC usp_CF_CustomFile @ClientID = 67, @CategoryID = 8, @IndicationArray = 'GHD', @DrugArray = '', @ScheduleTypeID = 2
                            EXEC usp_CF_CustomFile @ClientID = 67, @CategoryID = 8, @IndicationArray = 'CLL', @DrugArray = '', @ScheduleTypeID = 2 --CLL and NHL
							-- (we couldn't put 'CLL,NHL' to act like "join indications into file" because all the other steps act as "separate indications into different files")
                            EXEC usp_CF_CustomFile @ClientID = 67, @CategoryID = 8, @IndicationArray = 'RA', @DrugArray = '', @ScheduleTypeID = 2

**** Regeneron - Sanofi ****
  -- 'Regeneron-Sanofi all indications monthly files' : EXEC usp_CF_CustomFile @ClientID = 58, @CategoryID = 9, @IndicationArray = 'RA', @DrugArray = '', @ScheduleTypeID = 2

  -- 'Payer Sciences - HCHL File' : EXEC usp_CF_CustomFile @ClientID = 58, @CategoryID = 10, @IndicationArray = 'HCHL', @DrugArray = '', @ScheduleTypeID = 2
  -- 'McCann -  HCHL State File' : EXEC usp_CF_CustomFile @ClientID = 58, @CategoryID = 11, @IndicationArray = 'HCHL', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Protean - HCHL State File' : EXEC usp_CF_CustomFile @ClientID = 58, @CategoryID = 12, @IndicationArray = 'HCHL', @DrugArray = '', @ScheduleTypeID = 2

**** Heron **** 
  -- 'Heron - One File / Multiple Tabs' : EXEC usp_CF_CustomFile @ClientID = 63, @CategoryID = 13, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

**** Regeneron ****
  -- 'Beghou - Multiple files' : EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 14, @IndicationArray = 'AMD', @DrugArray = '', @ScheduleTypeID = 2
                                 EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 14, @IndicationArray = 'DME', @DrugArray = '', @ScheduleTypeID = 2
								 EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 14, @IndicationArray = 'DR',  @DrugArray = '', @ScheduleTypeID = 2
								 EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 14, @IndicationArray = 'RVO', @DrugArray = '', @ScheduleTypeID = 2
  
  -- 'Regeneron - AMD State File'        : EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 24, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Regeneron - DME State File'        : EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 25, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Regeneron - DR State File'         : EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 26, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Regeneron - RVO State File'        : EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 27, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

**** Novartis ****
  -- 'Weekly Files for Novartis Entresto'         : EXEC usp_CF_CustomFile @ClientID = 15, @CategoryID = 15, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 1
  -- 'Monthly Files for Novartis Access Analysis' : EXEC usp_CF_CustomFile @ClientID = 15, @CategoryID = 19, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Monthly File for Novartis RCC'              : EXEC usp_CF_CustomFile @ClientID = 15, @CategoryID = 22, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Weekly Files for Novartis Tasigna'          : EXEC usp_CF_CustomFile @ClientID = 15, @CategoryID = 33, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 1
  -- 'Weekly Files for Novartis Promacta'         : EXEC usp_CF_CustomFile @ClientID = 15, @CategoryID = 34, @IndicationArray = 'ITP', @DrugArray = '', @ScheduleTypeID = 1
                                                    EXEC usp_CF_CustomFile @ClientID = 15, @CategoryID = 34, @IndicationArray = 'SAA', @DrugArray = '', @ScheduleTypeID = 1

**** BMS ****
  -- 'Monthly Files for BMS' : EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'HL', @DrugArray = '', @ScheduleTypeID = 2
                               EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'MEL', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'MM', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'NSCLC', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'RA', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'RCC', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'HN', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'URC', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'CRC', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'HCC', @DrugArray = '', @ScheduleTypeID = 2
				   
**** Genentech ****
  -- 'Monthly Files for Genentech'
            EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'Asthma', @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'CF',     @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'CD',     @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'UC',     @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'IPF',    @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'AMD',    @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'MS',     @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'BCC',    @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'RA',     @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'Hemo',   @DrugArray = '', @ScheduleTypeID = 2
			
			-- EXCEPTION REPORT = Empty Indication
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 35, @IndicationArray = '',   @DrugArray = '', @ScheduleTypeID = 2

	-- 'Genentech Specialty Pharmacy'
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 20, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

	-- 'Monthly Genetech Ocular File'
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 23, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

	-- 'Geo Alignment'
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 31, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2


**** Ipsen ****
  -- 'Monthly Files for Ipsen Dysport'
            EXEC usp_CF_CustomFile @ClientID = 51, @CategoryID = 21, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

**** Lilly ****
  -- 'Lilly - BC FLAT'
            EXEC usp_CF_CustomFile @ClientID = 83, @CategoryID = 28, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

  -- 'Lilly - BC PBM'
            EXEC usp_CF_CustomFile @ClientID = 83, @CategoryID = 29, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

  -- 'Lilly - BC STATE'
            EXEC usp_CF_CustomFile @ClientID = 83, @CategoryID = 30, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

*/
 

CREATE PROCEDURE [dbo].[usp_CF_CustomFile]  
	-- Add the parameters for the stored procedure here
	@ClientID INT,
	@CategoryID INT,
	@IndicationArray VARCHAR(255),	
	@DrugArray VARCHAR(255),
	@ScheduleTypeID INT
AS
BEGIN

DECLARE @ClientAdminUserID INT ;
DECLARE @PACKING_SLIP TABLE ([TABNAME] VARCHAR(MAX))


	--SELECT @ScheduleTypeID = ISNULL(p.ScheduleTypeID,0) 
	--FROM Process p 
	--WHERE p.ProcessID = @ProcessID

	IF @ClientID = 32 ---Merck
	BEGIN		
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;

		--- Check the Category
		IF @CategoryID = 1 -- MET
		BEGIN
			
			---DECLARE @PACKING_SLIP TABLE ([TABNAME] VARCHAR(MAX))

			IF @ScheduleTypeID = 1 -- Run Normally, like the Weekly Process
				BEGIN 
				
					INSERT INTO @PACKING_SLIP VALUES ('Master-New')
					INSERT INTO @PACKING_SLIP VALUES ('Master')
					INSERT INTO @PACKING_SLIP VALUES ('AM-PR Overview')
					INSERT INTO @PACKING_SLIP VALUES ('Master-State')
					INSERT INTO @PACKING_SLIP VALUES ('Data Dictionary')
					INSERT INTO @PACKING_SLIP VALUES ('AM-PR Data Dictionary')
			
					-- Table 1
					SELECT * FROM @PACKING_SLIP;

					-- Table 2 ( Master New, Master, AM-PR Overview)
					EXEC usp_CF_Master_New 
							@ClientAdminUserID = @ClientAdminUserID, 
							@IndicationorDrug = @IndicationArray, 
							@DrugArray = @DrugArray;
					-- Table 3
					EXEC dbo.usp_CF_Master_State 
							@ClientAdminUserID = @ClientAdminUserID,  
							@IndicationAbbrv = @IndicationArray,  
							@DrugNameArray = @DrugArray 
					-- Table 4
					EXEC dbo.usp_Get_DataDictionary 
							@UserID = @ClientAdminUserID,  
							@Abbreviation = @IndicationArray
					-- Table 5
					EXEC  dbo.usp_CF_AMPR_DataDictionary
				END 
			ELSE
				BEGIN 
						--Monthly Process
						DECLARE @SINGLE_INDICATION VARCHAR(255)

						-- Construct Packing Slip
						DECLARE PACKINGSLIP_CURSOR CURSOR
						LOCAL STATIC READ_ONLY FORWARD_ONLY
						FOR 
							SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs

							OPEN PACKINGSLIP_CURSOR
							FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
							WHILE @@FETCH_STATUS = 0
							BEGIN 
									INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Master')
									INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Data')
									INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' PBM Data')
									INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Data Dictionary')

								FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
							END 
						CLOSE PACKINGSLIP_CURSOR
						DEALLOCATE PACKINGSLIP_CURSOR

						INSERT INTO @PACKING_SLIP VALUES ( 'Definition')

				-------------------------------------------------------------------------------------------------------

				-- Table 1
				SELECT * FROM @PACKING_SLIP;

				-- Process each indication
				DECLARE INDICATION_CURSOR CURSOR
				LOCAL STATIC READ_ONLY FORWARD_ONLY
				FOR 
					SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs

					OPEN INDICATION_CURSOR
					FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
					WHILE @@FETCH_STATUS = 0
					BEGIN 
							--PRINT '-----------------------------------------------------------------------------------'
							--PRINT ' >>>>>>>>>>>>>>>>>>>>>> ' + @SINGLE_INDICATION
							--PRINT '-----------------------------------------------------------------------------------'

							-- Drug Array to use for each client intication
							DECLARE @ClientDrugs VARCHAR(8000)
							SELECT @ClientDrugs = COALESCE(@ClientDrugs + ', ', '') + RTRIM(LTRIM(vccdi.Drug)) FROM dbo.vw_CF_ClientDrugIndication vccdi
							WHERE ClientID = @ClientID
							AND vccdi.IndicationAbbrv = @SINGLE_INDICATION

							-- [Indication Name] + Master Data, and Master Raw Data
							EXEC usp_CF_Master_Only
									@ClientAdminUserID = @ClientAdminUserID, 
									@IndicationorDrug = @SINGLE_INDICATION, 
									@DrugArray = @ClientDrugs;

							-- PBM Data
							EXEC usp_Get_Plans 
									@Userid = @ClientAdminUserID, 
									@IndicationorDrug = @SINGLE_INDICATION, 
									@isPBMOnly = 1;

							-- Data Dictionary
							EXEC dbo.usp_Get_DataDictionary 
									@UserID = @ClientAdminUserID,  
									@Abbreviation = @SINGLE_INDICATION

						FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
					END 
				CLOSE INDICATION_CURSOR
				DEALLOCATE INDICATION_CURSOR


				-- Only Need One Definition at the end
				EXEC dbo.usp_Get_DataDefinition 
						@UserID = @ClientAdminUserID

	  
			END 
				
		END


		--Next Category
		IF @CategoryID = 2 -- MET - Pivots
		BEGIN
			
			IF @ScheduleTypeID = 2 -- Relevant to the Monthly Process
				BEGIN 
				
						-- Construct Packing Slip
						DECLARE PACKINGSLIP_CURSOR CURSOR
						LOCAL STATIC READ_ONLY FORWARD_ONLY
						FOR 
							SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs

							OPEN PACKINGSLIP_CURSOR
							FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
							WHILE @@FETCH_STATUS = 0
							BEGIN 
									INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Master')
								---	INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Pivots')
 
								FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
							END 
						CLOSE PACKINGSLIP_CURSOR
						DEALLOCATE PACKINGSLIP_CURSOR

					-- Table 1
					SELECT * FROM @PACKING_SLIP;

					--PRINT 'Client Admin User ID = ' + CONVERT(VARCHAR, @ClientAdminUserID )
					--PRINT 'Indication = ' + CONVERT(VARCHAR, @IndicationArray )
					--PRINT 'Drug Array = ' + CONVERT(VARCHAR, @DrugArray )

					-- Table 2 ( Master )


							-- Process each indication
				DECLARE INDICATION_CURSOR CURSOR
				LOCAL STATIC READ_ONLY FORWARD_ONLY
				FOR 
					SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs

					OPEN INDICATION_CURSOR
					FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
					WHILE @@FETCH_STATUS = 0
					BEGIN 
							PRINT '-----------------------------------------------------------------------------------'
							PRINT ' >>>>>>>>>>>>>>>>>>>>>> ' + @SINGLE_INDICATION
							PRINT '-----------------------------------------------------------------------------------'

							-- Drug Array to use for each client intication
							--DECLARE @ClientDrugs2 VARCHAR(8000)
							--SELECT @ClientDrugs2 = COALESCE(@ClientDrugs2 + ', ', '') + RTRIM(LTRIM(vccdi.Drug)) FROM dbo.vw_CF_ClientDrugIndication vccdi
							--WHERE ClientID = @ClientID
							--AND vccdi.IndicationAbbrv = @SINGLE_INDICATION

								--PRINT 'Client Admin User ID = ' + CONVERT(VARCHAR, @ClientAdminUserID )
								--PRINT 'Indication = ' + CONVERT(VARCHAR, @IndicationArray )
								--PRINT 'Drug Array = ' + CONVERT(VARCHAR, @DrugArray )

							EXEC usp_CF_Master_Single
								@ClientAdminUserID = @ClientAdminUserID, 
								@IndicationorDrug = @SINGLE_INDICATION, 
								@DrugArray =@DrugArray;


							--Empty RS
							--SELECT '' AS PivotTable

						FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
					END 
				CLOSE INDICATION_CURSOR
				DEALLOCATE INDICATION_CURSOR


					
					/*
					
					Client Admin User ID = 2647
					Indication = MEL, NSCLC
					Drug Array = Keytruda,Opdivo
					
					EXEC usp_CF_Master_Single
							@ClientAdminUserID = 2647, 
							@IndicationorDrug = 'MEL', 
							@DrugArray ='Keytruda, Opdivo';

 
					*/
					
				 
				END 


		END 

		----End Category

	END
	ELSE IF  @ClientID = 14 ---Janssen
	   BEGIN		
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;

		--- Check the Category
		IF @CategoryID  =3 -- state
		BEGIN
			
	

							-- PBM Data
							EXEC usp_CF_JANSSEN_Get_Plans 
								   @Userid =@ClientAdminUserID,
								   @IndicationorDrug=@IndicationArray,
								   @CategoryID=@CategoryID
							
							
								-- Only Need One Definition at the end
							EXEC dbo.usp_Get_DataDefinition 
									@UserID = @ClientAdminUserID
						
						
							-- Data Dictionary
							EXEC dbo.usp_Get_DataDictionary 
									@UserID = @ClientAdminUserID,  
									@Abbreviation = @IndicationArray

						
				
		END
		IF @CategoryID = 4 -- CBSA
		BEGIN
			
				
							EXEC usp_CF_JANSSEN_Get_Plans 
								   @Userid =@ClientAdminUserID,
								   @IndicationorDrug=@IndicationArray,
								   @CategoryID=@CategoryID
							
									-- Only Need One Definition at the end
							EXEC dbo.usp_Get_DataDefinition 
								@UserID = @ClientAdminUserID
						
							-- Data Dictionary
							EXEC dbo.usp_Get_DataDictionary 
									@UserID = @ClientAdminUserID,  
									@Abbreviation = @IndicationArray

				
		END

		IF @CategoryID = 5 --Sales Division / Zip 
		BEGIN
			
			EXEC usp_CF_JANSSEN_SALES_DIVISON_ZIP
		END

		IF @CategoryID = 6 --National 
		BEGIN

			-- 4 Indications { CD, PSO, RA, UC }
			DECLARE @IndicationNational VARCHAR(25) = 'CD, PSO, RA, UC';

			-- Load Packing Slip
			DECLARE PACKINGSLIP_CURSOR CURSOR
			LOCAL STATIC READ_ONLY FORWARD_ONLY
			FOR 
				SELECT ITEM FROM dbo.fnSplit(@IndicationNational,',') fs

				OPEN PACKINGSLIP_CURSOR
				FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
				WHILE @@FETCH_STATUS = 0
				BEGIN 
						INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
 
					FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
				END 
			CLOSE PACKINGSLIP_CURSOR
			DEALLOCATE PACKINGSLIP_CURSOR

			SELECT * FROM @PACKING_SLIP;

			--Loop Through each indication
			DECLARE INDICATION_CURSOR CURSOR
			LOCAL STATIC READ_ONLY FORWARD_ONLY
			FOR 
				SELECT ITEM FROM dbo.fnSplit(@IndicationNational,',') fs

				OPEN INDICATION_CURSOR
				FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
				WHILE @@FETCH_STATUS = 0
				BEGIN 
						INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
						EXEC usp_CF_JANSSEN_NATIONAL_PBM @SINGLE_INDICATION

					FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
				END 
			CLOSE INDICATION_CURSOR
			DEALLOCATE INDICATION_CURSOR

		END



		IF @CategoryID = 7 -- VECTOR
		BEGIN
		
		       INSERT INTO @PACKING_SLIP VALUES ('Vector')
			
				SELECT * FROM @PACKING_SLIP;
		         EXEC usp_CF_JANSSEN_VECTOR_Get_Master
		             @Userid =@ClientAdminUserID,
					@IndicationArray=@IndicationArray
		
		END

		
    END
	 ELSE IF  @ClientID = 67 ---Sandoz
    BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
    
		----------------------------- Cursor for indications, reused in several loops
		DECLARE @Indications VARCHAR(25)
		
		-- CLL step contains 2 indications: CLL and NHL
		-- (we couldn't put 'CLL,NHL' to act like "join indications into file" because all the other steps act as "separate indications into different files")
		IF @IndicationArray = 'CLL'
			BEGIN
				SET @IndicationArray = 'CLL,NHL'
			END

		SET @Indications = @IndicationArray
		
		DECLARE curIndications CURSOR SCROLL
		LOCAL STATIC READ_ONLY
		FOR SELECT ITEM FROM dbo.fnSplit(@Indications,',') fs
			
		----------------------------- Packing Slip
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Recent Changes')
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' PBM')
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' by State')
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
			
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + '_DD')
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications
		
		INSERT INTO @PACKING_SLIP VALUES ('Definition')
		SELECT * FROM @PACKING_SLIP;
		

		------------------------------- Recent Changes, Flat, PBM, State
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN
				
				IF @SINGLE_INDICATION = 'NEU'
					BEGIN
						SELECT 'NEU Recent Changes' AS 'Recent Changes'
						EXEC usp_CF_SANDOZ_FLAT_NEU 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NEU'
						EXEC usp_CF_SANDOZ_PBM_NEU 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NEU'
						EXEC usp_CF_SANDOZ_STATE_NEU 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NEU'
					END
				ELSE IF @SINGLE_INDICATION = 'GHD'
					BEGIN
						SELECT 'GHD Recent Changes' AS 'Recent Changes'
						EXEC usp_CF_SANDOZ_FLAT_GHD 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'GHD'
						EXEC usp_CF_SANDOZ_PBM_GHD 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'GHD'
						EXEC usp_CF_SANDOZ_STATE_GHD 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'GHD'
					END
				ELSE IF @SINGLE_INDICATION = 'CLL'
					BEGIN
						SELECT 'CLL Recent Changes' AS 'Recent Changes'
						EXEC usp_CF_SANDOZ_FLAT_CLL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'CLL'
						EXEC usp_CF_SANDOZ_PBM_CLL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'CLL'
						EXEC usp_CF_SANDOZ_STATE_CLL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'CLL'
					END
				ELSE IF @SINGLE_INDICATION = 'NHL'
					BEGIN
						SELECT 'NHL Recent Changes' AS 'Recent Changes'
						EXEC usp_CF_SANDOZ_FLAT_NHL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NHL'
						EXEC usp_CF_SANDOZ_PBM_NHL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NHL'
						EXEC usp_CF_SANDOZ_STATE_NHL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NHL'
					END
				ELSE IF @SINGLE_INDICATION = 'RA'
					BEGIN
						SELECT 'RA Recent Changes' AS 'Recent Changes'
						EXEC usp_CF_SANDOZ_FLAT_RA 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'RA'
						EXEC usp_CF_SANDOZ_PBM_RA 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'RA'
						EXEC usp_CF_SANDOZ_STATE_RA 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'RA'
					END


				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications

		------------------------------- [INDICATION]_DD
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN

				EXEC dbo.usp_Get_DataDictionary 
					@UserID = @ClientAdminUserID,  
					@Abbreviation = @SINGLE_INDICATION

				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications

		------------------------------- Definition
		EXEC dbo.usp_Get_DataDefinition 
			@UserID = @ClientAdminUserID

    END





	ELSE IF  @ClientID = 58 ---Regeneron - Sanofi
    BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
    
		-- Regeneron-Sanofi all indications monthly files
		IF @CategoryID = 9 
			BEGIN
				----------------------------- Packing Slip
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray )
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' PBM Data')
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' Data Dictionary')
				INSERT INTO @PACKING_SLIP VALUES ( 'Definition')
				SELECT * FROM @PACKING_SLIP;
				
				----------------------------- [INDICATION]
				/*IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_FLAT_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END

				ELSE*/ IF @IndicationArray = 'RA'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_FLAT_RA
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				
				----------------------------- [INDICATION] PBM Data
				/*IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_PBM_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				ELSE*/ IF @IndicationArray = 'RA'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_PBM_RA
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				
				----------------------------- [INDICATION] Data Dictionary
				EXEC dbo.usp_Get_DataDictionary 
					@UserID = @ClientAdminUserID,  
					@Abbreviation = @IndicationArray
				
				----------------------------- Definition
				EXEC dbo.usp_Get_DataDefinition 
					@UserID = @ClientAdminUserID

			END
			
			
		-- Regeneron-PayerSciences
		IF @CategoryID = 10
			BEGIN
				----------------------------- Packing Slip
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray )
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' PBM Data')
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' Data Dictionary')
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' Health Plan Management')
				INSERT INTO @PACKING_SLIP VALUES ( 'Definition')
				SELECT * FROM @PACKING_SLIP;
				
				----------------------------- [INDICATION]
				IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_PAYER_SCIENCES_FLAT_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						SELECT 'This indication does not have a defined custom file' AS 'Error Message'
					END
				
				----------------------------- [INDICATION] PBM Data
				IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_PAYER_SCIENCES_PBM_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						SELECT 'This indication does not have a defined custom file' AS 'Error Message'
					END
				
				----------------------------- [INDICATION] Data Dictionary
				EXEC dbo.usp_Get_DataDictionary 
					@UserID = @ClientAdminUserID,  
					@Abbreviation = @IndicationArray
					
					
				----------------------------- [INDICATION] Health Plan Management
				SELECT 'Health Plan Management' AS 'Health Plan Management'
				
				----------------------------- Definition
				EXEC dbo.usp_Get_DataDefinition 
					@UserID = @ClientAdminUserID

			END
			
			
		-- Regeneron-McCann
		IF @CategoryID = 11
			BEGIN
				----------------------------- Packing Slip
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray ) -- It's 'HCHL by State' but users want 'HCHL'
				INSERT INTO @PACKING_SLIP VALUES ( 'Definition')
				SELECT * FROM @PACKING_SLIP;
				
				----------------------------- [INDICATION]  by State
				IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_MCCANN_STATE_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						SELECT 'This indication does not have a defined custom file' AS 'Error Message'
					END
				
				----------------------------- Definition
				EXEC dbo.usp_Get_DataDefinition 
					@UserID = @ClientAdminUserID

			END		
			
		-- Regeneron-Protean
		IF @CategoryID = 12
			BEGIN
				----------------------------- Packing Slip
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' by State')
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' Data Dictionary')
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' Health Plan Management')
				INSERT INTO @PACKING_SLIP VALUES ( 'Definition')
				SELECT * FROM @PACKING_SLIP;
				
				----------------------------- [INDICATION]  by State
				IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_PROTEAN_STATE_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						SELECT 'This indication does not have a defined custom file' AS 'Error Message'
					END
				
				----------------------------- [INDICATION] Data Dictionary
				EXEC dbo.usp_Get_DataDictionary 
					@UserID = @ClientAdminUserID,  
					@Abbreviation = @IndicationArray
					
					
				----------------------------- [INDICATION] Health Plan Management
				SELECT 'Health Plan Management' AS 'Health Plan Management'
				
				----------------------------- Definition
				EXEC dbo.usp_Get_DataDefinition 
					@UserID = @ClientAdminUserID

			END		
    END
    
    
    ELSE IF  @ClientID = 63 -- Heron
    BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
    
		----------------------------- Cursor for indications, reused in several loops
		--DECLARE @Indications VARCHAR(25) = 'CINV';
		SET @Indications = 'CINV'; -- Already declared in Sandoz code
		
		DECLARE curIndications CURSOR SCROLL
		LOCAL STATIC READ_ONLY
		FOR SELECT ITEM FROM dbo.fnSplit(@Indications,',') fs
			
		----------------------------- Packing Slip
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				-- Give 'Recent Changes' after source sheet - so no need for PT refresh in VBA
				-- VBA will reorder sheets in proper way
				-- For Heron only, not needed for Sandoz because Sandoz has no ValueFields(Sums)
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' by State' )
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Recent Changes by State')
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Recent Changes')
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
			
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + '_DD')
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications
		
		INSERT INTO @PACKING_SLIP VALUES ('Definition')
		SELECT * FROM @PACKING_SLIP;
		
		
		
		----------------------------- CINV
		EXEC usp_CF_HERON_STATE_CINV
			@Userid = @ClientAdminUserID,
			@SINGLE_INDICATION = 'CINV'
		SELECT 'CINV Recent Changes by State' AS 'Recent Changes by State'
		EXEC usp_CF_HERON_FLAT_CINV
			@Userid = @ClientAdminUserID,
			@SINGLE_INDICATION = 'CINV'
		SELECT 'CINV Recent Changes' AS 'Recent Changes'
		----------------------------- [INDICATION]_DD
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				EXEC dbo.usp_Get_DataDictionary 
					@UserID = @ClientAdminUserID,  
					@Abbreviation = @SINGLE_INDICATION
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications
		DEALLOCATE curIndications
		
		----------------------------- Definition
		EXEC dbo.usp_Get_DataDefinition 
			@UserID = @ClientAdminUserID
    END
    
    
    
    
    ELSE IF  @ClientID = 41 -- Regeneron
    BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
    
		IF @CategoryID = 14 -- (Beghou)
		BEGIN

			IF @IndicationArray = 'AMD'
				BEGIN
					INSERT INTO @PACKING_SLIP VALUES ('AMD')
					INSERT INTO @PACKING_SLIP VALUES ('Data Dictionary')
					INSERT INTO @PACKING_SLIP VALUES ('Definition')
					SELECT * FROM @PACKING_SLIP;

					EXEC usp_CF_BEGHOU_CBSA_AMD
						@Userid = @ClientAdminUserID,
						@IndOrDrug = 'AMD'

					EXEC dbo.usp_Get_DataDictionary 
						@UserID = @ClientAdminUserID,  
						@Abbreviation = 'AMD'

					EXEC dbo.usp_Get_DataDefinition 
						@UserID = @ClientAdminUserID
				END
			ELSE IF @IndicationArray = 'DME'
				BEGIN
					INSERT INTO @PACKING_SLIP VALUES ('DME')
					SELECT * FROM @PACKING_SLIP;

					EXEC usp_CF_BEGHOU_CBSA_DME
						@Userid = @ClientAdminUserID,
						@IndOrDrug = 'DME'
				END
			ELSE IF @IndicationArray = 'DR'
				BEGIN
					INSERT INTO @PACKING_SLIP VALUES ('DR')
					SELECT * FROM @PACKING_SLIP;

					EXEC usp_CF_BEGHOU_CBSA_DR
						@Userid = @ClientAdminUserID,
						@IndOrDrug = 'DR'
				END
			ELSE IF @IndicationArray = 'RVO'
				BEGIN
					INSERT INTO @PACKING_SLIP VALUES ('RVO')
					SELECT * FROM @PACKING_SLIP;

					EXEC usp_CF_BEGHOU_CBSA_RVO
						@Userid = @ClientAdminUserID,
						@IndOrDrug = 'RVO'
				END
		END

		
		IF @CategoryID = 24 -- 'Regeneron - AMD State File'
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('AMD')
			INSERT INTO @PACKING_SLIP VALUES ('Definition')
			SELECT * FROM @PACKING_SLIP;

			EXEC usp_CF_REGENERON_STATE_AMD
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'AMD'

			EXEC dbo.usp_Get_DataDefinition 
				@UserID = @ClientAdminUserID
		END

		IF @CategoryID = 25 -- 'Regeneron - DME State File'
		BEGIN			   
			INSERT INTO @PACKING_SLIP VALUES ('DME')
			INSERT INTO @PACKING_SLIP VALUES ('Definition')
			SELECT * FROM @PACKING_SLIP;

		    EXEC usp_CF_REGENERON_STATE_DME
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'DME'

			EXEC dbo.usp_Get_DataDefinition 
				@UserID = @ClientAdminUserID
		END				   

		IF @CategoryID = 26 -- 'Regeneron - DR State File'
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('DR')
			INSERT INTO @PACKING_SLIP VALUES ('Definition')
			SELECT * FROM @PACKING_SLIP;

			EXEC usp_CF_REGENERON_STATE_DR
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'DR'

			EXEC dbo.usp_Get_DataDefinition 
				@UserID = @ClientAdminUserID
		END

		IF @CategoryID = 27 -- 'Regeneron - RVO State File'
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('RVO')
			INSERT INTO @PACKING_SLIP VALUES ('Definition')
			SELECT * FROM @PACKING_SLIP;

			EXEC usp_CF_REGENERON_STATE_RVO
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'RVO'

			EXEC dbo.usp_Get_DataDefinition 
				@UserID = @ClientAdminUserID
		END

	END






	ELSE IF  @ClientID = 15 -- Novartis (Entresto and Jadenu ( 2/6/17 ) ) ; Novartis Access Analysis 2017-06-19
	BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
		
		-- CHF+Entresto
		IF @CategoryID = 15 
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('Changes')
			INSERT INTO @PACKING_SLIP VALUES ('State view')
			INSERT INTO @PACKING_SLIP VALUES ('States')
			INSERT INTO @PACKING_SLIP VALUES ('CHF State Flat File – Full')
			--INSERT INTO @PACKING_SLIP VALUES ('CHF PBM File') -- (to be removed)
			SELECT * FROM @PACKING_SLIP;
		
			----------------------------- Changes
			SELECT 'Changes' AS 'Changes' -- Placeholder for tab. VBA will generate Pivot there

			----------------------------- State view, States, CHF State Flat File – Full
			EXEC usp_CF_NOVARTIS_ENTRESTO
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'CHF'
		END

		/*-- IO+Jadenu
		IF @CategoryID = 17
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('Jadenu State Data')
			SELECT * FROM @PACKING_SLIP;

			EXEC usp_CF_NOVARTIS_JADENU
				@ClientID = @ClientID,
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'IO'
		END*/




		-- Novartis Access Analysis
		IF @CategoryID = 19
		BEGIN
			
			-- Indication and Drug parameters are ignored.
			-- Packing logic is here, Ind/Drug/Filtering logic is inside SPs

			INSERT INTO @PACKING_SLIP VALUES ('Afinitor')
			-- 2019-01-08: PCF111, removal of all the drugs except Afinitor and Promacta
			--INSERT INTO @PACKING_SLIP VALUES ('Gleevec')
			--INSERT INTO @PACKING_SLIP VALUES ('Votrient')
			--INSERT INTO @PACKING_SLIP VALUES ('Sandostatin_LAR')
			INSERT INTO @PACKING_SLIP VALUES ('Promacta')
			--INSERT INTO @PACKING_SLIP VALUES ('Rydapt')
			SELECT * FROM @PACKING_SLIP;
			
			EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_AFINITOR
				@Userid = @ClientAdminUserID,
				@IndOrDrug = ''

			-- 2019-01-08: PCF111, removal of all the drugs except Afinitor and Promacta
			--EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_GLEEVEC
			--	@Userid = @ClientAdminUserID,
			--	@IndOrDrug = ''

			--EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_VOTRIENT
			--	@Userid = @ClientAdminUserID,
			--	@IndOrDrug = ''

			--EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_SANDOSTATIN_LAR
			--	@Userid = @ClientAdminUserID,
			--	@IndOrDrug = ''

			EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_PROMACTA
				@Userid = @ClientAdminUserID,
				@IndOrDrug = ''

			--EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_RYDAPT
			--	@Userid = @ClientAdminUserID,
			--	@IndOrDrug = ''

		END





		-- Novartis RCC Flat+PBM
		IF @CategoryID = 22
		BEGIN
			-- Indication and Drug parameters are ignored.
			INSERT INTO @PACKING_SLIP VALUES ('Instructions')
			INSERT INTO @PACKING_SLIP VALUES ('Pivots')
			INSERT INTO @PACKING_SLIP VALUES ('RCC Combined')
			SELECT * FROM @PACKING_SLIP;
			
			-- Instructions
			SELECT * FROM CF_NovartisRccInstructions

			-- Pivots
			SELECT 'Pivots' AS 'Pivots'

			-- RCC Combined FLAT+PBM
			EXEC usp_CF_NOVARTIS_RCC_FLAT_PBM
				@Userid = @ClientAdminUserID,
				@IndOrDrug = ''
			
		END


		-- Novartis Tasigna
		IF @CategoryID = 33
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('Tasigna CML State Data')
			SELECT * FROM @PACKING_SLIP;

			EXEC usp_CF_NOVARTIS_TASIGNA
				@ClientID = @ClientID,
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'CML'
		END


		-- Novartis Promacta
		IF @CategoryID = 34
		BEGIN

			IF @IndicationArray = 'ITP'
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('Promacta ITP State Data')
				SELECT * FROM @PACKING_SLIP;

				EXEC usp_CF_NOVARTIS_PROMACTA_ITP
					@ClientID = @ClientID,
					@Userid = @ClientAdminUserID,
					@IndOrDrug = 'ITP'
			END
			ELSE IF @IndicationArray = 'SAA'
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('Promacta SAA State Data')
				SELECT * FROM @PACKING_SLIP;

				EXEC usp_CF_NOVARTIS_PROMACTA_SAA
					@ClientID = @ClientID,
					@Userid = @ClientAdminUserID,
					@IndOrDrug = 'SAA'
			END
		END


    END



    
    
	ELSE IF  @ClientID = 6 -- BMS
	BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;

		----------------------------- Cursor for indications, reused in several loops
		DECLARE curIndications CURSOR SCROLL
		LOCAL STATIC READ_ONLY
		FOR SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs
		----------------------------- Packing Slip
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				-- All indications have flat file
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )

				-- All indications except HL and URC have PBM file
				IF (@SINGLE_INDICATION <> 'HL') AND (@SINGLE_INDICATION <> 'URC')
					BEGIN
						INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' PBM')
					END
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications
		
		SELECT * FROM @PACKING_SLIP;


		----------------------------- Flat + PBM
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				-- All indications have flat file
				IF @SINGLE_INDICATION = 'HL'
					BEGIN
						EXEC usp_CF_BMS_FLAT_HL @ClientAdminUserID, @SINGLE_INDICATION
						-- No PBM for HL
					END
				ELSE IF @SINGLE_INDICATION = 'MEL'
					BEGIN
						EXEC usp_CF_BMS_FLAT_MEL @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_MEL @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'MM'
					BEGIN
						EXEC usp_CF_BMS_FLAT_MM @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_MM @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'NSCLC'
					BEGIN
						EXEC usp_CF_BMS_FLAT_NSCLC @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_NSCLC @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'RCC'
					BEGIN
						EXEC usp_CF_BMS_FLAT_RCC @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_RCC @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'RA'
					BEGIN
						EXEC usp_CF_BMS_FLAT_RA @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_RA @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'HN'
					BEGIN
						EXEC usp_CF_BMS_FLAT_HN @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_HN @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'URC'
					BEGIN
						EXEC usp_CF_BMS_FLAT_URC @ClientAdminUserID, @SINGLE_INDICATION
						-- No PBM for URC
					END
				ELSE IF @SINGLE_INDICATION = 'CRC'
					BEGIN
						EXEC usp_CF_BMS_FLAT_CRC @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_CRC @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'HCC'
					BEGIN
						EXEC usp_CF_BMS_FLAT_HCC @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_HCC @ClientAdminUserID, @SINGLE_INDICATION
					END

				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications
		DEALLOCATE curIndications

    END



	ELSE IF  @ClientID = 9 -- Genentech
	BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;


		IF @CategoryID = 18 -- Custom File
			BEGIN
				-- Asthma (Asthma, CIU) UI allows only 1 indication. @DrugArray ignored, get back all Drugs data
				IF @IndicationArray = 'Asthma'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('Asthma')
						INSERT INTO @PACKING_SLIP VALUES ('CIU')

						INSERT INTO @PACKING_SLIP VALUES ('PBM_Asthma')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_CIU')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_ASTHMA @ClientAdminUserID, 'Asthma'
						EXEC usp_CF_GENENTECH_FLAT_CIU @ClientAdminUserID, 'CIU'

						EXEC usp_CF_GENENTECH_PBM_ASTHMA @ClientAdminUserID, 'Asthma'
						EXEC usp_CF_GENENTECH_PBM_CIU @ClientAdminUserID, 'CIU'
					END

				-- CF. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'CF'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('CF')

						INSERT INTO @PACKING_SLIP VALUES ('PBM_CF')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_CF @ClientAdminUserID, 'CF'
						EXEC usp_CF_GENENTECH_PBM_CF @ClientAdminUserID, 'CF'
					END

				-- CD. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'CD'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('CD')

						--INSERT INTO @PACKING_SLIP VALUES ('PBM_CD')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_CD @ClientAdminUserID, 'CD'
						--EXEC usp_CF_GENENTECH_PBM_CD @ClientAdminUserID, 'CD'
					END

				-- UC. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'UC'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('UC')

						--INSERT INTO @PACKING_SLIP VALUES ('PBM_UC')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_UC @ClientAdminUserID, 'UC'
						--EXEC usp_CF_GENENTECH_PBM_UC @ClientAdminUserID, 'UC'
					END

				-- IPF. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'IPF'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('IPF')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_IPF')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_IPF @ClientAdminUserID, 'IPF'
						EXEC usp_CF_GENENTECH_PBM_IPF @ClientAdminUserID, 'IPF'
					END


				-- AMD (AMD, DME, DR, RVO) UI allows only 1 indication. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'AMD'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('AMD')
						INSERT INTO @PACKING_SLIP VALUES ('DME')
						INSERT INTO @PACKING_SLIP VALUES ('DR')
						INSERT INTO @PACKING_SLIP VALUES ('RVO')
						INSERT INTO @PACKING_SLIP VALUES ('mCNV')

						INSERT INTO @PACKING_SLIP VALUES ('PBM_AMD')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_DME')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_DR')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_RVO')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_mCNV')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_AMD @ClientAdminUserID, 'AMD'
						EXEC usp_CF_GENENTECH_FLAT_DME @ClientAdminUserID, 'DME'
						EXEC usp_CF_GENENTECH_FLAT_DR @ClientAdminUserID, 'DR'
						EXEC usp_CF_GENENTECH_FLAT_RVO @ClientAdminUserID, 'RVO'
						EXEC usp_CF_GENENTECH_FLAT_MCNV @ClientAdminUserID, 'mCNV'

						EXEC usp_CF_GENENTECH_PBM_AMD @ClientAdminUserID, 'AMD'
						EXEC usp_CF_GENENTECH_PBM_DME @ClientAdminUserID, 'DME'
						EXEC usp_CF_GENENTECH_PBM_DR @ClientAdminUserID, 'DR'
						EXEC usp_CF_GENENTECH_PBM_RVO @ClientAdminUserID, 'RVO'
						EXEC usp_CF_GENENTECH_PBM_MCNV @ClientAdminUserID, 'mCNV'
					END


				-- MS. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'MS'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('MS')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_MS')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_MS @ClientAdminUserID, 'MS'
						EXEC usp_CF_GENENTECH_PBM_MS @ClientAdminUserID, 'MS'
					END


				-- BCC (AML, BCC, BC, CLL, CRC, DLBCL, GC, GIST, MCL, MM, NHL, NSCLC, OC, RCC, URC) UI allows only 1 indication. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'BCC'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('AML')
						INSERT INTO @PACKING_SLIP VALUES ('BCC')
						INSERT INTO @PACKING_SLIP VALUES ('BC')
						INSERT INTO @PACKING_SLIP VALUES ('CLL')
						INSERT INTO @PACKING_SLIP VALUES ('CRC')
						INSERT INTO @PACKING_SLIP VALUES ('DLBCL')
						INSERT INTO @PACKING_SLIP VALUES ('GC')
						INSERT INTO @PACKING_SLIP VALUES ('GIST')
						INSERT INTO @PACKING_SLIP VALUES ('MCL')
						INSERT INTO @PACKING_SLIP VALUES ('MM')
						INSERT INTO @PACKING_SLIP VALUES ('NHL')
						INSERT INTO @PACKING_SLIP VALUES ('NSCLC')
						INSERT INTO @PACKING_SLIP VALUES ('OC')
						INSERT INTO @PACKING_SLIP VALUES ('RCC')
						INSERT INTO @PACKING_SLIP VALUES ('URC')

						INSERT INTO @PACKING_SLIP VALUES ('PBM_AML')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_BCC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_BC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_CLL')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_CRC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_GC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_GIST')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_MCL')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_MM')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_NHL')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_NSCLC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_OC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_URC')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_AML @ClientAdminUserID, 'AML'
						EXEC usp_CF_GENENTECH_FLAT_BCC @ClientAdminUserID, 'BCC'
						EXEC usp_CF_GENENTECH_FLAT_BC @ClientAdminUserID, 'BC'
						EXEC usp_CF_GENENTECH_FLAT_CLL @ClientAdminUserID, 'CLL'
						EXEC usp_CF_GENENTECH_FLAT_CRC @ClientAdminUserID, 'CRC'
						EXEC usp_CF_GENENTECH_FLAT_DLBCL @ClientAdminUserID, 'DLBCL'
						EXEC usp_CF_GENENTECH_FLAT_GC @ClientAdminUserID, 'GC'
						EXEC usp_CF_GENENTECH_FLAT_GIST @ClientAdminUserID, 'GIST'
						EXEC usp_CF_GENENTECH_FLAT_MCL @ClientAdminUserID, 'MCL'
						EXEC usp_CF_GENENTECH_FLAT_MM @ClientAdminUserID, 'MM'
						EXEC usp_CF_GENENTECH_FLAT_NHL @ClientAdminUserID, 'NHL'
						EXEC usp_CF_GENENTECH_FLAT_NSCLC @ClientAdminUserID, 'NSCLC'
						EXEC usp_CF_GENENTECH_FLAT_OC @ClientAdminUserID, 'OC'
						EXEC usp_CF_GENENTECH_FLAT_RCC @ClientAdminUserID, 'RCC'
						EXEC usp_CF_GENENTECH_FLAT_URC @ClientAdminUserID, 'URC'

						EXEC usp_CF_GENENTECH_PBM_AML @ClientAdminUserID, 'AML'
						EXEC usp_CF_GENENTECH_PBM_BCC @ClientAdminUserID, 'BCC'
						EXEC usp_CF_GENENTECH_PBM_BC @ClientAdminUserID, 'BC'
						EXEC usp_CF_GENENTECH_PBM_CLL @ClientAdminUserID, 'CLL'
						EXEC usp_CF_GENENTECH_PBM_CRC @ClientAdminUserID, 'CRC'
						EXEC usp_CF_GENENTECH_PBM_GC @ClientAdminUserID, 'GC'
						EXEC usp_CF_GENENTECH_PBM_GIST @ClientAdminUserID, 'GIST'
						EXEC usp_CF_GENENTECH_PBM_MCL @ClientAdminUserID, 'MCL'
						EXEC usp_CF_GENENTECH_PBM_MM @ClientAdminUserID, 'MM'
						EXEC usp_CF_GENENTECH_PBM_NHL @ClientAdminUserID, 'NHL'
						EXEC usp_CF_GENENTECH_PBM_NSCLC @ClientAdminUserID, 'NSCLC'
						EXEC usp_CF_GENENTECH_PBM_OC @ClientAdminUserID, 'OC'
						EXEC usp_CF_GENENTECH_PBM_URC @ClientAdminUserID, 'URC'
					END

				-- RA. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('RA')
						INSERT INTO @PACKING_SLIP VALUES ('GCA')

						INSERT INTO @PACKING_SLIP VALUES ('PBM_RA')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_GCA')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_RA @ClientAdminUserID, 'RA'
						EXEC usp_CF_GENENTECH_FLAT_GCA @ClientAdminUserID, 'GCA'

						EXEC usp_CF_GENENTECH_PBM_RA @ClientAdminUserID, 'RA'
						EXEC usp_CF_GENENTECH_PBM_GCA @ClientAdminUserID, 'GCA'
					END

				-- Hemo. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'Hemo'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('Hemo')
						INSERT INTO @PACKING_SLIP VALUES ('HemoWI')

						INSERT INTO @PACKING_SLIP VALUES ('PBM_Hemo')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_HemoWI')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_HEMO @ClientAdminUserID, 'Hemo'
						EXEC usp_CF_GENENTECH_FLAT_HEMOWI @ClientAdminUserID, 'HemoWI'

						EXEC usp_CF_GENENTECH_PBM_HEMO @ClientAdminUserID, 'Hemo'
						EXEC usp_CF_GENENTECH_PBM_HEMOWI @ClientAdminUserID, 'HemoWI'
					END


				-- EXCEPTION REPORT. @IndicationArray is empty
				--ELSE IF @IndicationArray = ''
				--	BEGIN
					

					END
			END

		IF @CategoryID = 20 -- Specialty Pharmacy
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('Specialty Pharmacy - Genentech')
				SELECT * FROM @PACKING_SLIP;

				EXEC usp_CF_GENENTECH_SPECIALTY_PHARMACY @ClientAdminUserID
			END

		IF @CategoryID = 23 -- Ocular
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('Genentech Ocular')
				SELECT * FROM @PACKING_SLIP;

				-- NOTE: We can not use old SPs usp_CF_GENENTECH_FLAT_AMD, usp_CF_GENENTECH_FLAT_DME ...
				--       Because they are designed for different saved process 'Monthly Files for Genentech'
				--       And they have different set of output columns
				EXEC usp_CF_GENENTECH_OCULAR @ClientAdminUserID
			END

		IF @CategoryID = 31 -- Geo Alignment
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('Geo Alignment')
				SELECT * FROM @PACKING_SLIP;

				EXEC usp_CF_GENENTECH_GEO_ALIGNMENT
			END

		IF @CategoryID = 35 -- Exception Report
			BEGIN
					-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('Asthma')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_Asthma')
						INSERT INTO @PACKING_SLIP VALUES ('CIU')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_CIU')

						INSERT INTO @PACKING_SLIP VALUES ('CF')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_CF')

						INSERT INTO @PACKING_SLIP VALUES ('CD')

						INSERT INTO @PACKING_SLIP VALUES ('UC')

						INSERT INTO @PACKING_SLIP VALUES ('IPF')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_IPF')

						INSERT INTO @PACKING_SLIP VALUES ('AMD')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_AMD')
						INSERT INTO @PACKING_SLIP VALUES ('DME')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_DME')
						INSERT INTO @PACKING_SLIP VALUES ('DR')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_DR')
						INSERT INTO @PACKING_SLIP VALUES ('RVO')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_RVO')
						INSERT INTO @PACKING_SLIP VALUES ('mCNV')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_mCNV')

						INSERT INTO @PACKING_SLIP VALUES ('MS')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_MS')

						INSERT INTO @PACKING_SLIP VALUES ('AML')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_AML')
						INSERT INTO @PACKING_SLIP VALUES ('BCC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_BCC')
						INSERT INTO @PACKING_SLIP VALUES ('BC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_BC')
						INSERT INTO @PACKING_SLIP VALUES ('CLL')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_CLL')
						INSERT INTO @PACKING_SLIP VALUES ('CRC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_CRC')
						INSERT INTO @PACKING_SLIP VALUES ('DLBCL')
						INSERT INTO @PACKING_SLIP VALUES ('GC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_GC')
						INSERT INTO @PACKING_SLIP VALUES ('GIST')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_GIST')
						INSERT INTO @PACKING_SLIP VALUES ('MCL')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_MCL')
						INSERT INTO @PACKING_SLIP VALUES ('MM')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_MM')
						INSERT INTO @PACKING_SLIP VALUES ('NHL')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_NHL')
						INSERT INTO @PACKING_SLIP VALUES ('NSCLC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_NSCLC')
						INSERT INTO @PACKING_SLIP VALUES ('OC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_OC')
						INSERT INTO @PACKING_SLIP VALUES ('RCC')
						INSERT INTO @PACKING_SLIP VALUES ('URC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_URC')

						INSERT INTO @PACKING_SLIP VALUES ('RA')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_RA')
						INSERT INTO @PACKING_SLIP VALUES ('GCA')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_GCA')

						INSERT INTO @PACKING_SLIP VALUES ('Hemo')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_Hemo')
						INSERT INTO @PACKING_SLIP VALUES ('HemoWI')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_HemoWI')
				
						SELECT * FROM @PACKING_SLIP;

						-- Errors
						EXEC usp_CF_GENENTECH_FLAT_ASTHMA @ClientAdminUserID, 'Asthma', 1
						EXEC usp_CF_GENENTECH_PBM_ASTHMA @ClientAdminUserID, 'Asthma', 1
						EXEC usp_CF_GENENTECH_FLAT_CIU @ClientAdminUserID, 'CIU', 1
						EXEC usp_CF_GENENTECH_PBM_CIU @ClientAdminUserID, 'CIU', 1

						EXEC usp_CF_GENENTECH_FLAT_CF @ClientAdminUserID, 'CF', 1
						EXEC usp_CF_GENENTECH_PBM_CF @ClientAdminUserID, 'CF', 1

						EXEC usp_CF_GENENTECH_FLAT_CD @ClientAdminUserID, 'CD', 1

						EXEC usp_CF_GENENTECH_FLAT_UC @ClientAdminUserID, 'UC', 1

						EXEC usp_CF_GENENTECH_FLAT_IPF @ClientAdminUserID, 'IPF', 1
						EXEC usp_CF_GENENTECH_PBM_IPF @ClientAdminUserID, 'IPF', 1

						EXEC usp_CF_GENENTECH_FLAT_AMD @ClientAdminUserID, 'AMD', 1
						EXEC usp_CF_GENENTECH_PBM_AMD @ClientAdminUserID, 'AMD', 1
						EXEC usp_CF_GENENTECH_FLAT_DME @ClientAdminUserID, 'DME', 1
						EXEC usp_CF_GENENTECH_PBM_DME @ClientAdminUserID, 'DME', 1
						EXEC usp_CF_GENENTECH_FLAT_DR @ClientAdminUserID, 'DR', 1
						EXEC usp_CF_GENENTECH_PBM_DR @ClientAdminUserID, 'DR', 1
						EXEC usp_CF_GENENTECH_FLAT_RVO @ClientAdminUserID, 'RVO', 1
						EXEC usp_CF_GENENTECH_PBM_RVO @ClientAdminUserID, 'RVO', 1
						EXEC usp_CF_GENENTECH_FLAT_MCNV @ClientAdminUserID, 'mCNV', 1
						EXEC usp_CF_GENENTECH_PBM_MCNV @ClientAdminUserID, 'mCNV', 1

						EXEC usp_CF_GENENTECH_FLAT_MS @ClientAdminUserID, 'MS', 1
						EXEC usp_CF_GENENTECH_PBM_MS @ClientAdminUserID, 'MS', 1

						EXEC usp_CF_GENENTECH_FLAT_AML @ClientAdminUserID, 'AML', 1
						EXEC usp_CF_GENENTECH_PBM_AML @ClientAdminUserID, 'AML', 1
						EXEC usp_CF_GENENTECH_FLAT_BCC @ClientAdminUserID, 'BCC', 1
						EXEC usp_CF_GENENTECH_PBM_BCC @ClientAdminUserID, 'BCC', 1
						EXEC usp_CF_GENENTECH_FLAT_BC @ClientAdminUserID, 'BC', 1
						EXEC usp_CF_GENENTECH_PBM_BC @ClientAdminUserID, 'BC', 1
						EXEC usp_CF_GENENTECH_FLAT_CLL @ClientAdminUserID, 'CLL', 1
						EXEC usp_CF_GENENTECH_PBM_CLL @ClientAdminUserID, 'CLL', 1
						EXEC usp_CF_GENENTECH_FLAT_CRC @ClientAdminUserID, 'CRC', 1
						EXEC usp_CF_GENENTECH_PBM_CRC @ClientAdminUserID, 'CRC', 1
						EXEC usp_CF_GENENTECH_FLAT_DLBCL @ClientAdminUserID, 'DLBCL', 1
						EXEC usp_CF_GENENTECH_FLAT_GC @ClientAdminUserID, 'GC', 1
						EXEC usp_CF_GENENTECH_PBM_GC @ClientAdminUserID, 'GC', 1
						EXEC usp_CF_GENENTECH_FLAT_GIST @ClientAdminUserID, 'GIST', 1
						EXEC usp_CF_GENENTECH_PBM_GIST @ClientAdminUserID, 'GIST', 1
						EXEC usp_CF_GENENTECH_FLAT_MCL @ClientAdminUserID, 'MCL', 1
						EXEC usp_CF_GENENTECH_PBM_MCL @ClientAdminUserID, 'MCL', 1
						EXEC usp_CF_GENENTECH_FLAT_MM @ClientAdminUserID, 'MM', 1
						EXEC usp_CF_GENENTECH_PBM_MM @ClientAdminUserID, 'MM', 1
						EXEC usp_CF_GENENTECH_FLAT_NHL @ClientAdminUserID, 'NHL', 1
						EXEC usp_CF_GENENTECH_PBM_NHL @ClientAdminUserID, 'NHL', 1
						EXEC usp_CF_GENENTECH_FLAT_NSCLC @ClientAdminUserID, 'NSCLC', 1
						EXEC usp_CF_GENENTECH_PBM_NSCLC @ClientAdminUserID, 'NSCLC', 1
						EXEC usp_CF_GENENTECH_FLAT_OC @ClientAdminUserID, 'OC', 1
						EXEC usp_CF_GENENTECH_PBM_OC @ClientAdminUserID, 'OC', 1
						EXEC usp_CF_GENENTECH_FLAT_RCC @ClientAdminUserID, 'RCC', 1
						EXEC usp_CF_GENENTECH_FLAT_URC @ClientAdminUserID, 'URC', 1
						EXEC usp_CF_GENENTECH_PBM_URC @ClientAdminUserID, 'URC', 1

						EXEC usp_CF_GENENTECH_FLAT_RA @ClientAdminUserID, 'RA', 1
						EXEC usp_CF_GENENTECH_PBM_RA @ClientAdminUserID, 'RA', 1
						EXEC usp_CF_GENENTECH_FLAT_GCA @ClientAdminUserID, 'GCA', 1
						EXEC usp_CF_GENENTECH_PBM_GCA @ClientAdminUserID, 'GCA', 1

						EXEC usp_CF_GENENTECH_FLAT_HEMO @ClientAdminUserID, 'Hemo', 1
						EXEC usp_CF_GENENTECH_PBM_HEMO @ClientAdminUserID, 'Hemo', 1
						EXEC usp_CF_GENENTECH_FLAT_HEMOWI @ClientAdminUserID, 'HemoWI', 1
						EXEC usp_CF_GENENTECH_PBM_HEMOWI @ClientAdminUserID, 'HemoWI', 1
					
			
	END



	ELSE IF  @ClientID = 51 -- Ipsen
	BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
    
		----------------------------- Cursor for indications, reused in several loops
		SET @Indications = 'AS,ULS,CervDys,PLLS'; -- Already declared in the code above
		/*DECLARE @SINGLE_INDICATION_FULL_NAME VARCHAR(255)
		
		DECLARE curIndications CURSOR SCROLL
		LOCAL STATIC READ_ONLY
		FOR SELECT ITEM FROM dbo.fnSplit(@Indications,',') fs
			
		----------------------------- Packing Slip
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				SELECT @SINGLE_INDICATION_FULL_NAME = (SELECT TOP 1 Name FROM Indication WHERE Abbreviation = @SINGLE_INDICATION)
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' PBM')
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications*/

		-- We will add values manually, sheet names in example file don't follow any rule
		INSERT INTO @PACKING_SLIP VALUES ('Adult Spasticity')
		INSERT INTO @PACKING_SLIP VALUES ('Adult Spasticity PBM')
		INSERT INTO @PACKING_SLIP VALUES ('Upper Limb Spasticity')
		INSERT INTO @PACKING_SLIP VALUES ('Upper Limb Spasticity PBM')
		INSERT INTO @PACKING_SLIP VALUES ('CervDyst')
		INSERT INTO @PACKING_SLIP VALUES ('CervDyst PBM')
		INSERT INTO @PACKING_SLIP VALUES ('Pediatric Limb Spasticity')
		INSERT INTO @PACKING_SLIP VALUES ('Pediatric Limb Spasticity PBM')
		SELECT * FROM @PACKING_SLIP;

		EXEC usp_CF_IPSEN_FLAT_AS      @ClientAdminUserID, 'AS',      'Dysport'
		EXEC usp_CF_IPSEN_PBM_AS       @ClientAdminUserID, 'AS',      'Dysport'
		EXEC usp_CF_IPSEN_FLAT_ULS     @ClientAdminUserID, 'ULS',     'Dysport'
		EXEC usp_CF_IPSEN_PBM_ULS      @ClientAdminUserID, 'ULS',     'Dysport'
		EXEC usp_CF_IPSEN_FLAT_CERVDYS @ClientAdminUserID, 'CervDys', 'Dysport'
		EXEC usp_CF_IPSEN_PBM_CERVDYS  @ClientAdminUserID, 'CervDys', 'Dysport'
		EXEC usp_CF_IPSEN_FLAT_PLLS    @ClientAdminUserID, 'PLLS',    'Dysport'
		EXEC usp_CF_IPSEN_PBM_PLLS     @ClientAdminUserID, 'PLLS',    'Dysport'
	END


	ELSE IF  @ClientID = 83 -- Lilly
	BEGIN

		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
		
		IF @CategoryID = 28 -- Lilly BC Flat
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('BC')
				SELECT * FROM @PACKING_SLIP;

				EXEC usp_CF_LILLY_FLAT_BC @ClientAdminUserID, 'BC'
			END

		IF @CategoryID = 29 -- Lilly BC Pbm
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('BC')
				SELECT * FROM @PACKING_SLIP;

				EXEC usp_CF_LILLY_PBM_BC @ClientAdminUserID, 'BC'
			END

		IF @CategoryID = 30 -- Lilly BC State
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('BC')
				SELECT * FROM @PACKING_SLIP;

				EXEC usp_CF_LILLY_STATE_BC @ClientAdminUserID, 'BC'
			END
	END


END


GO




-----------------------------------------------------------------3. Views-------------------------------------------------------

IF object_id('vw_CF_ClientDrugIndication','v') IS NOT NULL
	DROP VIEW [dbo].[vw_CF_ClientDrugIndication]
GO

CREATE VIEW [dbo].[vw_CF_ClientDrugIndication]
AS

 SELECT
	c.ClientID,
    c.Name AS Client ,

	i.IndicationID,
    i.Name AS Indication ,
	IndicationAbbrv = i.Abbreviation,

	d.DrugID,
    d.Name AS Drug
 FROM
    ClientDrugIndication cdi INNER JOIN dbo.Indication i ON i.IndicationID = cdi.IndicationID
	INNER JOIN dbo.Client c ON c.ClientID = cdi.ClientID
	INNER JOIN dbo.Drug d ON d.DrugID = cdi.DrugID
 WHERE 
 (
  c.ClientID IN ( 32 ) -- Merck
  AND i.IndicationID IN ( 87, 90 )        -- MEL, NSCLC
  AND d.DrugID IN ( 6776, 6794 , 6596, 6756, 6597, 6592 )   -- Keytruda, Opdivo ( Weekly Files use these )                 -- Alimta, Cyramza, Tarceva, Yervoy ( Additionally Monthly Files use these )
 )
OR
 (
  c.ClientID IN ( 14 ) -- Janssen
  AND i.IndicationID IN ( 72, 73, 78, 88, 93, 109, 111 )   --  RA, CD, FC, MM, PRC, PSO, UC                  
 )
OR
 (
  c.ClientID IN ( 67 ) -- Sandoz
  AND i.IndicationID IN ( 116, 72/*, 111, 73, 109*/ , 79, 76, 174 )   --  NEU, RA/*, UC, CD, PSO*/ , NHL, CLL, GHD
 )
OR
 (
  c.ClientID IN ( 58 ) -- Regeneron
  AND i.IndicationID IN ( 138, 72 )   --  HCHL, RA
 )
OR
 (
  c.ClientID IN ( 63 ) -- Heron
  AND i.IndicationID IN ( 139 )   -- CINV
 )
OR
 (
  c.ClientID IN ( 41 ) -- Regeneron (Beghou)
  AND i.IndicationID IN ( 95, 100, 110, 132 )   -- AMD, DME, RVO, DR
 )
OR
 (
  c.ClientID IN ( 15 ) -- Novartis (Entresto), Novartis (Jadenu)       
                       -- Novartis Monthly Access Analysis
  AND i.IndicationID IN ( 135, 148,     -- CHF, IO
                       74, 151, 149, 152, 94, 150, 77, 84, 156, 127, 147, 153, 155) -- BC, GI_NET, Lung_NET, P_NET, RCC, TSC, CML, GIST, STS, Acromegaly, NET, ITP, SAA
 )
OR
 (
  c.ClientID IN ( 6 ) -- BMS
  AND i.IndicationID IN ( 159, 87, 88, 90, 94, 72, 86, 158, 81, 85 )   -- HL, MEL, MM, NSCLC, RCC, RA, HN, URC, CRC, HCC
 )
OR
 (
  c.ClientID IN ( 9 ) -- Genentech
  AND i.IndicationID IN ( 96, 123, 129, 134, 95, 100, 132, 110, 107, 75, 74, 76, 83, 84, 124, 79, 90, 91, 158, 72, 170, 137, 167, 73, 111, 81, 160, 191, 88, 94 )   
  -- Asthma, CIU, CF, IPF, AMD, DME, DR, RVO, MS, BCC, BC, CLL, GC, GIST, MCL, NHL, NSCLC, OC, URC, RA, mCNV, Hemo, GCA, CD, UC, CRC, AML, DLBCL, MM, RCC
 )
OR
 (
  c.ClientID IN ( 51 ) -- Ipsen
  AND i.IndicationID IN ( 168, 145, 128, 157 )   -- AS, ULS, CervDys, PLLS
 )
OR
 (
  c.ClientID IN ( 83 ) -- Lilly
  AND i.IndicationID IN ( 74 )   -- BC
 )

GO

