--------------------------------------INSERTION SCLC--------------------------------------
DECLARE @IndicationID_SCLC INT = (SELECT IndicationID FROM Indication WHERE Abbreviation = 'SCLC')
DECLARE @ProcessID INT = 25

IF NOT EXISTS (SELECT 1 FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'SCLC')
	INSERT INTO ProcessDetail VALUES(@ProcessID, 110, 'SCLC', 1)


DECLARE @PdID_SCLC INT = (SELECT ProcessDetailID FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'SCLC')

IF NOT EXISTS (SELECT 1 FROM ClientIndication WHERE ClientID = 6 AND IndicationID = @IndicationID_SCLC)
	INSERT INTO ClientIndication VALUES (@PdID_SCLC, 6, @IndicationID_SCLC)


IF NOT EXISTS (SELECT 1 FROM ClientDrugs cd
INNER JOIN ClientIndication ci ON cd.ClientIndicationID = ci.ClientIndicationID
WHERE ci.ClientID = 6 AND ci.IndicationId = @IndicationID_SCLC)
	INSERT INTO ClientDrugs
	SELECT DISTINCT ci.ClientIndicationID, d.DrugID FROM ClientDrugIndication cdi
	INNER JOIN ClientIndication ci ON ci.ClientID = cdi.ClientID AND ci.IndicationID = cdi.IndicationID
	INNER JOIN DRUG d ON d.DrugID = cdi.DrugID
	WHERE cdi.ClientID = 6 AND cdi.IndicationId = @IndicationID_SCLC
	