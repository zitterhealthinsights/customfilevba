--------------------------------------INSERTION CD--------------------------------------
DECLARE @IndicationID_CD INT = (SELECT IndicationID FROM Indication WHERE Abbreviation = 'CD')
DECLARE @ProcessID INT = 27

IF NOT EXISTS (SELECT 1 FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'CD')
	INSERT INTO ProcessDetail VALUES(@ProcessID, 90, 'CD', 1)


DECLARE @PdID_CD INT = (SELECT ProcessDetailID FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'CD')

IF NOT EXISTS (SELECT 1 FROM ClientIndication WHERE ClientID = 9 AND IndicationID = @IndicationID_CD)
	INSERT INTO ClientIndication VALUES (@PdID_CD, 9, @IndicationID_CD)


IF NOT EXISTS (SELECT 1 FROM ClientDrugs cd
INNER JOIN ClientIndication ci ON cd.ClientIndicationID = ci.ClientIndicationID
WHERE ci.ClientID = 9 AND ci.IndicationId = @IndicationID_CD)
	INSERT INTO ClientDrugs
	SELECT DISTINCT ci.ClientIndicationID, d.DrugID FROM ClientDrugIndication cdi
	INNER JOIN ClientIndication ci ON ci.ClientID = cdi.ClientID AND ci.IndicationID = cdi.IndicationID
	INNER JOIN DRUG d ON d.DrugID = cdi.DrugID
	WHERE cdi.ClientID = 9 AND cdi.IndicationId = @IndicationID_CD
	
	
	

----------------------------------INSERTION UC----------------------------------------------
DECLARE @IndicationID_UC INT = (SELECT IndicationID FROM Indication WHERE Abbreviation = 'UC')

IF NOT EXISTS (SELECT 1 FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'UC')
	INSERT INTO ProcessDetail VALUES(@ProcessID, 100, 'UC', 1)


DECLARE @PdID_UC INT = (SELECT ProcessDetailID FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'UC')

IF NOT EXISTS (SELECT 1 FROM ClientIndication WHERE ClientID = 9 AND IndicationID = @IndicationID_UC)
	INSERT INTO ClientIndication VALUES (@PdID_UC, 9, @IndicationID_UC)


IF NOT EXISTS (SELECT 1 FROM ClientDrugs cd
INNER JOIN ClientIndication ci ON cd.ClientIndicationID = ci.ClientIndicationID
WHERE ci.ClientID = 9 AND ci.IndicationId = @IndicationID_UC)
	INSERT INTO ClientDrugs
	SELECT DISTINCT ci.ClientIndicationID, d.DrugID FROM ClientDrugIndication cdi
	INNER JOIN ClientIndication ci ON ci.ClientID = cdi.ClientID AND ci.IndicationID = cdi.IndicationID
	INNER JOIN DRUG d ON d.DrugID = cdi.DrugID
	WHERE cdi.ClientID = 9 AND cdi.IndicationId = @IndicationID_UC