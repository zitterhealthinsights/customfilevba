--------------------------------------INSERTION NMOSD--------------------------------------
DECLARE @IndicationID_NMOSD INT = (SELECT IndicationID FROM Indication WHERE Abbreviation = 'NMOSD')
DECLARE @ProcessID INT = 27

IF NOT EXISTS (SELECT 1 FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'NMOSD')
	INSERT INTO ProcessDetail VALUES(@ProcessID, 110, 'NMOSD', 1)


DECLARE @PdID_NMOSD INT = (SELECT ProcessDetailID FROM ProcessDetail WHERE ProcessID = @ProcessID AND StepDescription = 'NMOSD')

IF NOT EXISTS (SELECT 1 FROM ClientIndication WHERE ClientID = 9 AND IndicationID = @IndicationID_NMOSD)
	INSERT INTO ClientIndication VALUES (@PdID_NMOSD, 9, @IndicationID_NMOSD)


IF NOT EXISTS (SELECT 1 FROM ClientDrugs cd
INNER JOIN ClientIndication ci ON cd.ClientIndicationID = ci.ClientIndicationID
WHERE ci.ClientID = 9 AND ci.IndicationId = @IndicationID_NMOSD)
	INSERT INTO ClientDrugs
	SELECT DISTINCT ci.ClientIndicationID, d.DrugID FROM ClientDrugIndication cdi
	INNER JOIN ClientIndication ci ON ci.ClientID = cdi.ClientID AND ci.IndicationID = cdi.IndicationID
	INNER JOIN DRUG d ON d.DrugID = cdi.DrugID
	WHERE cdi.ClientID = 9 AND cdi.IndicationId = @IndicationID_NMOSD
	