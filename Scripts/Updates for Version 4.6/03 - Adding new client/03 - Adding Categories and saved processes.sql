--USE [CWP_3.0]

SELECT c.* FROM dbo.Category c ORDER BY c.CategoryID DESC
SELECT cc.* FROM dbo.ClientCategory cc ORDER BY cc.ClientCategoryID DESC


INSERT dbo.Category (CategoryName,Active)
VALUES('Lilly BC Flat',1) -- New ID should be 28
GO
INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(83,28,1) -- New ID should be 28
GO


INSERT dbo.Category (CategoryName,Active)
VALUES('Lilly BC Pbm',1) -- New ID should be 29
GO
INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(83,29,1) -- New ID should be 29
GO

INSERT dbo.Category (CategoryName,Active)
VALUES('Lilly BC State',1) -- New ID should be 30
GO
INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(83,30,1) -- New ID should be 30
GO



SELECT c.* FROM dbo.Category c ORDER BY c.CategoryID DESC
SELECT cc.* FROM dbo.ClientCategory cc ORDER BY cc.ClientCategoryID DESC


----------------------------------------------------------------------------------------

EXEC usp_CF_SaveProcess
    @ProcessID = 0, -- Add, not Update
    @ProcessName = 'Lilly - BC FLAT',
    @ScheduleTypeID = 2, -- Monthly
    @LastRunUserName = 'Milan', 
    @CategoryID = 28, -- Lilly BC Flat
    @ClientID = 83, -- Lilly
    @ExportFilePath = 'C:\Temp\'
GO
EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 37, -- the one we just created
    @ClientID = 83, -- Lilly
    @StepDescription = 'Lilly - BC FLAT',
    @IndicationNames = 'BC', -- just a placeholder
    @DrugNames = 'Ibrance' -- just a placeholder
GO



EXEC usp_CF_SaveProcess
    @ProcessID = 0, -- Add, not Update
    @ProcessName = 'Lilly - BC PBM',
    @ScheduleTypeID = 2, -- Monthly
    @LastRunUserName = 'Milan', 
    @CategoryID = 29, -- Lilly BC Pbm
    @ClientID = 83, -- Lilly
    @ExportFilePath = 'C:\Temp\'
GO
EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 38, -- the one we just created
    @ClientID = 83, -- Lilly
    @StepDescription = 'Lilly - BC PBM',
    @IndicationNames = 'BC', -- just a placeholder
    @DrugNames = 'Ibrance' -- just a placeholder
GO



EXEC usp_CF_SaveProcess
    @ProcessID = 0, -- Add, not Update
    @ProcessName = 'Lilly - BC STATE',
    @ScheduleTypeID = 2, -- Monthly
    @LastRunUserName = 'Milan', 
    @CategoryID = 30, -- Lilly BC State
    @ClientID = 83, -- Lilly
    @ExportFilePath = 'C:\Temp\'
GO
EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 39, -- the one we just created
    @ClientID = 83, -- Lilly
    @StepDescription = 'Lilly - BC State',
    @IndicationNames = 'BC', -- just a placeholder
    @DrugNames = 'Ibrance' -- just a placeholder
GO


SELECT * FROM Process ORDER BY ProcessID DESC
SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC