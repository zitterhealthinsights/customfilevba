--USE [CWP_3.0]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_Get_ClientList]    Script Date: 7/16/2018 3:40:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--- TEST BLOX SURVIVE
-- 	SELECT * FROM dbo.Client c WHERE ClientID IN (32, 14, 67, 58, 63, 41, 15, 6)
-- 3/1/17 - DH 
-- Removed Merck (32 )  and Janssen ( 14 ) due to subscription removal ( John Griggs )


ALTER PROC [dbo].[usp_CF_Get_ClientList] ( @ClientName AS VARCHAR(150) )

AS

IF @ClientName = '<ALL>' 
	BEGIN
		 
			SELECT 0 AS ClientID, '<ALL>' AS Name
			UNION  
			SELECT DISTINCT  ClientID, [Name] = ClientName FROM vw_ClientIndication WHERE ClientID IN ( 67, 58, 63, 41, 15, 6, 9, 51, 83)
		 
	END 
ELSE
	BEGIN 
		SELECT 0 AS ClientID, '<ALL>' AS Name
		UNION  
		SELECT DISTINCT ClientID, [Name] = ClientName  FROM vw_ClientIndication WHERE ClientName = @ClientName 
	END 


GO


