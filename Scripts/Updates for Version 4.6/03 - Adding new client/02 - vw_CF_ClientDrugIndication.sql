--USE [CWP_3.0]
--GO

/****** Object:  View [dbo].[vw_CF_ClientDrugIndication]    Script Date: 7/16/2018 3:43:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[vw_CF_ClientDrugIndication]

AS

 SELECT
	c.ClientID,
    c.Name AS Client ,

	i.IndicationID,
    i.Name AS Indication ,
	IndicationAbbrv = i.Abbreviation,

	d.DrugID,
    d.Name AS Drug
 FROM
    ClientDrugIndication cdi INNER JOIN dbo.Indication i ON i.IndicationID = cdi.IndicationID
	INNER JOIN dbo.Client c ON c.ClientID = cdi.ClientID
	INNER JOIN dbo.Drug d ON d.DrugID = cdi.DrugID
 WHERE 
 (
  c.ClientID IN ( 32 ) -- Merck
  AND i.IndicationID IN ( 87, 90 )        -- MEL, NSCLC
  AND d.DrugID IN ( 6776, 6794 , 6596, 6756, 6597, 6592 )   -- Keytruda, Opdivo ( Weekly Files use these )                 -- Alimta, Cyramza, Tarceva, Yervoy ( Additionally Monthly Files use these )
 )
OR
 (
  c.ClientID IN ( 14 ) -- Janssen
  AND i.IndicationID IN ( 72, 73, 78, 88, 93, 109, 111 )   --  RA, CD, FC, MM, PRC, PSO, UC                  
 )
OR
 (
  c.ClientID IN ( 67 ) -- Sandoz
  AND i.IndicationID IN ( 116, 72/*, 111, 73, 109*/ , 79, 76, 174 )   --  NEU, RA/*, UC, CD, PSO*/ , NHL, CLL, GHD
 )
OR
 (
  c.ClientID IN ( 58 ) -- Regeneron
  AND i.IndicationID IN ( 138, 72 )   --  HCHL, RA
 )
OR
 (
  c.ClientID IN ( 63 ) -- Heron
  AND i.IndicationID IN ( 139 )   -- CINV
 )
OR
 (
  c.ClientID IN ( 41 ) -- Regeneron (Beghou)
  AND i.IndicationID IN ( 95, 100, 110, 132 )   -- AMD, DME, RVO, DR
 )
OR
 (
  c.ClientID IN ( 15 ) -- Novartis (Entresto), Novartis (Jadenu)       
                       -- Novartis Monthly Access Analysis
  AND i.IndicationID IN ( 135, 148,     -- CHF, IO
                       74, 151, 149, 152, 94, 150, 77, 84, 156, 127, 147, 153, 155) -- BC, GI_NET, Lung_NET, P_NET, RCC, TSC, CML, GIST, STS, Acromegaly, NET, ITP, SAA
 )
OR
 (
  c.ClientID IN ( 6 ) -- BMS
  AND i.IndicationID IN ( 159, 87, 88, 90, 94, 72, 86, 158, 81, 85 )   -- HL, MEL, MM, NSCLC, RCC, RA, HN, URC, CRC, HCC
 )
OR
 (
  c.ClientID IN ( 9 ) -- Genentech
  AND i.IndicationID IN ( 96, 123, 129, 134, 95, 100, 132, 110, 107, 75, 74, 76, 83, 84, 124, 79, 90, 91, 158, 72, 170, 137, 167  )   
  -- Asthma, CIU, CF, IPF, AMD, DME, DR, RVO, MS, BCC, BC, CLL, GC, GIST, MCL, NHL, NSCLC, OC, URC, RA, mCNV, Hemo, GCA
 )
OR
 (
  c.ClientID IN ( 51 ) -- Ipsen
  AND i.IndicationID IN ( 168, 145, 128, 157 )   -- AS, ULS, CervDys, PLLS
 )
OR
 (
  c.ClientID IN ( 83 ) -- Lilly
  AND i.IndicationID IN ( 74 )   -- BC
 )

GO


