--USE [CWP_3.0]
--GO

SELECT * FROM Process WHERE ProcessName LIKE '%Sandoz%'
SELECT * FROM ProcessDetail WHERE ProcessID = 16 AND Active = 1

--EXEC usp_CF_DeactivateStep 36
--EXEC usp_CF_SaveStep
--    @ProcessDetailID = 0, -- Add, not Update
--    @ProcessID = 16, -- Sandoz Custom File
--    @ClientID = 67, -- Sandoz
--    @StepDescription = 'NEU',
--    @IndicationNames = 'NEU',
--    @DrugNames = 'Granix' -- just a placeholder
--GO
--EXEC usp_CF_SaveStep
--    @ProcessDetailID = 0, -- Add, not Update
--    @ProcessID = 16, -- Sandoz Custom File
--    @ClientID = 67, -- Sandoz
--    @StepDescription = 'GHD',
--    @IndicationNames = 'GHD',
--    @DrugNames = 'Genotropin' -- just a placeholder
--GO
--EXEC usp_CF_SaveStep
--    @ProcessDetailID = 0, -- Add, not Update
--    @ProcessID = 16, -- Sandoz Custom File
--    @ClientID = 67, -- Sandoz
--    @StepDescription = 'CLL and NHL',
--    @IndicationNames = 'CLL',
--	-- (we couldn't put 'CLL,NHL' to act like "join indications into file" because all the other steps act as "separate indications into different files")
--    @DrugNames = 'Arzerra' -- just a placeholder
--GO
--EXEC usp_CF_SaveStep
--    @ProcessDetailID = 0, -- Add, not Update
--    @ProcessID = 16, -- Sandoz Custom File
--    @ClientID = 67, -- Sandoz
--    @StepDescription = 'RA',
--    @IndicationNames = 'RA',
--    @DrugNames = 'Cimzia' -- just a placeholder
--GO

SELECT * FROM ProcessDetail WHERE ProcessID = 16 AND Active = 1