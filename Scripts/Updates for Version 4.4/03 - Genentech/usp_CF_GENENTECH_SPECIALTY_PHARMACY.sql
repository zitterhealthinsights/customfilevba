--USE [CWP_3.0]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_GENENTECH_SPECIALTY_PHARMACY]    Script Date: 4/12/2018 3:30:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- EXEC usp_CF_GENENTECH_SPECIALTY_PHARMACY 2630 -- Genentech_Admin
ALTER PROCEDURE [dbo].[usp_CF_GENENTECH_SPECIALTY_PHARMACY]
	@Userid as INT
AS
BEGIN

DECLARE @IndicationArray VARCHAR(1000) = 'Asthma,CIU,CF,IPF,AMD,DME,DR,RVO,MS,BCC,BC,CLL,GC,GIST,MCL,NHL,NSCLC,OC,URC,RA,Hemo,mCNV,GCA'
DECLARE @SingleIndication VARCHAR(255)
DECLARE @GetPlansResult AS CF_GENENTECH_SPECIALTY_PHARMACY_FOR_GET_PLANS
DECLARE @OneIndAll AS CF_GENENTECH_SPECIALTY_PHARMACY
DECLARE @OneIndFinal AS CF_GENENTECH_SPECIALTY_PHARMACY
DECLARE @Output AS CF_GENENTECH_SPECIALTY_PHARMACY

DECLARE @OutputWithDleNo AS CF_GENENTECH_SPECIALTY_PHARMACY
DECLARE @OutputWithReplacements AS CF_GENENTECH_SPECIALTY_PHARMACY
DECLARE @OutputNoDuplicates AS CF_GENENTECH_SPECIALTY_PHARMACY

	-- Prepare @selectedFields for calling usp_Get_Plans
	DECLARE @selectedFields as varchar(max)
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_GENENTECH_SPECIALTY_PHARMACY_FOR_GET_PLANS','');

	
	DELETE FROM @Output

	-- Loop by Indications
	DECLARE INDICATION_CURSOR CURSOR
	LOCAL STATIC READ_ONLY FORWARD_ONLY
	FOR 
		SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs

		OPEN INDICATION_CURSOR
		FETCH NEXT FROM INDICATION_CURSOR INTO @SingleIndication
		WHILE @@FETCH_STATUS = 0
		BEGIN 

				-- Call get plans (FLAT) for single indication
				DELETE FROM @GetPlansResult
				INSERT INTO @GetPlansResult
					EXEC dbo.usp_Get_Plans
						@IndicationorDrug = @SingleIndication,
						@Userid = @Userid,
						@isPBMOnly = 0,
						@SelectFields = @selectedFields
				

				-- Store Get plans result with Specialty_Pharmacy <- Name_Of_Specialty_Drug_Distributer_1
				DELETE FROM @OneIndAll
				INSERT INTO @OneIndAll
					SELECT
						[Mcoid] = [Mcoid],
						[PayerName] = [PayerName],
						[Plan_Name] = [Plan_Name],
						[Segment] = [Segment],
						[Distribution_Limitations_Enforced] = [Distribution_Limitations_Enforced],
						[Drug_Name] = [Drug_Name],
						[Indication] = [Indication],
						[Specialty_Pharmacy] = [Name_Of_Specialty_Drug_Distributer_1],
						[Data_Extraction_Date] = CONVERT(VARCHAR(10), GETDATE() , 101)
					FROM @GetPlansResult

				-- Add more records with Specialty_Pharmacy <- Name_Of_Specialty_Drug_Distributer_2
				INSERT INTO @OneIndAll
					SELECT
						[Mcoid] = [Mcoid],
						[PayerName] = [PayerName],
						[Plan_Name] = [Plan_Name],
						[Segment] = [Segment],
						[Distribution_Limitations_Enforced] = [Distribution_Limitations_Enforced],
						[Drug_Name] = [Drug_Name],
						[Indication] = [Indication],
						[Specialty_Pharmacy] = [Name_Of_Specialty_Drug_Distributer_2],
						[Data_Extraction_Date] = CONVERT(VARCHAR(10), GETDATE() , 101)
					FROM @GetPlansResult

				-- 2017-10-12 Milan: v3.5 removed per Tanya request in email (2017-10-11)
				---- Delete rows with invalid Specialty_Pharmacy
				--DELETE FROM @OneIndAll
				--WHERE [Specialty_Pharmacy] IN ('Not Specified', 'Not Applicable', 'Not Aplicable')


				-- Delete duplicate records
				DELETE FROM @OneIndFinal
				INSERT INTO @OneIndFinal
					SELECT DISTINCT [Mcoid], [PayerName], [Plan_Name], [Segment], [Distribution_Limitations_Enforced], [Drug_Name], [Indication], [Specialty_Pharmacy], [Data_Extraction_Date]
					FROM @OneIndAll

				-- Add to Output
				INSERT INTO @Output
					SELECT 
						[Mcoid],
						[PayerName],
						[Plan_Name],
						[Segment],
						[Distribution_Limitations_Enforced],
						[Drug_Name],
						[Indication],
						[Specialty_Pharmacy],
						[Data_Extraction_Date]
					FROM @OneIndFinal

			FETCH NEXT FROM INDICATION_CURSOR INTO @SingleIndication
		END 
	CLOSE INDICATION_CURSOR
	DEALLOCATE INDICATION_CURSOR

	-- 2017-09-21 Milan: extension
	-- Where Distribution_Limitations_Enforced = 'No' (for every Drug loop)
	-- Do an Union with table(excel wishlist file)  table name: GenentechSpecialtyPharmacyCompiledList {ingore Maryland Care sheet}
	-- by columns Mcoid	PayerName	Plan_Name	Segment
	-- Remove the duplicates again

	/*SELECT
		[Mcoid],
		[PayerName],
		[Plan_Name],
		[Segment],
		[Distribution_Limitations_Enforced],
		[Drug_Name],
		[Indication],
		[Specialty_Pharmacy]
	FROM @Output
	--ORDER BY [Distribution_Limitations_Enforced] DESC, [Mcoid], [Indication], [Specialty_Pharmacy], [Drug_Name]
	ORDER BY [Mcoid], [Indication], [Distribution_Limitations_Enforced] DESC, [Drug_Name], [Specialty_Pharmacy]
	*/

	-- Pick rows where: Distribution_Limitations_Enforced = 'No' -> put them into @OutputWithDleNo
	DELETE FROM @OutputWithDleNo
	INSERT INTO @OutputWithDleNo
		SELECT 
			[Mcoid],
			[PayerName],
			[Plan_Name],
			[Segment],
			[Distribution_Limitations_Enforced],
			[Drug_Name],
			[Indication],
			[Specialty_Pharmacy],
			[Data_Extraction_Date]
		FROM @Output
		WHERE [Distribution_Limitations_Enforced] = 'No'


	-- Now when records with DLE = 'No' are stored in another table - delete them from the outputs
	DELETE FROM @Output
	WHERE [Distribution_Limitations_Enforced] = 'No'


	-- Loop through each row in @OutputWithDleNo, read values into variables.
	-- Then join with GenentechSpecialtyPharmacyCompiledList and add those records to @Output
	declare @Mcoid varchar(max),
            @PayerName  varchar(max),
			@Plan_Name  varchar(max),
			@Segment  varchar(max),
			@Distribution_Limitations_Enforced varchar(max),
            @Drug_Name varchar(max),
			@Indication varchar(max),
			@Specialty_Pharmacy varchar(max),
			@Data_Extraction_Date varchar(max)

	declare crsMyTblParams cursor for
			select [Mcoid], [PayerName], [Plan_Name], [Segment], [Distribution_Limitations_Enforced], [Drug_Name], [Indication], [Specialty_Pharmacy], [Data_Extraction_Date]
			from @OutputWithDleNo

	open crsMyTblParams
	fetch next from crsMyTblParams into @Mcoid, @PayerName, @Plan_Name, @Segment, @Distribution_Limitations_Enforced, @Drug_Name, @Indication, @Specialty_Pharmacy, @Data_Extraction_Date
	WHILE @@FETCH_STATUS = 0
		BEGIN

			INSERT INTO @Output
				SELECT
					[Mcoid] = @Mcoid,
					[PayerName] = @PayerName,
					[Plan_Name] = @Plan_Name,
					[Segment] = @Segment,
					[Distribution_Limitations_Enforced] = @Distribution_Limitations_Enforced,
					[Drug_Name] = @Drug_Name,
					[Indication] = @Indication,
					[Specialty_Pharmacy] = gspcl.SpecialtyPharmacy, -- <<< --- this one comes from the GenentechSpecialtyPharmacyCompiledList
					[Data_Extraction_Date] = @Data_Extraction_Date
				FROM GenentechSpecialtyPharmacyCompiledList gspcl
				WHERE gspcl.Mcoid = @Mcoid
				AND gspcl.PayerName = @PayerName
				AND gspcl.Plan_Name = @Plan_Name
				AND gspcl.PlanType = @Segment

			fetch next from crsMyTblParams into @Mcoid, @PayerName, @Plan_Name, @Segment, @Distribution_Limitations_Enforced, @Drug_Name, @Indication, @Specialty_Pharmacy, @Data_Extraction_Date
		END 
	CLOSE crsMyTblParams
	DEALLOCATE crsMyTblParams

	-- Do the replacements on Specialty Pharmacy
	DELETE FROM @OutputWithReplacements
	INSERT INTO @OutputWithReplacements
		SELECT
			[Mcoid] = o.Mcoid,
			[PayerName] = o.PayerName,
			[Plan_Name] = o.Plan_Name,
			[Segment] = o.Segment,
			[Distribution_Limitations_Enforced] = o.Distribution_Limitations_Enforced,
			[Drug_Name] = o.Drug_Name,
			[Indication] = o.Indication,
			[Specialty_Pharmacy] = CASE WHEN o.Specialty_Pharmacy = 'Acaria Health Specialty Pharmacy' THEN 'AcariaHealth'
										WHEN o.Specialty_Pharmacy = 'Acro Pharaceuticals ' THEN 'Acro Pharmaceutical Services'
										WHEN o.Specialty_Pharmacy = 'Acro Pharmaceuticals' THEN 'Acro Pharmaceutical Services'
										WHEN o.Specialty_Pharmacy = 'Aetna' THEN 'Aetna Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'Ardon Health ' THEN 'Ardon Health Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'Caremark Specialty Pharmacy' THEN 'CVS Caremark Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'Cigna Specialty Pharmacy Services' THEN 'CIGNA Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'CVS Caremark' THEN 'CVS Caremark Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'CVS Caremark, Walgreens Specialty Pharmacy' THEN 'CVS Caremark Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'Diplomat' THEN 'Diplomat Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'EnvisionSpecialty' THEN 'Envision Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'Exactus' THEN 'Exactus Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'Fairview Specialty Services' THEN 'Fairview Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'Magellan Rx' THEN 'Magellan Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'MagellanRx' THEN 'Magellan Specialty Pharmacy'
										WHEN o.Specialty_Pharmacy = 'PerformRx' THEN 'PerformRx Specialty Network'
										WHEN o.Specialty_Pharmacy = 'PerformSpecialty' THEN 'PerformRx Specialty Network'
										WHEN o.Specialty_Pharmacy = 'Walgreens Specialty' THEN 'Walgreens Specialty Pharmacy'
									ELSE
										o.Specialty_Pharmacy
									END,
			[Data_Extraction_Date] = o.Data_Extraction_Date
		FROM @Output o

	-- Delete duplicate records
	DELETE FROM @OutputNoDuplicates
	INSERT INTO @OutputNoDuplicates
		SELECT DISTINCT [Mcoid], [PayerName], [Plan_Name], [Segment], [Distribution_Limitations_Enforced], [Drug_Name], [Indication], [Specialty_Pharmacy], [Data_Extraction_Date]
		FROM @OutputWithReplacements


	SELECT
		[Mcoid],
		[Payer_Name] = [PayerName], --Tanya in email 2018-04-10
		[Plan_Name],
		[Segment],
		[Distribution_Limitations_Enforced],
		[Drug_Name],
		[Indication],
		[Specialty_Pharmacy],
		[Data_Extraction_Date]
	FROM @OutputNoDuplicates
	ORDER BY [Mcoid], [Indication], [Distribution_Limitations_Enforced] DESC, [Drug_Name], [Specialty_Pharmacy], [Data_Extraction_Date]

END




GO


