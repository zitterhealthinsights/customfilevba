--USE [PATT]
--GO

SELECT c.* FROM dbo.Category c ORDER BY 1 DESC
SELECT cc.* FROM dbo.ClientCategory cc ORDER BY 1 DESC

/*
INSERT dbo.Category (CategoryName,Active)
VALUES('Novartis Tasigna',1) -- New ID should be 33

INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(15,33,1) -- New ID should be 33
*/
/*
INSERT dbo.Category (CategoryName,Active)
VALUES('Novartis Promacta',1) -- New ID should be 34

INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(15,34,1) -- New ID should be 34
*/

SELECT c.* FROM dbo.Category c ORDER BY 1 DESC
SELECT cc.* FROM dbo.ClientCategory cc ORDER BY 1 DESC