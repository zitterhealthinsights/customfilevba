--USE [PATT]
--GO

SELECT * FROM Process
WHERE ClientID = 15

UPDATE Process
	SET Active = 0
WHERE ProcessID = 26

/*
EXEC usp_CF_SaveProcess
    @ProcessID = 0, -- Add, not Update
    @ProcessName = 'Weekly Files for Novartis Tasigna',
    @ScheduleTypeID = 1, -- Weekly
    @LastRunUserName = 'Milan', 
    @CategoryID = 33, -- Novartis Tasigna
    @ClientID = 15, -- Novartis
    @ExportFilePath = 'C:\Temp\'
GO
SELECT * FROM Process ORDER BY ProcessID DESC

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 42, -- the one we just created
    @ClientID = 15, -- Novartis
    @StepDescription = 'CML Tasigna',
    @IndicationNames = 'CML', -- just a placeholder
    @DrugNames = 'Tasigna' -- just a placeholder
GO
SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC
*/


/*
EXEC usp_CF_SaveProcess
    @ProcessID = 0, -- Add, not Update
    @ProcessName = 'Weekly Files for Novartis Promacta',
    @ScheduleTypeID = 1, -- Weekly
    @LastRunUserName = 'Milan', 
    @CategoryID = 34, -- Novartis Promacta
    @ClientID = 15, -- Novartis
    @ExportFilePath = 'C:\Temp\'
GO
SELECT * FROM Process ORDER BY ProcessID DESC

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 43, -- the one we just created
    @ClientID = 15, -- Novartis
    @StepDescription = 'ITP Promacta',
    @IndicationNames = 'ITP', -- just a placeholder
    @DrugNames = 'Promacta' -- just a placeholder
GO
SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 43, -- the one we just created
    @ClientID = 15, -- Novartis
    @StepDescription = 'SAA Promacta',
    @IndicationNames = 'SAA', -- just a placeholder
    @DrugNames = 'Promacta' -- just a placeholder
GO
SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC
*/

SELECT * FROM Process
WHERE ClientID = 15

SELECT * FROM Process ORDER BY ProcessID DESC
SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC