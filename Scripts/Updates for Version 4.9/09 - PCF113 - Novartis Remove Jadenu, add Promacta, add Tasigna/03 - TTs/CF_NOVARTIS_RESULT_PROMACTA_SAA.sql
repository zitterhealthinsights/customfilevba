--USE [PATT]
--GO

/****** Object:  UserDefinedTableType [dbo].[CF_NOVARTIS_RESULT_PROMACTA_SAA]    Script Date: 2/11/2019 9:24:02 PM ******/
CREATE TYPE [dbo].[CF_NOVARTIS_RESULT_PROMACTA_SAA] AS TABLE(
	[STATE_CD] [varchar](max) NULL,
	[STATE_NAME] [varchar](max) NULL,
	[PLAN_ID] [varchar](max) NULL,
	[PAYER_NAME] [varchar](max) NULL,
	[PLAN_NAME] [varchar](max) NULL,
	[PLAN_RANK] [varchar](max) NULL,
	[LIVES] [varchar](max) NULL,
	[PLAN_TYPE] [varchar](max) NULL,
	[TIER] [varchar](max) NULL,
	[TIER_NUMBER] [varchar](max) NULL,
	[INDICATION] [varchar](max) NULL,
	[DW_INS_DT] [varchar](max) NULL,
	[RETAIL_COPAY_MIN] [varchar](max) NULL,
	[RETAIL_COPAY_MAX] [varchar](max) NULL,
	[MO_COPAY_MIN] [varchar](max) NULL,
	[MO_COPAY_MAX] [varchar](max) NULL,
	[POLICY_URL] [varchar](max) NULL,
	[PA_URL] [varchar](max) NULL,
	[RESTRICTION_CODE] [varchar](max) NULL,
	[RESTRICTION_DETAIL_TEXT] [varchar](max) NULL,
	[PROD_NAME] [varchar](max) NULL
)
GO


