--USE [MCMM_Prod]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_GENENTECH_BRIDGE_FILE]    Script Date: 11/15/2018 7:19:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- EXEC usp_CF_GENENTECH_BRIDGE_FILE '7/12/2018', '8/13/2018'
ALTER PROCEDURE [dbo].[usp_CF_GENENTECH_BRIDGE_FILE]
	@StartDate as Date,
	@EndDate as Date
AS
BEGIN
	
	-- Create temp table for holding semi-results, ID will be needed only for transformations
	DECLARE @Results AS TABLE (
		ID INT IDENTITY(1,1) NOT NULL,
		[Survey Completed] VARCHAR(10),     -- date mm/dd/yyyy
		[MCOid] VARCHAR(255),               -- 
		[PayerName] VARCHAR(300),           -- from view [TA - completed surveys PLANS SQL]
		[PlanName] VARCHAR(300),            -- from view [TA - completed surveys PLANS SQL]
		[Segment] VARCHAR(255),             -- empty for now
		[Last P&T] VARCHAR(10),             -- mm/yyyy
		[Next P&T] VARCHAR(10),             -- mm/yyyy
		[Therapeutic Area] VARCHAR(128),    -- from view [TA - completed surveys PLANS SQL]
		[DATA_EXTRACTION_DATE] VARCHAR(10)  -- date mm/dd/yyyy
	)

	-- Fill results from a view:
	-- 1. Filter by start date and end dates. Filter by therap areas
	-- 2. Last P&T, Next P&T: format as dash if NULL, otherwise format as MM/YYYY
	-- 3. Order data
	INSERT INTO @Results
		SELECT
			[Survey Completed] = CONVERT(char(10), CompleteDate, 101), -- date mm/dd/yyyy
			MCOid = ISNULL(cfgbpnrwm.MCOid, ''),
			PayerName,
			PlanName = ISNULL(cfgbpnrwm.PlanNameReplacement, vw.PlanName),
			Segment = dbo.udf_CF_GetSegmentFromPlanName(ISNULL(cfgbpnrwm.PlanNameReplacement, vw.PlanName)),
			[Last P&T] =	CASE -- MM/YYYY or dash
								WHEN (RecentMeetingMonth IS NULL) OR (RecentMeetingYear IS NULL) THEN
									'�'
								ELSE
									RIGHT('00' + RecentMeetingMonth, 2) + '/' + RecentMeetingYear
							END,
			[Next P&T] =	CASE -- MM/YYYY or dash
								WHEN (NextMeetingMonth IS NULL) OR (NextMeetingYear IS NULL) THEN
									'�'
								ELSE
									RIGHT('00' + NextMeetingMonth, 2) + '/' + NextMeetingYear
							END,
			[Therapeutic Area] = ISNULL(cfgbtar.TherapAreaReplacement, vw.TherapArea),
			DATA_EXTRACTION_DATE = CONVERT(char(10), GetDate(), 101) -- date mm/dd/yyyy
		FROM [dbo].[TA - completed surveys PLANS SQL] vw
		LEFT JOIN CF_GenentechBridgeTherapAreaRenaming cfgbtar ON cfgbtar.TherapAreaID = vw.TherapAreaID
		LEFT JOIN CF_GenentechBridgePlanNameRenamingWithMcoid cfgbpnrwm ON cfgbpnrwm.PlanName = vw.PlanName
		WHERE (vw.CompleteDate >= @StartDate) AND (vw.CompleteDate <= @EndDate)
		AND vw.TherapAreaID IN (1390, 1392, 1407, 14, 1521, 41, 45)
		AND vw.PlanName NOT IN (SELECT PlanName FROM CF_GenentechBridgePlanNamesExcluded)
		ORDER BY [Therapeutic Area] ASC, PayerName ASC, PlanName ASC, vw.CompleteDate DESC


	-- Now, if some of the rows have the same: PayerName, PlanName, [Therapeutic Area]
	-- then leave only the 1st row from that group
	DELETE FROM @Results
	WHERE ID NOT IN 
	(
		SELECT MIN(ID) FROM @Results
		GROUP BY PayerName, PlanName, [Therapeutic Area]
	)

	-- We should not show the exact survey date. Instead, we display the first of the month in which the information was captured
	UPDATE @Results
		SET [Survey Completed] = CONVERT(char(10), DATEADD(m, DATEDIFF(m, 0, [Survey Completed]), 0) , 101)

	-- Put a dash for [Last P&T] older than two years
	UPDATE @Results
		SET [Last P&T] =	CASE
								WHEN [Last P&T] = '�' THEN
									'�'
								ELSE
									CASE 
										WHEN CONVERT(date, '01/' + [Last P&T], 103) < DATEADD(YEAR, -2, GetDate()) THEN
											'�'
										ELSE
											[Last P&T]
									END
							END

	
	-- Select the result, without ID column
	-- Output column renaming, as requested by users
	SELECT
		[SURVEY_COMPLETED] = [Survey Completed],
		[MCOID] = [MCOid],
		[PAYER_NAME] = [PayerName],
		[PLAN_NAME] = [PlanName],
		[SEGMENT] = [Segment],
		[LAST_PT] = [Last P&T],
		[NEXT_PT] = [Next P&T],
		[THERAPEUTIC_AREA] = [Therapeutic Area],
		[DATA_EXTRACTION_DATE]
	FROM @Results

END

GO


