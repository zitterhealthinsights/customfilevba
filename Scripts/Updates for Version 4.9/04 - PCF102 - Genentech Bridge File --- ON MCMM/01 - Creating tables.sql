--USE [MCMM_Prod]
--GO

CREATE TABLE CF_GenentechBridgeTherapAreaRenaming (
	[TherapAreaID] INT NULL,
	[TherapArea] VARCHAR(128) NULL,
	[TherapAreaReplacement] VARCHAR(128) NULL
)	
INSERT INTO CF_GenentechBridgeTherapAreaRenaming(TherapAreaID, TherapArea, TherapAreaReplacement) VALUES (1390, 'Breast cancer','BC')
INSERT INTO CF_GenentechBridgeTherapAreaRenaming(TherapAreaID, TherapArea, TherapAreaReplacement) VALUES (1392, 'Chronic lymphocytic leukemia','CLL')
INSERT INTO CF_GenentechBridgeTherapAreaRenaming(TherapAreaID, TherapArea, TherapAreaReplacement) VALUES (41, 'Hemophilia','Hemophilia')
INSERT INTO CF_GenentechBridgeTherapAreaRenaming(TherapAreaID, TherapArea, TherapAreaReplacement) VALUES (45, 'Multiple sclerosis','MS')
INSERT INTO CF_GenentechBridgeTherapAreaRenaming(TherapAreaID, TherapArea, TherapAreaReplacement) VALUES (1407, 'Non-small cell lung cancer','NSCLC')
INSERT INTO CF_GenentechBridgeTherapAreaRenaming(TherapAreaID, TherapArea, TherapAreaReplacement) VALUES (14, 'Rheumatoid arthritis','RA')
INSERT INTO CF_GenentechBridgeTherapAreaRenaming(TherapAreaID, TherapArea, TherapAreaReplacement) VALUES (1521, 'Severe asthma (eosinophilic / allergic)','Asthma')






CREATE TABLE CF_GenentechBridgePlanNamesExcluded (
	[PlanName] VARCHAR(300) NULL
)
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SUTTER HEALTH PLUS [COMMERCIAL]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SUTTERSELECT [COMMERCIAL]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('CARDINAL HEALTH')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SUTTER HEALTH PLUS [COMMERCIAL]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SUTTER HEALTH PLUS [MEDICAID]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SUTTER HEALTH PLUS [MEDICARE]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SUTTER SENIOR CARE [COMMERCIAL]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SUTTER SENIOR CARE [MEDICAID]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SUTTER SENIOR CARE [MEDICARE]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SUTTERSELECT [COMMERCIAL]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SUTTERSELECT [MEDICARE]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('BCBS LOUISIANA [MEDICAID]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('BCBS LOUISIANA [MEDICARE]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SHARP HEALTH PLAN [COMMERCIAL]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('SHARP HEALTH PLAN [MEDICARE]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('HARVARD PILGRIM HEALTH CARE [MEDICAID]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('HARVARD PILGRIM HEALTH CARE [MEDICARE]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('INDEPENDENT HEALTH ASSOCIATION [MEDICAID]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('MAGELLAN RX MANAGEMENT [COMMERCIAL]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('MAGELLAN RX MANAGEMENT [MEDICARE]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('NAVITUS [MEDICARE]')
INSERT INTO CF_GenentechBridgePlanNamesExcluded(PlanName) VALUES('WELLCARE - NATIONAL [COMMERCIAL]')






CREATE TABLE CF_GenentechBridgePlanNameRenamingWithMcoid (
	[PlanName] VARCHAR(300) NULL,
	[PlanNameReplacement] VARCHAR(300) NULL,
	[MCOid] VARCHAR(255) NULL
)

INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('PRIME THERAPEUTICS [COMMERCIAL]','PRIME THERAPEUTICS [PBM-COMMERCIAL]','900080000')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('PRIME THERAPEUTICS [MEDICAID]','PRIME THERAPEUTICS [PBM-MEDICAID]','900082005')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('PRIME THERAPEUTICS [MEDICARE]','PRIME THERAPEUTICS [PBM-MEDICARE]','900081005')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('ANTHEM - NATIONAL [MEDICAID]','ANTHEM HEALTHKEEPERS, INC [MEDICAID]','630001101')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('FIRSTCARE [MEDICAID]','FIRSTCARE ADVANTAGE [MEDICAID]','214000001')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('FIRSTCARE [MEDICARE]','FIRSTCARE [COMMERCIAL]','214000000')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('MAGELLAN RX MANAGEMENT [MEDICAID]','MAGELLAN HEALTH [PBM-MEDICAID]','900252005')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('MEDIMPACT [MEDICARE]','MEDIMPACT [PBM-MEDICARE]','900261005')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('EXPRESS SCRIPTS, INC [COMMERCIAL]','EXPRESS SCRIPTS, INC - NATIONAL PREFERRED FORMULARY [PBM-COMMERCIAL]','900270010')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('MEDICAL MUTUAL OF OHIO NATIONAL [COMMERCIAL]','MEDICAL MUTUAL OF OHIO [COMMERCIAL]','341001000')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('NETWORK HEALTH (WI) [COMMERCIAL]','NETWORK HEALTH PLAN (WI) [COMMERCIAL]','647000000')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('NETWORK HEALTH (WI) [MEDICARE]','NETWORK HEALTH PLAN (WI) [MEDICARE]','647000002')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('CIGNA - NATIONAL (STATES) [MEDICARE]','CIGNA - NATIONAL [MEDICARE]','187001002')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('MEDIMPACT [COMMERCIAL]','MEDIMPACT - PORTFOLIO HIGH [PBM-COMMERCIAL]','900743000')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('MEDIMPACT [MEDICAID]','MEDIMPACT [PBM-MEDICAID]','900262005')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('NAVITUS [COMMERCIAL]','NAVITUS - CUSTOM [PBM - COMMERCIAL]','900807005')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('NAVITUS [MEDICAID]','NAVITUS [PBM-MEDICAID]','900062005')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('SANFORD HEALTH PLAN (FORMERLY SIOUX VALLEY) [COMMERCIAL]','SANFORD HEALTH (FORMERLY SIOUX VALLEY) [COMMERCIAL]','447000000')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('SANFORD HEALTH PLAN (FORMERLY SIOUX VALLEY) [MEDICAID]','SANFORD HEALTH (FORMERLY SIOUX VALLEY) [MEDICAID]','447000001')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('SELECTHEALTH (FORMERLY INTERMOUNTAIN HEALTH CARE) [COMMERCIAL]','SELECTHEALTH [COMMERCIAL]','307000000')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('SELECTHEALTH (FORMERLY INTERMOUNTAIN HEALTH CARE) [MEDICAID]','SELECTHEALTH UTAH [MEDICAID]','307000001')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('SELECTHEALTH (FORMERLY INTERMOUNTAIN HEALTH CARE) [MEDICARE]','SELECTHEALTH [MEDICARE]','307000002')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('CIGNA - PENNSYLVANIA [COMMERCIAL]','CIGNA - NATIONAL [COMMERCIAL]','187001000')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('CVS CAREMARK CORPORATION [COMMERCIAL]','CAREMARK - PERFORMANCE DRUG LIST [PBM - COMMERCIAL]','630014105')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('KAISER - NATIONAL - [COMMERCIAL]','KAISER - NORTHERN CALIFORNIA [COMMERCIAL]','313003000')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('KAISER - NATIONAL - [MEDICAID]','KAISER - CALIFORNIA [MEDICAID]','313003001')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('KAISER - NATIONAL - [MEDICARE]','KAISER - NATIONAL - [MEDICARE]','313000002')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('WELLCARE - NATIONAL [MEDICAID]','WELLCARE - GEORGIA [MEDICAID]','627003111')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('OPTUMRX [COMMERCIAL]','OPTUMRX - SELECT [PBM-COMMERCIAL]','900150000')
INSERT INTO CF_GenentechBridgePlanNameRenamingWithMcoid(PlanName, PlanNameReplacement, MCOid) VALUES('OPTUMRX [MEDICAID]','OPTUMRX [PBM-MEDICAID]','900152005')