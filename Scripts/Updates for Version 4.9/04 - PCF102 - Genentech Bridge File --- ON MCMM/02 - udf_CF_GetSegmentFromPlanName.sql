--USE [MCMM_Prod]
--GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
OPTUMRX - CUSTOMIZED [PBM-COMMERCIAL] -> PBM Commercial
OPTUMRX [PBM-MEDICARE]                -> PBM Medicare
OPTUMRX [PBM-MEDICAID]                -> PBM Medicaid
HEALTHPARTNERS (MN) [COMMERCIAL]      -> Commercial
HEALTHPARTNERS (MN) [MEDICARE]		  -> Medicare
HEALTHPARTNERS (MN) [MEDICAID]		  -> Medicaid

EXPRESS SCRIPTS, INC - NATIONAL FORMULARY [PBM - COMMERCIAL], it can have spaces inside []


SELECT dbo.udf_CF_GetSegmentFromPlanName('') AS 'Result'
SELECT dbo.udf_CF_GetSegmentFromPlanName('abc') AS 'Result'
SELECT dbo.udf_CF_GetSegmentFromPlanName('abc[123]') AS 'Result'
SELECT dbo.udf_CF_GetSegmentFromPlanName('abc[PBM-COMMERCIAL]') AS 'Result'
SELECT dbo.udf_CF_GetSegmentFromPlanName('abc[PBM - COMMERCIAL]') AS 'Result'
SELECT dbo.udf_CF_GetSegmentFromPlanName('abc[PBM-MEDICARE]') AS 'Result'
SELECT dbo.udf_CF_GetSegmentFromPlanName('abc[PBM - MEDICAID]') AS 'Result'
SELECT dbo.udf_CF_GetSegmentFromPlanName('HEALTHPARTNERS (MN) [COMMERCIAL]') AS 'Result'
SELECT dbo.udf_CF_GetSegmentFromPlanName('HEALTHPARTNERS (MN) [MEDICARE]') AS 'Result'
SELECT dbo.udf_CF_GetSegmentFromPlanName('HEALTHPARTNERS (MN) [MEDICAID]') AS 'Result'
*/
CREATE FUNCTION [dbo].[udf_CF_GetSegmentFromPlanName]
(
    @PlanName NVARCHAR(255)
)
RETURNS NVARCHAR(100)
AS
BEGIN

	DECLARE @StrOutput NVARCHAR(MAX)
	DECLARE @PosOpenBracket AS INT
	DECLARE @PosClosedBracket AS INT

	SET @StrOutput = ''
	
	SET @PosOpenBracket = CHARINDEX('[', @PlanName)
	SET @PosClosedBracket = CHARINDEX(']', @PlanName)
		
	IF (@PosOpenBracket > 0) AND (@PosClosedBracket > 0) AND (@PosClosedBracket > @PosOpenBracket)
		BEGIN
			SET @StrOutput = SUBSTRING(@PlanName, @PosOpenBracket + 1, @PosClosedBracket - @PosOpenBracket - 1)
			SET @StrOutput = REPLACE(@StrOutput, ' ', '')

			SET @StrOutput = CASE
								WHEN @StrOutput = 'PBM-COMMERCIAL' THEN 'PBM Commercial'
								WHEN @StrOutput = 'PBM-MEDICARE' THEN 'PBM Medicare'
								WHEN @StrOutput = 'PBM-MEDICAID' THEN 'PBM Medicaid'
								WHEN @StrOutput = 'COMMERCIAL' THEN 'Commercial'
								WHEN @StrOutput = 'MEDICARE' THEN 'Medicare'
								WHEN @StrOutput = 'MEDICAID' THEN 'Medicaid'
								ELSE ''
								END
		END
	ELSE
		BEGIN
			SET @StrOutput = ''
		END

	RETURN @StrOutput

END


