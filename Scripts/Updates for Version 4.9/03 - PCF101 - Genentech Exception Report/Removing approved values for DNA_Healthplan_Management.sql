--USE [CWP_3.0]
--GO

SELECT * FROM CF_FieldName
SELECT * FROM CF_ApprovedFieldValues
WHERE FieldNameID = 1
/*The following values are not valid for the DNA_Healthplan_Management column. Please adjust the exception report file to include any instances of these.
Highly Managed
Less Managed
Less Managed than PI
Managed to PI
Moderately Managed*/

SELECT * FROM CF_ApprovedFieldValues
WHERE FieldNameID = 1
AND FieldValue IN ('Highly Managed','Less Managed','Less Managed than PI','Managed to PI','Moderately Managed')

--DELETE CF_ApprovedFieldValues
--WHERE FieldNameID = 1
--AND FieldValue IN ('Highly Managed','Less Managed','Less Managed than PI','Managed to PI','Moderately Managed')