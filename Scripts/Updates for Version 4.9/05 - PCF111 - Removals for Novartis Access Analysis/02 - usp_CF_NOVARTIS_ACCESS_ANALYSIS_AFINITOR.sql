--USE [CWP_VBA_DEV]
--GO
/****** Object:  StoredProcedure [dbo].[usp_CF_NOVARTIS_ACCESS_ANALYSIS_AFINITOR]    Script Date: 1/8/2019 12:34:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_AFINITOR @Userid = 2635, @IndOrDrug = ''
ALTER PROCEDURE [dbo].[usp_CF_NOVARTIS_ACCESS_ANALYSIS_AFINITOR]
	@Userid INT,
	@IndOrDrug VARCHAR(50)
AS
BEGIN

DECLARE @selectedFields AS VARCHAR(MAX)

DECLARE @BC_Raw AS CF_NOVARTIS_FLAT_BC
DECLARE @BC_Filtered AS CF_NOVARTIS_FLAT_BC
DECLARE @GI_NET_Raw AS CF_NOVARTIS_FLAT_GI_NET
DECLARE @GI_NET_Filtered AS CF_NOVARTIS_FLAT_GI_NET
DECLARE @Lung_NET_Raw AS CF_NOVARTIS_FLAT_Lung_NET
DECLARE @Lung_NET_Filtered AS CF_NOVARTIS_FLAT_Lung_NET
DECLARE @P_NET_Raw AS CF_NOVARTIS_FLAT_P_NET
DECLARE @P_NET_Filtered AS CF_NOVARTIS_FLAT_P_NET
DECLARE @RCC_Raw AS CF_NOVARTIS_FLAT_RCC
DECLARE @RCC_Filtered AS CF_NOVARTIS_FLAT_RCC
DECLARE @TSC_Raw AS CF_NOVARTIS_FLAT_TSC
DECLARE @TSC_Filtered AS CF_NOVARTIS_FLAT_TSC

DECLARE @OutputUnion AS CF_NOVARTIS_ACCESS_ANALYSIS_OUTPUT


	------------------------ BC get FLAT
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_FLAT_BC','');
	INSERT INTO @BC_Raw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = 'BC',
			@Userid = @Userid,
			@isPBMOnly = 0, -- Flat
			@SelectFields = @selectedFields

	------------------------ BC filter by Drug Name
	INSERT INTO @BC_Filtered
		SELECT * FROM @BC_Raw WHERE Drug_Name = 'Afinitor'


	------------------------ BC append to output
	INSERT INTO @OutputUnion
		SELECT
			[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
			[PayerLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Plan_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PlanLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanFullyInsuredLIves] = LTRIM(RTRIM(REPLACE(REPLACE([PlanFullyInsuredLIves],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[Novartis_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Novartis_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') ))
		FROM @BC_Filtered



	-- 2019-01-08: PCF111, removal of all Afinitor indications except: BC,RCC,TSC
	/*------------------------ GI_NET get FLAT
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_FLAT_GI_NET','');
	INSERT INTO @GI_NET_Raw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = 'GI_NET',
			@Userid = @Userid,
			@isPBMOnly = 0, -- Flat
			@SelectFields = @selectedFields

	------------------------ GI_NET filter by Drug Name
	INSERT INTO @GI_NET_Filtered
		SELECT * FROM @GI_NET_Raw WHERE Drug_Name = 'Afinitor'


	------------------------ GI_NET append to output
	INSERT INTO @OutputUnion
		SELECT
			[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
			[PayerLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Plan_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PlanLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanFullyInsuredLIves] = LTRIM(RTRIM(REPLACE(REPLACE([PlanFullyInsuredLIves],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[Novartis_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Novartis_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') ))
		FROM @GI_NET_Filtered





	------------------------ Lung_NET get FLAT
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_FLAT_Lung_NET','');
	INSERT INTO @Lung_NET_Raw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = 'Lung_NET',
			@Userid = @Userid,
			@isPBMOnly = 0, -- Flat
			@SelectFields = @selectedFields

	------------------------ Lung_NET filter by Drug Name
	INSERT INTO @Lung_NET_Filtered
		SELECT * FROM @Lung_NET_Raw WHERE Drug_Name = 'Afinitor'


	------------------------ Lung_NET append to output
	INSERT INTO @OutputUnion
		SELECT
			[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
			[PayerLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Plan_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PlanLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanFullyInsuredLIves] = LTRIM(RTRIM(REPLACE(REPLACE([PlanFullyInsuredLIves],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[Novartis_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Novartis_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') ))
		FROM @Lung_NET_Filtered





	------------------------ P_NET get FLAT
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_FLAT_P_NET','');
	INSERT INTO @P_NET_Raw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = 'P_NET',
			@Userid = @Userid,
			@isPBMOnly = 0, -- Flat
			@SelectFields = @selectedFields

	------------------------ P_NET filter by Drug Name
	INSERT INTO @P_NET_Filtered
		SELECT * FROM @P_NET_Raw WHERE Drug_Name = 'Afinitor'


	------------------------ P_NET append to output
	INSERT INTO @OutputUnion
		SELECT
			[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
			[PayerLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Plan_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PlanLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanFullyInsuredLIves] = LTRIM(RTRIM(REPLACE(REPLACE([PlanFullyInsuredLIves],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[Novartis_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Novartis_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') ))
		FROM @P_NET_Filtered
		*/



	------------------------ RCC get FLAT
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_FLAT_RCC','');
	INSERT INTO @RCC_Raw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = 'RCC',
			@Userid = @Userid,
			@isPBMOnly = 0, -- Flat
			@SelectFields = @selectedFields

	------------------------ RCC filter by Drug Name
	INSERT INTO @RCC_Filtered
		SELECT * FROM @RCC_Raw WHERE Drug_Name = 'Afinitor'


	------------------------ RCC append to output
	INSERT INTO @OutputUnion
		SELECT
			[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
			[PayerLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Plan_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PlanLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanFullyInsuredLIves] = LTRIM(RTRIM(REPLACE(REPLACE([PlanFullyInsuredLIves],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[Novartis_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Novartis_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') ))
		FROM @RCC_Filtered





	------------------------ TSC get FLAT
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_FLAT_TSC','');
	INSERT INTO @TSC_Raw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = 'TSC',
			@Userid = @Userid,
			@isPBMOnly = 0, -- Flat
			@SelectFields = @selectedFields

	------------------------ TSC filter by Drug Name
	INSERT INTO @TSC_Filtered
		SELECT * FROM @TSC_Raw WHERE Drug_Name = 'Afinitor'


	------------------------ TSC append to output
	INSERT INTO @OutputUnion
		SELECT
			[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
			[PayerLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PayerSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PayerSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentFullyInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentFullyInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSegmentSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSegmentSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Plan_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PlanLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanMedicalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanMedicalLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanPharmacyLives],CHAR(13),' '),CHAR(10),' ') )),
			[PlanFullyInsuredLIves] = LTRIM(RTRIM(REPLACE(REPLACE([PlanFullyInsuredLIves],CHAR(13),' '),CHAR(10),' ') )),
			[PlanSelfInsuredLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanSelfInsuredLives],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[Novartis_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Novartis_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') ))
		FROM @TSC_Filtered




	SELECT * FROM @OutputUnion
	ORDER BY [Indication], [Mcoid]

END
--USE [CWP_3.0]
--GO

SET ANSI_NULLS ON
