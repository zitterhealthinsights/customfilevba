--USE [PATT]
--GO

SELECT * FROM Process
WHERE ProcessID = 17

UPDATE Process
	SET ProcessName = 'Regeneron-Sanofi all indications monthly files'
WHERE ProcessID = 17

SELECT * FROM Process
WHERE ProcessID = 17