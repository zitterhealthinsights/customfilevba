USE [PATT]
GO

/****** Object:  UserDefinedTableType [dbo].[CF_REGENERON_PBM_RA]    Script Date: 2/5/2019 10:26:16 PM ******/
CREATE TYPE [dbo].[CF_REGENERON_PBM_RA] AS TABLE(
	[Mcoid] [varchar](max) NULL,
	[PayerName] [varchar](max) NULL,
	[PayerLives] [varchar](max) NULL,
	[PayerMedicalLives] [varchar](max) NULL,
	[PayerPharmacyLives] [varchar](max) NULL,
	[PayerFullyInsuredLives] [varchar](max) NULL,
	[PayerSelfInsuredLives] [varchar](max) NULL,
	[PBM_Total_Lives] [varchar](max) NULL,
	[PBM_Controlled_Lives] [varchar](max) NULL,
	[PBM_Claims_Lives] [varchar](max) NULL,
	[PBM_Self_Insured_Lives] [varchar](max) NULL,
	[Segment] [varchar](max) NULL,
	[PlanSegmentLives] [varchar](max) NULL,
	[PlanSegmentMedicalLives] [varchar](max) NULL,
	[PlanSegmentPharmacyLives] [varchar](max) NULL,
	[PlanSegmentFullyInsuredLives] [varchar](max) NULL,
	[PlanSegmentSelfInsuredLives] [varchar](max) NULL,
	[Plan_Name] [varchar](max) NULL,
	[PlanLives] [varchar](max) NULL,
	[PlanMedicalLives] [varchar](max) NULL,
	[PlanPharmacyLives] [varchar](max) NULL,
	[PlanFullyInsuredLIves] [varchar](max) NULL,
	[PlanSelfInsuredLives] [varchar](max) NULL,
	[TZG Payer Name] [varchar](max) NULL,
	[SegmentParentMcoID] [varchar](max) NULL,
	[Indication] [varchar](max) NULL,
	[Drug_Name] [varchar](max) NULL,
	[On_Formulary] [varchar](max) NULL,
	[Preferred_Status] [varchar](max) NULL,
	[Preferred_implementation] [varchar](max) NULL,
	[Preferred_Status_by_Admin_Method] [varchar](max) NULL,
	[Preferred_Implementation_by_Admin_Method] [varchar](max) NULL,
	[Healthplan_Management] [varchar](max) NULL,
	[Benefit_Type] [varchar](max) NULL,
	[PA_Required] [varchar](max) NULL,
	[URL_to_PA_Policy] [varchar](max) NULL,
	[PACriteria_DocPath] [varchar](max) NULL,
	[URL_to_PA_Form] [varchar](max) NULL,
	[URL_To_Draft_Policy] [varchar](max) NULL,
	[PAForm_DocPath] [varchar](max) NULL,
	[PA_Management] [varchar](max) NULL,
	[Tier_Placement] [varchar](max) NULL,
	[Number_of_Tiers] [varchar](max) NULL,
	[Dosing_Limitation] [varchar](max) NULL,
	[Dosing_Limitation_Desc] [varchar](max) NULL,
	[Quantity_Limitation] [varchar](max) NULL,
	[Quantity_Limitation_Desc] [varchar](max) NULL,
	[Initial_Auth_Time_Limit_Req] [varchar](max) NULL,
	[Initial_Auth_Time_Length] [varchar](max) NULL,
	[Recert_Time_Limit_Req] [varchar](max) NULL,
	[Recert_Time_Length] [varchar](max) NULL,
	[Step_Therapy_Req] [varchar](max) NULL,
	[Step_Therapy_Placement] [varchar](max) NULL,
	[Number_of_Steps] [varchar](max) NULL,
	[Step_Therapy_Notes] [varchar](max) NULL,
	[Specific_PA_Criteria_Details] [varchar](max) NULL,
	[BioInject_Trial_Ct] [varchar](max) NULL,
	[BioInject_Len] [varchar](max) NULL,
	[BioInject_Opr] [varchar](max) NULL,
	[Specific_Biologic_Failure] [varchar](max) NULL,
	[Administrative_Requirements] [varchar](max) NULL,
	[Separate_Pharmacy_and_Medical_Policy] [varchar](max) NULL,
	[Separate_Pharmacy_and_Medical_Policy_Criteria] [varchar](max) NULL,
	[Proof_of_Effectiveness_Required_Summary] [varchar](max) NULL,
	[ProofOfEfficacy] [varchar](max) NULL,
	[Q_Codes_Approved] [varchar](max) NULL,
	[J_Codes_Approved] [varchar](max) NULL,
	[ICD_Codes_Approved] [varchar](max) NULL,
	[Split_Fill_Requirements] [varchar](max) NULL,
	[Specialist_Appr] [varchar](max) NULL,
	[PBM_Indicated] [varchar](max) NULL,
	[PBM_Control] [varchar](max) NULL,
	[Distribution_Limitations_Enforced] [varchar](max) NULL,
	[Name_Of_Specialty_Drug_Distributer_1] [varchar](max) NULL,
	[Name_Of_Specialty_Drug_Distributer_2] [varchar](max) NULL,
	[Other_Policy_Utilized] [varchar](max) NULL,
	[Documentation_Source] [varchar](max) NULL,
	[Change_to_Entry] [varchar](max) NULL,
	[Reason_For_Change] [varchar](max) NULL,
	[Reason_for_Change_Details] [varchar](max) NULL,
	[Policy_Date] [varchar](max) NULL,
	[Renewal_Date] [varchar](max) NULL,
	[No_Policy_Date_Found] [varchar](max) NULL,
	[Entry_Date] [varchar](max) NULL,
	[Note_1] [varchar](max) NULL,
	[Note_2] [varchar](max) NULL,
	[Note_3] [varchar](max) NULL,
	[General_PA_Form] [varchar](max) NULL,
	[extramco] [varchar](max) NULL
)
GO


