USE [PATT]
GO
/****** Object:  StoredProcedure [dbo].[usp_CF_REGENERON_PBM_HCHL]    Script Date: 2/5/2019 10:27:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[usp_CF_REGENERON_PBM_HCHL]
	@Userid INT,
	@IndOrDrug VARCHAR(50)
AS
BEGIN

		DECLARE @Get_PBMHCHL as CF_REGENERON_PBM_HCHL
		DECLARE @selectedFields as varchar(max)
		SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_REGENERON_PBM_HCHL','');
		Insert into @Get_PBMHCHL
		EXEC	dbo.usp_Get_Plans
			@UserID = @Userid, 
			@IndicationorDrug = @IndOrDrug,
			@SelectFields = @selectedFields,
			@IsPBMOnly = 1
				
		SELECT
			[Mcoid] = LTRIM(RTRIM(REPLACE(REPLACE([Mcoid],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_TotalLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Total_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_ControlledFullyInsuredPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Controlled_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_ClaimsLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Claims_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_SelfInsuredPharmacyLives] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Self_Insured_Lives],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[Regeneron_Sanofi_Healthplan_Management] = LTRIM(RTRIM(REPLACE(REPLACE([Regeneron_Sanofi_Healthplan_Management],CHAR(13),' '),CHAR(10),' ') )),
			[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[URL_To_Draft_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_To_Draft_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[ePA_URL_Available] = LTRIM(RTRIM(REPLACE(REPLACE([ePA_URL_Available],CHAR(13),' '),CHAR(10),' ') )),
			[ePA_URL] = LTRIM(RTRIM(REPLACE(REPLACE([ePA_URL],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Management] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Management],CHAR(13),' '),CHAR(10),' ') )),
			--[General_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([General_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
			[Age_Limit] = LTRIM(RTRIM(REPLACE(REPLACE([Age_Limit],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Limit_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Limit_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Steps] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Notes] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_Drug_Failure_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Drug_Failure_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Regimen_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Regimen_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Regimen_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Regimen_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Mandates_on_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
			[Mandates_on_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Mandates_on_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_Combination_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Combination_Therapy],CHAR(13),' '),CHAR(10),' ') )),
			[Prohibits_Specified_Concomitant_Therapies] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies],CHAR(13),' '),CHAR(10),' ') )),
			[Prohibits_Specified_Concomitant_Therapies_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Prohibits_Specified_Concomitant_Therapies_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_PA_Criteria_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_Details],CHAR(13),' '),CHAR(10),' ') )),
			[Time_Frame_Since_Last_Statin_Trial_Requirement] = LTRIM(RTRIM(REPLACE(REPLACE([Time_Frame_Since_Last_Statin_Trial_Requirement],CHAR(13),' '),CHAR(10),' ') )),
			[Time_Frame_Since_Last_Statin_Trial_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Time_Frame_Since_Last_Statin_Trial_Description],CHAR(13),' '),CHAR(10),' ') )),
			--[Time_Frame_Since_Last_Other_Lipid_Lowering_Agent_Trial_Requirement] = LTRIM(RTRIM(REPLACE(REPLACE([Time_Frame_Since_Last_Other_Lipid_Lowering_Agent_Trial_Requirement],CHAR(13),' '),CHAR(10),' ') )),
			--[Time_Frame_Since_Other_Lipid_Lowering_Agent_Trial_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Time_Frame_Since_Other_Lipid_Lowering_Agent_Trial_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Statin_Intolerance_Definition] = LTRIM(RTRIM(REPLACE(REPLACE([Statin_Intolerance],CHAR(13),' '),CHAR(10),' ') )),
			[Statin_Trial_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Statin_Trial_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Statin_Trial_Required_Duration] = LTRIM(RTRIM(REPLACE(REPLACE([Statin_Trial_Required_Duration],CHAR(13),' '),CHAR(10),' ') )),
			[Statin_Trial_Requirements_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Statin_Trial_Requirements_Details],CHAR(13),' '),CHAR(10),' ') )),
			[Statin_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Statin_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Lipid_Lowering_Therapy_Trial_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Lipid_Lowering_Therapy_Trial_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Lipid_Lowering_Therapy_Trial_Duration] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Lipid_Lowering_Therapy_Trial_Duration],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Lipid_Lowering_Therapy_Trial_Requirements_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Lipid_Lowering_Therapy_Trial_Requirements_Details],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Lipid_Lowering_Therapy_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Lipid_Lowering_Therapy_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Prior_PCSK9_Trial_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Prior_PCSK9_Trial_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Prior_PCSK9_Trial_Required_Duration] = LTRIM(RTRIM(REPLACE(REPLACE([Prior_PCSK9_Trial_Required_Duration],CHAR(13),' '),CHAR(10),' ') )),
			[Prior_PCSK9_Trial_Requirements_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Prior_PCSK9_Trial_Requirements_Details],CHAR(13),' '),CHAR(10),' ') )),
			[Prior_PCSK9_Trial_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Prior_PCSK9_Trial_Opr],CHAR(13),' '),CHAR(10),' ') )),
			--[Combination_Lipid_Lowering_Therapy_Trial_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Combination_Lipid_Lowering_Therapy_Trial_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			--[Combination_Lipid_Lowering_Therapy_Trial_Duration] = LTRIM(RTRIM(REPLACE(REPLACE([Combination_Lipid_Lowering_Therapy_Trial_Duration],CHAR(13),' '),CHAR(10),' ') )),
			--[Combination_Lipid_Lowering_Therapy_Trial_Requirements_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Combination_Lipid_Lowering_Therapy_Trial_Requirements_Details],CHAR(13),' '),CHAR(10),' ') )),
			--[Combination_Lipid_Lowering_Therapy_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Combination_Lipid_Lowering_Therapy_Opr],CHAR(13),' '),CHAR(10),' ') )),
			--[Framingham_Risk_Factor_Score_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Framingham_Risk_Factor_Score_Required],CHAR(13),' '),CHAR(10),' ') )),
			--[Framingham_Risk_Factor_Score_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Framingham_Risk_Factor_Score_Description],CHAR(13),' '),CHAR(10),' ') )),
			--[ACC_AHA_Risk_Score_Calculator_Required] = LTRIM(RTRIM(REPLACE(REPLACE([ACC_AHA_Risk_Score_Calculator_Required],CHAR(13),' '),CHAR(10),' ') )),
			--[ACC_AHA_Risk_Score_Description] = LTRIM(RTRIM(REPLACE(REPLACE([ACC_AHA_Risk_Score_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Risk_Factor_Score_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Risk_Factor_Score_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Risk_Factor_Score_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Risk_Factor_Score_Description],CHAR(13),' '),CHAR(10),' ') )),
			--[Disease_Severity_Restrictions] = LTRIM(RTRIM(REPLACE(REPLACE([Disease_Severity_Restrictions],CHAR(13),' '),CHAR(10),' ') )),
			--[Disease_Severity_Restrictions_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Disease_Severity_Restrictions_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Lipid_Testing_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Lipid_Testing_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Lipid_Testing_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Lipid_Testing_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Covered_for_ASCVD_Population] = LTRIM(RTRIM(REPLACE(REPLACE([Covered_for_ASCVD_Population],CHAR(13),' '),CHAR(10),' ') )),
			[Invasive_or_Noninvasive_Testing_Required_for_ASCVD] = LTRIM(RTRIM(REPLACE(REPLACE([Invasive_or_Noninvasive_Testing_Required_for_ASCVD],CHAR(13),' '),CHAR(10),' ') )),
			[Invasive_or_Noninvasive_Testing_Required_for_ASCVD_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Invasive_or_Noninvasive_Testing_Required_for_ASCVD_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Genotyping_Required_for_ASCVD] = LTRIM(RTRIM(REPLACE(REPLACE([Genotyping_Required_for_ASCVD],CHAR(13),' '),CHAR(10),' ') )),
			[Required_Disease_State_for_ASCVD] = LTRIM(RTRIM(REPLACE(REPLACE([Required_Disease_State_for_ASCVD],CHAR(13),' '),CHAR(10),' ') )),
			[Required_Disease_State_for_ASCVD_Description] = LTRIM(RTRIM(REPLACE(REPLACE([Required_Disease_State_for_ASCVD_Description],CHAR(13),' '),CHAR(10),' ') )),
			[LDL_C_Levels_Requirement] = LTRIM(RTRIM(REPLACE(REPLACE([LDL_C_Levels_Requirement],CHAR(13),' '),CHAR(10),' ') )),
			[LDL_C_Level_Requirement_Description] = LTRIM(RTRIM(REPLACE(REPLACE([LDL_C_Level_Requirement_Description],CHAR(13),' '),CHAR(10),' ') )),
			[ASCVD_LDL_C_Level_Requirement_Description] = LTRIM(RTRIM(REPLACE(REPLACE([ASCVD_LDL_C_Level_Requirement_Description],CHAR(13),' '),CHAR(10),' ') )),
			[HEFH_LDL_C_Level_Requirement_Description] = LTRIM(RTRIM(REPLACE(REPLACE([HEFH_LDL_C_Level_Requirement_Description],CHAR(13),' '),CHAR(10),' ') )),
			[Specialty_Consultation_Required] = LTRIM(RTRIM(REPLACE(REPLACE([Specialty_Consultation_Required],CHAR(13),' '),CHAR(10),' ') )),
			[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy_Criteria] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_Criteria],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Required_for_Continued_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_for_Continued_Approval],CHAR(13),' '),CHAR(10),' ') )),
			[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[Specialist_Approval] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Approval],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
			[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
			[Name_Of_Specialty_Drug_Distributer_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
			[Name_Of_Specialty_Drug_Distributer_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Policy_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Policy_Utilized],CHAR(13),' '),CHAR(10),' ') )),
			[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
			[Change_to_Entry] = LTRIM(RTRIM(REPLACE(REPLACE([Change_to_Entry],CHAR(13),' '),CHAR(10),' ') )),
			[Reason_for_Change] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change],CHAR(13),' '),CHAR(10),' ') )),
			[Reason_for_Change_Details] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change_Details],CHAR(13),' '),CHAR(10),' ') )),
			[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(CONVERT(VARCHAR, CONVERT(DATE,[Policy_Date]), 101),CHAR(13),' '),CHAR(10),' ') )),
			[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(CONVERT(VARCHAR, CONVERT(DATE,[Renewal_Date]), 101),CHAR(13),' '),CHAR(10),' ') )),
			[No_Policy_Date_Found] = LTRIM(RTRIM(REPLACE(REPLACE([No_Policy_Date_Found],CHAR(13),' '),CHAR(10),' ') )),
			[Entry_Date] = LTRIM(RTRIM(REPLACE(REPLACE(CONVERT(VARCHAR, CONVERT(DATE,[Entry_Date]), 101),CHAR(13),' '),CHAR(10),' ') )),
			---[Original_Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(CONVERT(VARCHAR, CONVERT(DATE,[Original_Policy_Date]), 101),CHAR(13),' '),CHAR(10),' ') )),
			[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
			[Note_2] = LTRIM(RTRIM(REPLACE(REPLACE([Note_2],CHAR(13),' '),CHAR(10),' ') )),
			[Note_3] = LTRIM(RTRIM(REPLACE(REPLACE([Note_3],CHAR(13),' '),CHAR(10),' ') ))
		FROM @Get_PBMHCHL

END





