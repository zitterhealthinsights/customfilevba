--USE [MCMM_Prod]
--GO

/****** Object:  View [dbo].[TA - completed surveys PLANS SQL]    Script Date: 1/28/2019 11:01:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[TA - completed surveys PLANS SQL]
AS
SELECT DISTINCT 
                      dbo.Survey.CompleteDate, dbo.Survey.SurveyID, dbo.Survey.AdvisorID, 
                      CASE WHEN ComercialLiveTotalNumber > 0 THEN 'Commercial, ' ELSE '' END + CASE WHEN MedicareLiveTotalNumber > 0 THEN 'Medicare, ' ELSE '' END + CASE WHEN
                       MedicaidLiveTotalNumber > 0 THEN 'Medicaid ' ELSE '' END AS Responsibilities, dbo.Manufacturer.Manufacturer,
					  dbo.QPayerName.PayerNameID, dbo.QPayerName.PayerName, dbo.QPlanName.PlanNameID, dbo.QPlanName.PlanName,
					  [PlanNameActive] = dbo.QPlanName.Active,
                      dbo.QGeoAreaCovered.GeoAreaCovered, dbo.SurveyTherapAreaDiscussed.RecentMeetingMonth, 
                      dbo.SurveyTherapAreaDiscussed.RecentMeetingYear, dbo.SurveyTherapAreaDiscussed.NextMeetingMonth, dbo.SurveyTherapAreaDiscussed.NextMeetingYear, 
                      dbo.TherapArea.TherapArea, dbo.SurveyTherapAreaDiscussed.TherapAreaID
FROM         dbo.QHowDidMeetingOccur INNER JOIN
                      dbo.QGeoAreaCovered INNER JOIN
                      dbo.Manufacturer INNER JOIN
                      dbo.Survey ON dbo.Manufacturer.ManufacturerID = dbo.Survey.ManafacturerID INNER JOIN
                      dbo.SurveyTherapAreaDiscussed ON dbo.Survey.SurveyID = dbo.SurveyTherapAreaDiscussed.SurveyID INNER JOIN
                      dbo.TherapArea ON dbo.SurveyTherapAreaDiscussed.TherapAreaID = dbo.TherapArea.TherapAreaID LEFT OUTER JOIN
                      dbo.SurveyPlanName ON dbo.Survey.SurveyID = dbo.SurveyPlanName.SurveyID LEFT OUTER JOIN
                      dbo.QPlanName ON dbo.SurveyPlanName.PlanNameID = dbo.QPlanName.PlanNameID LEFT OUTER JOIN
                      dbo.QPayerName ON dbo.QPlanName.PayerNameID = dbo.QPayerName.PayerNameID ON 
                      dbo.QGeoAreaCovered.GeoAreaCoveredID = dbo.Survey.GeoAreaCoveredID ON 
                      dbo.QHowDidMeetingOccur.HowDidMeetingOccurID = dbo.Survey.HowDidMeetingOccurID INNER JOIN
                      dbo.QYourTitle ON dbo.Survey.YourTitleID = dbo.QYourTitle.YourTitleID

GO


