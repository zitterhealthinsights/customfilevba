 

select  DISTINCT 
		m.mcoid AS Mcoid,
		PLAN_NAME = m.MCO,
		SEGMENT = mct.[Name],

		cfgbm.[PATT MCOID],
		cfgbm.[PATT Name],
		MCMMPLANID = cfgbm.PlanNameID,
		cfgbm.[MCMM Name]

from [MCO].Production.MCO m
INNER JOIN CF_GenentechBridgeMapping cfgbm ON m.mcoid = cfgbm.[PATT MCOID]
INNER JOIN [MCO].dbo.mcotype mct ON mct.id = m.mcotypeid
WHERE m.Active = 1 AND mct.id <> 4 -- Exclude Health Exchanges
and  m.mcoid IN ( 900743000,  900262005, 900261005,293003001 , 293003002  )
	

select * 
FROM [dbo].[TA - completed surveys PLANS SQL] vw
WHERE (vw.CompleteDate >= '1/1/2019')
AND (vw.CompleteDate <= '2/7/2019')

--WHERE (vw.CompleteDate >= '1/1/2019') AND (vw.CompleteDate <= '2/7/2019')
--AND vw.TherapAreaID IN (1390, 1392, 1407, 14, 1521, 41, 45)
AND vw.PlanName NOT IN (SELECT PlanName FROM CF_GenentechBridgePlanNamesExcluded)
and PlanNameID in ( 1535,  1536, 1197, 1196, 1195 ) 

select * from TherapArea where theraparea.TherapAreaID in (1390, 1392, 1407, 14, 1521, 41, 45)
