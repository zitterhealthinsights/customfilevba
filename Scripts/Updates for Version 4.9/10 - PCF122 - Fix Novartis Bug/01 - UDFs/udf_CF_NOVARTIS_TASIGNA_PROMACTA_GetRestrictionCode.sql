/*
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode] ('','','')                                                  -- ''
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode] ('Yes','','')                                               -- 'PA'
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode] ('','Yes','')                                               -- 'QL'
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode] ('','QL Required No Criteria Exist','')                     -- 'QL'
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode] ('','','something different')                               -- 'ST'
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode] ('Yes','Yes','')                                            -- 'PA,QL'
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode] ('Yes','','something different')                            -- 'PA,ST'
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode] ('','Yes','something different')                            -- 'ST,QL'
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode] ('Yes','Yes','something different')                         -- 'PA,ST,QL'
*/

CREATE FUNCTION [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode]
(
    @PA_Column NVARCHAR(MAX),
	@QL_Column NVARCHAR(MAX),
	@ST_Column NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @blnPA AS BIT
	DECLARE @blnQL AS BIT
	DECLARE @blnST AS BIT

	DECLARE @StrOutput NVARCHAR(MAX)
	
	
	SET @StrOutput = ''
	
	SET @blnPA = CASE WHEN (LTRIM(RTRIM(@PA_Column)) = 'Yes') THEN 1 ELSE 0 END
	SET @blnQL = CASE WHEN (LTRIM(RTRIM(@QL_Column)) IN ('Yes','QL Required No Criteria Exist')) THEN 1 ELSE 0 END
	
	SET @blnST = CASE 
					WHEN (LTRIM(RTRIM(@ST_Column)) NOT IN ('PA Required No Criteria Exist', 'No', 
						'Data Not Available', 'Covered for All Medically Accepted Indications', 'Follows NCCN Guidelines',
						'B vs D', 'Follows FDA Guidelines', 'Not Covered', '', 'Drug Covered with No PA')) THEN 1 
					ELSE 0 
				 END

	IF @blnPA = 1
		BEGIN
			SET @StrOutput = 'PA'
		END

	IF @blnST = 1
		BEGIN
			IF LEN(@StrOutput) = 0
				BEGIN
					SET @StrOutput = 'ST'
				END
			ELSE
				BEGIN
					SET @StrOutput = @StrOutput + ', ST'
				END
		END

	IF @blnQL = 1
		BEGIN
			IF LEN(@StrOutput) = 0
				BEGIN
					SET @StrOutput = 'QL'
				END
			ELSE
				BEGIN
					SET @StrOutput = @StrOutput + ', QL'
				END
		END
		
	RETURN @StrOutput

END

GO


