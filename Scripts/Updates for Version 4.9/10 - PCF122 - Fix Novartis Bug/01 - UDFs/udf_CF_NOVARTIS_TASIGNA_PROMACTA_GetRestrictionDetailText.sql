/*
-- Empty values ignored
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionDetailText] ('Step Therapy Notes','','Limitation on Cancer Type','',
'Place in Therapy','','Lab Requirements','','Proof of Effectiveness Required Summary','','PH+_ Req','','Specialist Appr','')
-- ''

-- Empty values ignored after trim
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionDetailText] ('Step Therapy Notes',' ','Limitation on Cancer Type','  ',
'Place in Therapy','   ','Lab Requirements','','Proof of Effectiveness Required Summary','','PH+_ Req','','Specialist Appr','')
-- ''

-- Not Applicable ignored
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionDetailText] ('Step Therapy Notes','Not Applicable','Limitation on Cancer Type','Not Applicable',
'Place in Therapy','Not Applicable','Lab Requirements','Not Applicable','Proof of Effectiveness Required Summary','Not Applicable','PH+_ Req','Not Applicable',
'Specialist Appr','Not Applicable')
-- ''

-- Not Applicable ignored after trim
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionDetailText] ('Step Therapy Notes',' Not Applicable','Limitation on Cancer Type','Not Applicable ',
'Place in Therapy',' Not Applicable ','Lab Requirements','  Not Applicable  ','Proof of Effectiveness Required Summary','Not Applicable','PH+_ Req','Not Applicable',
'Specialist Appr','Not Applicable')
-- ''

-- Normal case: Values added to column names
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionDetailText] ('Age Restriction','1','Step Therapy Notes','2',
'Platelet Count Requirement','3','Splenectomy Required','4','Lab Requirements','5','Proof of Effectiveness Required Summary','6','Specialist Appr','7')
-- 'Age Restriction: 1|Step Therapy Notes: 2|Platelet Count Requirement: 3|Splenectomy Required: 4|Lab Requirements: 5|Proof of Effectiveness Required Summary: 6|Specialist Appr: 7'

-- Special values, from @NoColumnNameValues table
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionDetailText] ('Age Restriction','Drug Covered with No PA','Step Therapy Notes','B vs D',
'Platelet Count Requirement','3','Splenectomy Required','4','Lab Requirements','5','Proof of Effectiveness Required Summary','6','Specialist Appr','7')
-- Drug Covered with No PA|B vs D|Platelet Count Requirement: 3|Splenectomy Required: 4|Lab Requirements: 5|Proof of Effectiveness Required Summary: 6|Specialist Appr: 7

-- Special values, from @NoColumnNameValues table, repeated
SELECT [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionDetailText] ('Age Restriction','Drug Covered with No PA','Step Therapy Notes','Drug Covered with No PA',
'Platelet Count Requirement','Drug Covered with No PA','Splenectomy Required','Drug Covered with No PA','Lab Requirements','Drug Covered with No PA',
'Proof of Effectiveness Required Summary','Drug Covered with No PA','Specialist Appr','Drug Covered with No PA')
-- Drug Covered with No PA

*/
CREATE FUNCTION [dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionDetailText]
(
	-- Note: can not join them and make a loop. No good value for the separator, values can be anything
	@Col1Name NVARCHAR(255), @Col1Value NVARCHAR(MAX),
	@Col2Name NVARCHAR(255), @Col2Value NVARCHAR(MAX),
	@Col3Name NVARCHAR(255), @Col3Value NVARCHAR(MAX),
	@Col4Name NVARCHAR(255), @Col4Value NVARCHAR(MAX),
	@Col5Name NVARCHAR(255), @Col5Value NVARCHAR(MAX),
	@Col6Name NVARCHAR(255), @Col6Value NVARCHAR(MAX),
	@Col7Name NVARCHAR(255), @Col7Value NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @Results TABLE(ID INT IDENTITY(1,1) NOT NULL, [ColumnName] NVARCHAR(255), [ColumnValue] NVARCHAR(MAX))
	DECLARE @NewValue NVARCHAR(MAX)
	DECLARE @StrOutput NVARCHAR(MAX)

	-- Prepare @NoColumnNameValues table
	DECLARE @NoColumnNameValues TABLE(ID INT IDENTITY(1,1) NOT NULL, [Value] NVARCHAR(MAX))
	INSERT INTO @NoColumnNameValues VALUES('Drug Covered with No PA')
	INSERT INTO @NoColumnNameValues VALUES('Covered for All Medically Accepted Indications')
	INSERT INTO @NoColumnNameValues VALUES('Follows NCCN Guidelines')
	INSERT INTO @NoColumnNameValues VALUES('B vs D')
	INSERT INTO @NoColumnNameValues VALUES('Follows FDA Guidelines')
	INSERT INTO @NoColumnNameValues VALUES('PA Required No Criteria Exist')
	INSERT INTO @NoColumnNameValues VALUES('Data Not Available')
	INSERT INTO @NoColumnNameValues VALUES('Not Covered')
	
	
	SET @StrOutput = ''

	-- Fill results table: if column value is empty or 'Not Applicable' - do not put it into the results
	SET @NewValue = LTRIM(RTRIM(@Col1Value))
	IF (LEN(@NewValue) > 0) AND (@NewValue <> 'Not Applicable')
		BEGIN
			IF EXISTS (SELECT * FROM @NoColumnNameValues v WHERE v.[Value] = @NewValue)
				-- special value
				BEGIN
					-- inserting only if not already present
					IF NOT EXISTS (SELECT * FROM @Results r WHERE r.[ColumnValue] = @NewValue)
						BEGIN
							-- inserting without a column name
							INSERT INTO @Results (ColumnName, ColumnValue) VALUES ('', @NewValue)
						END
				END
			ELSE
				-- Not a special value, normal insert into @Results table
				BEGIN
					INSERT INTO @Results (ColumnName, ColumnValue) VALUES (@Col1Name, @NewValue)
				END
		END
	
	SET @NewValue = LTRIM(RTRIM(@Col2Value))
	IF (LEN(@NewValue) > 0) AND (@NewValue <> 'Not Applicable')
		BEGIN
			IF EXISTS (SELECT * FROM @NoColumnNameValues v WHERE v.[Value] = @NewValue)
				-- special value
				BEGIN
					-- inserting only if not already present
					IF NOT EXISTS (SELECT * FROM @Results r WHERE r.[ColumnValue] = @NewValue)
						BEGIN
							-- inserting without a column name
							INSERT INTO @Results (ColumnName, ColumnValue) VALUES ('', @NewValue)
						END
				END
			ELSE
				-- Not a special value, normal insert into @Results table
				BEGIN
					INSERT INTO @Results (ColumnName, ColumnValue) VALUES (@Col2Name, @NewValue)
				END
		END
	SET @NewValue = LTRIM(RTRIM(@Col3Value))
	IF (LEN(@NewValue) > 0) AND (@NewValue <> 'Not Applicable')
		BEGIN
			IF EXISTS (SELECT * FROM @NoColumnNameValues v WHERE v.[Value] = @NewValue)
				-- special value
				BEGIN
					-- inserting only if not already present
					IF NOT EXISTS (SELECT * FROM @Results r WHERE r.[ColumnValue] = @NewValue)
						BEGIN
							-- inserting without a column name
							INSERT INTO @Results (ColumnName, ColumnValue) VALUES ('', @NewValue)
						END
				END
			ELSE
				-- Not a special value, normal insert into @Results table
				BEGIN
					INSERT INTO @Results (ColumnName, ColumnValue) VALUES (@Col3Name, @NewValue)
				END
		END
	SET @NewValue = LTRIM(RTRIM(@Col4Value))
	IF (LEN(@NewValue) > 0) AND (@NewValue <> 'Not Applicable')
		BEGIN
			IF EXISTS (SELECT * FROM @NoColumnNameValues v WHERE v.[Value] = @NewValue)
				-- special value
				BEGIN
					-- inserting only if not already present
					IF NOT EXISTS (SELECT * FROM @Results r WHERE r.[ColumnValue] = @NewValue)
						BEGIN
							-- inserting without a column name
							INSERT INTO @Results (ColumnName, ColumnValue) VALUES ('', @NewValue)
						END
				END
			ELSE
				-- Not a special value, normal insert into @Results table
				BEGIN
					INSERT INTO @Results (ColumnName, ColumnValue) VALUES (@Col4Name, @NewValue)
				END
		END
	SET @NewValue = LTRIM(RTRIM(@Col5Value))
	IF (LEN(@NewValue) > 0) AND (@NewValue <> 'Not Applicable')
		BEGIN
			IF EXISTS (SELECT * FROM @NoColumnNameValues v WHERE v.[Value] = @NewValue)
				-- special value
				BEGIN
					-- inserting only if not already present
					IF NOT EXISTS (SELECT * FROM @Results r WHERE r.[ColumnValue] = @NewValue)
						BEGIN
							-- inserting without a column name
							INSERT INTO @Results (ColumnName, ColumnValue) VALUES ('', @NewValue)
						END
				END
			ELSE
				-- Not a special value, normal insert into @Results table
				BEGIN
					INSERT INTO @Results (ColumnName, ColumnValue) VALUES (@Col5Name, @NewValue)
				END
		END
	SET @NewValue = LTRIM(RTRIM(@Col6Value))
	IF (LEN(@NewValue) > 0) AND (@NewValue <> 'Not Applicable')
		BEGIN
			IF EXISTS (SELECT * FROM @NoColumnNameValues v WHERE v.[Value] = @NewValue)
				-- special value
				BEGIN
					-- inserting only if not already present
					IF NOT EXISTS (SELECT * FROM @Results r WHERE r.[ColumnValue] = @NewValue)
						BEGIN
							-- inserting without a column name
							INSERT INTO @Results (ColumnName, ColumnValue) VALUES ('', @NewValue)
						END
				END
			ELSE
				-- Not a special value, normal insert into @Results table
				BEGIN
					INSERT INTO @Results (ColumnName, ColumnValue) VALUES (@Col6Name, @NewValue)
				END
		END
	SET @NewValue = LTRIM(RTRIM(@Col7Value))
	IF (LEN(@NewValue) > 0) AND (@NewValue <> 'Not Applicable')
		BEGIN
			IF EXISTS (SELECT * FROM @NoColumnNameValues v WHERE v.[Value] = @NewValue)
				-- special value
				BEGIN
					-- inserting only if not already present
					IF NOT EXISTS (SELECT * FROM @Results r WHERE r.[ColumnValue] = @NewValue)
						BEGIN
							-- inserting without a column name
							INSERT INTO @Results (ColumnName, ColumnValue) VALUES ('', @NewValue)
						END
				END
			ELSE
				-- Not a special value, normal insert into @Results table
				BEGIN
					INSERT INTO @Results (ColumnName, ColumnValue) VALUES (@Col7Name, @NewValue)
				END
		END
	
	

	SELECT @StrOutput = COALESCE(@StrOutput + '|', '') + 
		CASE [ColumnName] WHEN '' THEN '' ELSE [ColumnName] + ': ' END + 
		[ColumnValue] FROM @Results

	-- Remove leading separator
	IF LEN(@StrOutput) > LEN('|')
		BEGIN
			SET @StrOutput = RIGHT(@StrOutput, LEN(@StrOutput) - LEN('|'))
		END

	RETURN @StrOutput

END


GO


