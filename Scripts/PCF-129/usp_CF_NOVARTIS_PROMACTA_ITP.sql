USE [PATT]
GO
/****** Object:  StoredProcedure [dbo].[usp_CF_NOVARTIS_PROMACTA_ITP]    Script Date: 3/19/2019 2:29:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC usp_CF_NOVARTIS_PROMACTA_ITP @ClientID = 15, @Userid = 2635, @IndOrDrug = 'ITP'
ALTER PROCEDURE [dbo].[usp_CF_NOVARTIS_PROMACTA_ITP]
	@ClientID INT,
	@Userid INT,
	@IndOrDrug VARCHAR(50)
AS
BEGIN

DECLARE @selectedFields AS VARCHAR(MAX)
DECLARE @StateRaw AS CF_NOVARTIS_STATE_ITP
DECLARE @PbmRaw   AS CF_NOVARTIS_PBM_ITP
DECLARE @States TABLE(ID INT IDENTITY(1,1) NOT NULL, [State] VARCHAR(255), [StateName] VARCHAR(255))
DECLARE @PbmITP      AS CF_NOVARTIS_RESULT_PROMACTA_ITP
DECLARE @OutputUnion AS CF_NOVARTIS_RESULT_PROMACTA_ITP


	------------------------ Get Raw tables from usp_Get_Plans ------------------------
	-- Get State File
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_STATE_ITP','');
	INSERT INTO @StateRaw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = @IndOrDrug, 							
			@Userid = @Userid, 		
			@SelectFields = @selectedFields,
			@Level = 'State'


	-- Get PBM File
	SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_NOVARTIS_PBM_ITP','');
	INSERT INTO @PbmRaw
		EXEC dbo.usp_Get_Plans 										
			@IndicationorDrug = @IndOrDrug, 							
			@Userid = @Userid, 				
			@SelectFields = @selectedFields,					
			@isPBMOnly = 1
																		--SELECT * FROM @StateRaw
																		--SELECT * FROM @PbmRaw

	------------------------ Filter, remove unwanted rows ------------------------
	DELETE FROM @StateRaw
	WHERE (Drug_Name <> 'Promacta')

	DELETE FROM @PbmRaw
	WHERE (Drug_Name <> 'Promacta')
	OR (MCoid NOT IN (SELECT McoId FROM PBMFilter pf WHERE (pf.ClientID = @ClientID) AND (pf.Indication = @IndOrDrug)))

																		--SELECT * FROM @StateRaw
																		--SELECT * FROM @PbmRaw






	------------------------ Remapping and Business Rules ------------------------

	-- Fill State
	INSERT INTO @OutputUnion
		SELECT
			[STATE_CD] = LTRIM(RTRIM(REPLACE(REPLACE([State],CHAR(13),' '),CHAR(10),' ') )),
			[STATE_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([StateName],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_ID] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([TZG Payer Name],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_RANK] = '',
			[LIVES] = LTRIM(RTRIM(REPLACE(REPLACE([Pharmacy],CHAR(13),' '),CHAR(10),' ') )),

			[PLAN_TYPE] =	REPLACE(REPLACE(
			
								ISNULL( CASE LTRIM(RTRIM([Segment]))
									WHEN 'Managed Medicaid' THEN 'Medicaid'
									WHEN 'State Medicaid' THEN 'Medicaid'
									WHEN 'Managed Medicare' THEN 'Medicare'
									WHEN 'Commercial MCO' THEN 'Commercial'
								ELSE 
									LTRIM(RTRIM([Segment]))
								END,
								'')

							,CHAR(13),' '),CHAR(10),' '),

			[TIER] =		REPLACE(REPLACE(

								ISNULL( CASE LTRIM(RTRIM([Novartis_Healthplan_Management])) 
									WHEN 'To PI or Better' THEN 'Covered'
									WHEN 'Non-Preferred' THEN 'Covered'
									WHEN 'Step Edit - Preferred' THEN 'Covered'
								ELSE 
									LTRIM(RTRIM([Novartis_Healthplan_Management]))
								END,
								'')

							,CHAR(13),' '),CHAR(10),' '),

			[TIER_NUMBER] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )), 
			[INDICATION] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )), 
			[DW_INS_DT] = LTRIM(RTRIM(REPLACE(REPLACE([dbo].[udf_ConvertToDate]([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )), 
			[RETAIL_COPAY_MIN] = '',
			[RETAIL_COPAY_MAX] = '',
			[MO_COPAY_MIN] = '',
			[MO_COPAY_MAX] = '',
			[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )), 

			[PA_URL] =	REPLACE(REPLACE(

							ISNULL( CASE LTRIM(RTRIM([URL_to_PA_Form])) 
										WHEN 'Data Not Available' THEN LTRIM(RTRIM([General_PA_Form]))
									ELSE 
										LTRIM(RTRIM([URL_to_PA_Form]))
									END,
									'')

						,CHAR(13),' '),CHAR(10),' '),
			[RESTRICTION_CODE] = REPLACE(REPLACE(

									[dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode](
										[PA_Required],
										[Quantity_Limitation],
										[Step_Therapy_Req]
									 )

								,CHAR(13),' '),CHAR(10),' '),
			[RESTRICTION_DETAIL_TEXT] = REPLACE(REPLACE(

											[dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionDetailText](
												'Age Restriction', [Age_Limit],
												'Step Therapy Notes', [Step_Therapy_Notes],
												'Platelet Count Requirement', [Platelet_Count_Requirement],
												'Splenectomy Required', [Splenectomy_Required],
												'Lab Requirements', [Lab_Requirements],
												'Proof of Effectiveness Required Summary', [Proof_of_Effectiveness_Required_Summary],
												'Specialist Appr', [Specialist_Appr]
											)

										,CHAR(13),' '),CHAR(10),' '),

			[PROD_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') ))

		FROM @StateRaw


	-- Fill PBM
	INSERT INTO @PbmITP
		SELECT
			[STATE_CD] = '',
			[STATE_NAME] = '',
			[PLAN_ID] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([PayerName],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[PLAN_RANK] = '',
			[LIVES] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Self_Insured_Lives],CHAR(13),' '),CHAR(10),' ') )),

			[PLAN_TYPE] =	REPLACE(REPLACE(

								ISNULL( CASE LTRIM(RTRIM([MCoid]))
									WHEN '90028000'  THEN 'Medicaid'
									WHEN '900252005' THEN 'Medicaid'
									WHEN '900332000' THEN 'Medicaid'
								ELSE 
									'Commercial'
								END,
								'')

							,CHAR(13),' '),CHAR(10),' '),

			[TIER] =	REPLACE(REPLACE(

							ISNULL( CASE LTRIM(RTRIM([Novartis_Healthplan_Management])) 
								WHEN 'To PI or Better'       THEN 'Covered'
								WHEN 'Non-Preferred'         THEN 'Covered'
								WHEN 'Step Edit - Preferred' THEN 'Covered'
							ELSE 
								LTRIM(RTRIM([Novartis_Healthplan_Management]))
							END,
							'')

						,CHAR(13),' '),CHAR(10),' '),

			[TIER_NUMBER] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[INDICATION] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[DW_INS_DT] = LTRIM(RTRIM(REPLACE(REPLACE([dbo].[udf_ConvertToDate]([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[RETAIL_COPAY_MIN] = '',
			[RETAIL_COPAY_MAX] = '',
			[MO_COPAY_MIN] = '',
			[MO_COPAY_MAX] = '',
			[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )), 

			[PA_URL] =	REPLACE(REPLACE(

							ISNULL( CASE LTRIM(RTRIM([URL_to_PA_Form])) 
										WHEN 'Data Not Available' THEN LTRIM(RTRIM([General_PA_Form]))
									ELSE 
										LTRIM(RTRIM([URL_to_PA_Form]))
									END,
									'')

						,CHAR(13),' '),CHAR(10),' '),

			[RESTRICTION_CODE] = REPLACE(REPLACE(

									[dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionCode](
										[PA_Required],
										[Quantity_Limitation],
										[Step_Therapy_Req]
									 )

								,CHAR(13),' '),CHAR(10),' '),

			[RESTRICTION_DETAIL_TEXT] = REPLACE(REPLACE(

											[dbo].[udf_CF_NOVARTIS_TASIGNA_PROMACTA_GetRestrictionDetailText](
												'Age Restriction', [Age_Limit],
												'Step Therapy Notes', [Step_Therapy_Notes],
												'Platelet Count Requirement', [Platelet_Count_Requirement],
												'Splenectomy Required', [Splenectomy_Required],
												'Lab Requirements', [Lab_Requirements],
												'Proof of Effectiveness Required Summary', [Proof_of_Effectiveness_Required_Summary],
												'Specialist Appr', [Specialist_Appr]
											)

										,CHAR(13),' '),CHAR(10),' '),

			[PROD_NAME] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') ))
		FROM @PbmRaw
																		--SELECT * FROM @PbmITP


	------------------------ Fill States ------------------------
	INSERT INTO @States
		SELECT DISTINCT
			[State] = [State],
			[StateName] = [StateName]
		FROM @StateRaw
		ORDER BY [State]
	
																		--SELECT * FROM @States



	
	
	------------------------ Fill PMBs from a loop into the Union output table ------------------------

	-- Cursor Loop to add PBM's for each state
	DECLARE @LoopCount INT = 1
	DECLARE @Count INT
	DECLARE @SINGLE_STATE VARCHAR(155)	 
	DECLARE @SINGLE_STATE_NAME VARCHAR(155)	 

	SELECT @Count = MAX(ID) FROM @States 

	WHILE @LoopCount <= @Count
		BEGIN
			SELECT @SINGLE_STATE = State FROM @States  WHERE ID = @LoopCount
			SELECT @SINGLE_STATE_NAME = StateName FROM @States  WHERE ID = @LoopCount

			INSERT INTO @OutputUnion
			SELECT
				[STATE_CD] = LTRIM(RTRIM(REPLACE(REPLACE(@SINGLE_STATE,CHAR(13),' '),CHAR(10),' ') )),
				[STATE_NAME] = LTRIM(RTRIM(REPLACE(REPLACE(@SINGLE_STATE_NAME,CHAR(13),' '),CHAR(10),' ') )),
				[PLAN_ID] = [PLAN_ID],
				[PayerName] = [PAYER_NAME],
				[PLAN_NAME] = [PLAN_NAME],
				[PLAN_RANK] = [PLAN_RANK],
				[LIVES] = [LIVES],
				[PLAN_TYPE] = [PLAN_TYPE],
				[TIER] = [TIER],
				[TIER_NUMBER] = [TIER_NUMBER],
				[INDICATION] = [INDICATION],
				[DW_INS_DT] = [DW_INS_DT],
				[RETAIL_COPAY_MIN] = [RETAIL_COPAY_MIN],
				[RETAIL_COPAY_MAX] = [RETAIL_COPAY_MAX],
				[MO_COPAY_MIN] = [MO_COPAY_MIN],
				[MO_COPAY_MAX] = [MO_COPAY_MAX],
				[URL_to_PA_Policy] = [POLICY_URL],
				[PA_URL] = [PA_URL],
				[RESTRICTION_CODE] = [RESTRICTION_CODE],
				[RESTRICTION_DETAIL_TEXT] = [RESTRICTION_DETAIL_TEXT],
				[PROD_NAME] = [PROD_NAME]
			FROM @PbmITP

			SET @LoopCount = @LoopCount + 1;
		END		
	
	
	
	SELECT 
		[STATE_CD],
	[STATE_NAME],
	[PLAN_ID] ,
	[ PayerName] = [PAYER_NAME] ,
	[PLAN_NAME] ,
	[PLAN_RANK] ,
	[LIVES] ,
	[PLAN_TYPE] ,
	[TIER] ,
	[TIER_NUMBER] ,
	[INDICATION] ,
	[DW_INS_DT] ,
	[RETAIL_COPAY_MIN] ,
	[RETAIL_COPAY_MAX] ,
	[MO_COPAY_MIN] ,
	[MO_COPAY_MAX],
	[URL_to_PA_Policy] = [POLICY_URL] ,
	[PA_URL] ,
	[RESTRICTION_CODE] ,
	[RESTRICTION_DETAIL_TEXT] ,
	[PROD_NAME]  
	
	FROM @OutputUnion

END

