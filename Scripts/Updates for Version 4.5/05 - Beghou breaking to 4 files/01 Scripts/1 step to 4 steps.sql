SELECT * FROM Process
WHERE ClientID = 41

UPDATE Process
	SET ProcessName = 'Beghou - Multiple files'
WHERE ProcessID = 22
---------------------------------------------------
SELECT * FROM ProcessDetail
WHERE ProcessID = 22


SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC
SELECT IDENT_CURRENT('ProcessDetail')
--DBCC CHECKIDENT ('ProcessDetail', RESEED, 77);

EXEC usp_CF_DeactivateStep 43

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 22, -- the one we just created
    @ClientID = 41, -- Genentech
    @StepDescription = 'AMD',
    @IndicationNames = 'AMD', -- just a placeholder
    @DrugNames = 'Eylea' -- just a placeholder
GO

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 22, -- the one we just created
    @ClientID = 41, -- Genentech
    @StepDescription = 'DME',
    @IndicationNames = 'DME', -- just a placeholder
    @DrugNames = 'Eylea' -- just a placeholder
GO

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 22, -- the one we just created
    @ClientID = 41, -- Genentech
    @StepDescription = 'DR',
    @IndicationNames = 'DR', -- just a placeholder
    @DrugNames = 'Eylea' -- just a placeholder
GO

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 22, -- the one we just created
    @ClientID = 41, -- Genentech
    @StepDescription = 'RVO',
    @IndicationNames = 'RVO', -- just a placeholder
    @DrugNames = 'Eylea' -- just a placeholder
GO

SELECT * FROM ProcessDetail
WHERE ProcessID = 22