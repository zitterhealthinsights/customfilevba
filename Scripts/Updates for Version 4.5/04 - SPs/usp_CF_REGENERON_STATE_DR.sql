--USE [CWP_3.0]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_REGENERON_STATE_DR]    Script Date: 5/4/2018 3:57:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- EXEC usp_CF_REGENERON_STATE_DR 2652, 'DR'
CREATE PROCEDURE [dbo].[usp_CF_REGENERON_STATE_DR]
	@Userid INT,
	@IndOrDrug VARCHAR(50)
AS
BEGIN

		DECLARE @Get_StateDR as CF_REGENERON_STATE_DR
		DECLARE @selectedFields as varchar(max)
		SET @selectedFields=dbo.fnGetColumnsAsCSV('CF_REGENERON_STATE_DR','');
		Insert into @Get_StateDR
		EXEC dbo.usp_Get_Plans
			@UserID = @Userid, 
			@IndicationorDrug = @IndOrDrug,
			@SelectFields = @selectedFields,
			@Level = 'State'
				
		SELECT
			[MCoid] = LTRIM(RTRIM(REPLACE(REPLACE([MCoid],CHAR(13),' '),CHAR(10),' ') )),
			[PayerName] = LTRIM(RTRIM(REPLACE(REPLACE([TZG Payer Name],CHAR(13),' '),CHAR(10),' ') )),
			[PlanName] = LTRIM(RTRIM(REPLACE(REPLACE([Plan_Name],CHAR(13),' '),CHAR(10),' ') )),
			[StateName] = LTRIM(RTRIM(REPLACE(REPLACE([StateName],CHAR(13),' '),CHAR(10),' ') )),
			[State] = LTRIM(RTRIM(REPLACE(REPLACE([State],CHAR(13),' '),CHAR(10),' ') )),
			[Segment] = LTRIM(RTRIM(REPLACE(REPLACE([Segment],CHAR(13),' '),CHAR(10),' ') )),
			[Indication] = LTRIM(RTRIM(REPLACE(REPLACE([Indication],CHAR(13),' '),CHAR(10),' ') )),
			[MarketSharePercentage] = LTRIM(RTRIM(REPLACE(REPLACE([MarketSharePercentage],CHAR(13),' '),CHAR(10),' ') )),
			[PlanLives] = LTRIM(RTRIM(REPLACE(REPLACE([PlanLivesInState],CHAR(13),' '),CHAR(10),' ') )),
			[Self Insured Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Commercial ASO],CHAR(13),' '),CHAR(10),' ') )),
			[Fully Insured Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Commercial Fully Insured],CHAR(13),' '),CHAR(10),' ') )),
			[Medical Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Medical],CHAR(13),' '),CHAR(10),' ') )),
			[Pharmacy Lives] = LTRIM(RTRIM(REPLACE(REPLACE([Pharmacy],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_Name] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_Name],CHAR(13),' '),CHAR(10),' ') )),
			[On_Formulary] = LTRIM(RTRIM(REPLACE(REPLACE([On_Formulary],CHAR(13),' '),CHAR(10),' ') )),
			[Preferred_Status] = LTRIM(RTRIM(REPLACE(REPLACE([Regeneron_Preferred_Status],CHAR(13),' '),CHAR(10),' ') )),
			[Preferred_implementation] = LTRIM(RTRIM(REPLACE(REPLACE([Regeneron_Preferred_Implementation],CHAR(13),' '),CHAR(10),' ') )),
			[Regeneron_Healthplan_management] = LTRIM(RTRIM(REPLACE(REPLACE([Regeneron_Healthplan_management],CHAR(13),' '),CHAR(10),' ') )),
			[Regeneron_Segmentation] = '',
			[Regeneron_Tier] = '',
			[Journal_Articles_Referenced] = '',
			[Benefit_Type] = LTRIM(RTRIM(REPLACE(REPLACE([Benefit_Type],CHAR(13),' '),CHAR(10),' ') )),
			[PA_Required] = LTRIM(RTRIM(REPLACE(REPLACE([PA_Required],CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[URL_to_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([URL_to_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[General_PA_Form] = LTRIM(RTRIM(REPLACE(REPLACE([General_PA_Form],CHAR(13),' '),CHAR(10),' ') )),
			[URL_To_Draft_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([URL_To_Draft_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[Tier_Placement] = LTRIM(RTRIM(REPLACE(REPLACE([Tier_Placement],CHAR(13),' '),CHAR(10),' ') )),
			[Number_of_Tiers] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Tiers],CHAR(13),' '),CHAR(10),' ') )),
			[Dosing_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Dosing_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Quantity_Limitation_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Quantity_Limitation_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Initial_Auth_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Initial_Auth_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Recert_Time_Length] = LTRIM(RTRIM(REPLACE(REPLACE([Recert_Time_Length],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Place_In_Therapy] = LTRIM(RTRIM(REPLACE(REPLACE([Number_of_Steps],CHAR(13),' '),CHAR(10),' ') )),
			[Step_Therapy_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Step_Therapy_Notes],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_PA_Criteria_details] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_PA_Criteria_details],CHAR(13),' '),CHAR(10),' ') )),
			[Eye_Limitation_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Eye_Limitation_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Age_Limit] = LTRIM(RTRIM(REPLACE(REPLACE([Age_Limit],CHAR(13),' '),CHAR(10),' ') )),
			[OCT_Req] = LTRIM(RTRIM(REPLACE(REPLACE([OCT_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Fluorescein_Angiography_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Fluorescein_Angiography_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Visual_Acuity_Test_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Visual_Acuity_Test_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Visual_Acuity_Maint/Improvement_Req] = LTRIM(RTRIM(REPLACE(REPLACE([Visual_Acuity_Maint/Improvement_Req],CHAR(13),' '),CHAR(10),' ') )),
			[Visual_Acuity_Maint/Improvement_Desc] = LTRIM(RTRIM(REPLACE(REPLACE([Visual_Acuity_Maint/Improvement_Desc],CHAR(13),' '),CHAR(10),' ') )),
			[Visual_Acuity_LessOrEqual_0_to_50_Exception] = '',
			[Visual_Acuity_LessOrEqual_0_to_50_ExceptionDescription] = '',
			[Nbr_of_Bio_Injectable_Trials] = LTRIM(RTRIM(REPLACE(REPLACE([Nbr_of_Bio_Injectable_Trials],CHAR(13),' '),CHAR(10),' ') )),
			[BioInject_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([BioInject_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Specific_Biologic_Failure] = LTRIM(RTRIM(REPLACE(REPLACE([Specific_Biologic_Failure],CHAR(13),' '),CHAR(10),' ') )),
			[Laser_Photocoagulation_Trial_Ct] = LTRIM(RTRIM(REPLACE(REPLACE([Laser_Photocoagulation_Trial_Ct],CHAR(13),' '),CHAR(10),' ') )),
			[Laser_Photocoagulation_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Laser_Photocoagulation_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Intraocular_Corticosteroid_Trial_Ct] = LTRIM(RTRIM(REPLACE(REPLACE([Intraocular_Corticosteroid_Trial_Ct],CHAR(13),' '),CHAR(10),' ') )),
			[Intraocular_Corticosteroid_Opr] = LTRIM(RTRIM(REPLACE(REPLACE([Intraocular_Corticosteroid_Opr],CHAR(13),' '),CHAR(10),' ') )),
			[Drug_covered] = LTRIM(RTRIM(REPLACE(REPLACE([Drug_covered],CHAR(13),' '),CHAR(10),' ') )),
			[Documentation_for_Dx] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_for_Dx],CHAR(13),' '),CHAR(10),' ') )),
			[Must_submit_doc_for_Dx] = LTRIM(RTRIM(REPLACE(REPLACE([Must_submit_doc_for_Dx],CHAR(13),' '),CHAR(10),' ') )),
			[Lab_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Lab_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Administrative_Requirements] = LTRIM(RTRIM(REPLACE(REPLACE([Administrative_Requirements],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy_Criteria] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_Criteria],CHAR(13),' '),CHAR(10),' ') )),
			[Proof_of_Effectiveness_Required_Summary] = LTRIM(RTRIM(REPLACE(REPLACE([Proof_of_Effectiveness_Required_Summary],CHAR(13),' '),CHAR(10),' ') )),
			[Separate_Pharmacy_and_Medical_Policy_URL] = LTRIM(RTRIM(REPLACE(REPLACE([Separate_Pharmacy_and_Medical_Policy_URL],CHAR(13),' '),CHAR(10),' ') )),
			[ProofOfEfficacy] = LTRIM(RTRIM(REPLACE(REPLACE([ProofOfEfficacy],CHAR(13),' '),CHAR(10),' ') )),
			[J_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([J_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[ICD_Codes_Approved] = LTRIM(RTRIM(REPLACE(REPLACE([ICD_Codes_Approved],CHAR(13),' '),CHAR(10),' ') )),
			[Specialist_Appr] = LTRIM(RTRIM(REPLACE(REPLACE([Specialist_Appr],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Indicated] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Indicated],CHAR(13),' '),CHAR(10),' ') )),
			[PBM_Control] = LTRIM(RTRIM(REPLACE(REPLACE([PBM_Control],CHAR(13),' '),CHAR(10),' ') )),
			[Distribution_Limitations_Enforced] = LTRIM(RTRIM(REPLACE(REPLACE([Distribution_Limitations_Enforced],CHAR(13),' '),CHAR(10),' ') )),
			[Name_Of_Specialty_Drug_Distributer_1] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_1],CHAR(13),' '),CHAR(10),' ') )),
			[Name_Of_Specialty_Drug_Distributer_2] = LTRIM(RTRIM(REPLACE(REPLACE([Name_Of_Specialty_Drug_Distributer_2],CHAR(13),' '),CHAR(10),' ') )),
			[Other_Policy_Utilized] = LTRIM(RTRIM(REPLACE(REPLACE([Other_Policy_Utilized],CHAR(13),' '),CHAR(10),' ') )),
			[Documentation_Source] = LTRIM(RTRIM(REPLACE(REPLACE([Documentation_Source],CHAR(13),' '),CHAR(10),' ') )),
			[Change_In_Coverage] = LTRIM(RTRIM(REPLACE(REPLACE([Reason_for_Change_Details],CHAR(13),' '),CHAR(10),' ') )),
			[Policy_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Policy_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Renewal_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Renewal_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[No_Policy_Date_Found] = LTRIM(RTRIM(REPLACE(REPLACE([No_Policy_Date_Found],CHAR(13),' '),CHAR(10),' ') )),
			[Entry_Date] = LTRIM(RTRIM(REPLACE(REPLACE(dbo.udf_ConvertToDate([Entry_Date]),CHAR(13),' '),CHAR(10),' ') )),
			[Note_1] = LTRIM(RTRIM(REPLACE(REPLACE([Note_1],CHAR(13),' '),CHAR(10),' ') )),
			[Note_2] = LTRIM(RTRIM(REPLACE(REPLACE([Note_2],CHAR(13),' '),CHAR(10),' ') )),
			[Note_3] = LTRIM(RTRIM(REPLACE(REPLACE([Note_3],CHAR(13),' '),CHAR(10),' ') ))
		FROM @Get_StateDR

END


GO