--USE [CWP_3.0]
--GO

/****** Object:  StoredProcedure [dbo].[usp_CF_CustomFile]    Script Date: 5/4/2018 4:33:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*

**** Merck **** deactivated

**** Janssen **** deactivated

**** Sandoz **** 
  -- 'Sandoz Custom File' : EXEC usp_CF_CustomFile @ClientID = 67, @CategoryID = 8, @IndicationArray = 'NEU', @DrugArray = '', @ScheduleTypeID = 2
                            EXEC usp_CF_CustomFile @ClientID = 67, @CategoryID = 8, @IndicationArray = 'GHD', @DrugArray = '', @ScheduleTypeID = 2
                            EXEC usp_CF_CustomFile @ClientID = 67, @CategoryID = 8, @IndicationArray = 'CLL', @DrugArray = '', @ScheduleTypeID = 2 --CLL and NHL
							-- (we couldn't put 'CLL,NHL' to act like "join indications into file" because all the other steps act as "separate indications into different files")
                            EXEC usp_CF_CustomFile @ClientID = 67, @CategoryID = 8, @IndicationArray = 'RA', @DrugArray = '', @ScheduleTypeID = 2

**** Regeneron - Sanofi ****
  -- 'Regeneron - Sanofi  - Custom File - HCHL Files' : EXEC usp_CF_CustomFile @ClientID = 58, @CategoryID = 9, @IndicationArray = 'HCHL', @DrugArray = '', @ScheduleTypeID = 2
                                                        EXEC usp_CF_CustomFile @ClientID = 58, @CategoryID = 9, @IndicationArray = 'RA', @DrugArray = '', @ScheduleTypeID = 2

  -- 'Payer Sciences - HCHL File' : EXEC usp_CF_CustomFile @ClientID = 58, @CategoryID = 10, @IndicationArray = 'HCHL', @DrugArray = '', @ScheduleTypeID = 2
  -- 'McCann -  HCHL State File' : EXEC usp_CF_CustomFile @ClientID = 58, @CategoryID = 11, @IndicationArray = 'HCHL', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Protean - HCHL State File' : EXEC usp_CF_CustomFile @ClientID = 58, @CategoryID = 12, @IndicationArray = 'HCHL', @DrugArray = '', @ScheduleTypeID = 2

**** Heron **** 
  -- 'Heron - One File / Multiple Tabs' : EXEC usp_CF_CustomFile @ClientID = 63, @CategoryID = 13, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

**** Regeneron (Beghou) ****
  -- 'Beghou - One File / Multiple Tabs' : EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 14, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Regeneron - AMD State File'        : EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 24, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Regeneron - DME State File'        : EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 25, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Regeneron - DR State File'         : EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 26, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Regeneron - RVO State File'        : EXEC usp_CF_CustomFile @ClientID = 41, @CategoryID = 27, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

**** Novartis ****
  -- 'Weekly Files for Novartis Entresto'         : EXEC usp_CF_CustomFile @ClientID = 15, @CategoryID = 15, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 1
  -- 'Weekly Files for Novartis Jadenu'           : EXEC usp_CF_CustomFile @ClientID = 15, @CategoryID = 17, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 1
  -- 'Monthly Files for Novartis Access Analysis' : EXEC usp_CF_CustomFile @ClientID = 15, @CategoryID = 19, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2
  -- 'Monthly File for Novartis RCC'              : EXEC usp_CF_CustomFile @ClientID = 15, @CategoryID = 22, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

**** BMS ****
  -- 'Monthly Files for BMS' : EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'HL', @DrugArray = '', @ScheduleTypeID = 2
                               EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'MEL', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'MM', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'NSCLC', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'RA', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'RCC', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'HN', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'URC', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'CRC', @DrugArray = '', @ScheduleTypeID = 2
							   EXEC usp_CF_CustomFile @ClientID = 6, @CategoryID = 16, @IndicationArray = 'HCC', @DrugArray = '', @ScheduleTypeID = 2
				   
**** Genentech ****
  -- 'Monthly Files for Genentech'
            EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'Asthma', @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'CF',     @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'IPF',    @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'AMD',    @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'MS',     @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'BCC',    @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'RA',     @DrugArray = '', @ScheduleTypeID = 2
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 18, @IndicationArray = 'Hemo',   @DrugArray = '', @ScheduleTypeID = 2

	-- 'Genentech Specialty Pharmacy'
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 20, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

	-- 'Monthly Genetech Ocular File'
			EXEC usp_CF_CustomFile @ClientID = 9, @CategoryID = 23, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2


**** Ipsen ****
  -- 'Monthly Files for Ipsen Dysport'
            EXEC usp_CF_CustomFile @ClientID = 51, @CategoryID = 21, @IndicationArray = '', @DrugArray = '', @ScheduleTypeID = 2

*/
 

ALTER PROCEDURE [dbo].[usp_CF_CustomFile]  
	-- Add the parameters for the stored procedure here
	@ClientID INT,
	@CategoryID INT,
	@IndicationArray VARCHAR(255),	
	@DrugArray VARCHAR(255),
	@ScheduleTypeID INT
AS
BEGIN

DECLARE @ClientAdminUserID INT ;
DECLARE @PACKING_SLIP TABLE ([TABNAME] VARCHAR(MAX))


	--SELECT @ScheduleTypeID = ISNULL(p.ScheduleTypeID,0) 
	--FROM Process p 
	--WHERE p.ProcessID = @ProcessID

	IF @ClientID = 32 ---Merck
	BEGIN		
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;

		--- Check the Category
		IF @CategoryID = 1 -- MET
		BEGIN
			
			---DECLARE @PACKING_SLIP TABLE ([TABNAME] VARCHAR(MAX))

			IF @ScheduleTypeID = 1 -- Run Normally, like the Weekly Process
				BEGIN 
				
					INSERT INTO @PACKING_SLIP VALUES ('Master-New')
					INSERT INTO @PACKING_SLIP VALUES ('Master')
					INSERT INTO @PACKING_SLIP VALUES ('AM-PR Overview')
					INSERT INTO @PACKING_SLIP VALUES ('Master-State')
					INSERT INTO @PACKING_SLIP VALUES ('Data Dictionary')
					INSERT INTO @PACKING_SLIP VALUES ('AM-PR Data Dictionary')
			
					-- Table 1
					SELECT * FROM @PACKING_SLIP;

					-- Table 2 ( Master New, Master, AM-PR Overview)
					EXEC usp_CF_Master_New 
							@ClientAdminUserID = @ClientAdminUserID, 
							@IndicationorDrug = @IndicationArray, 
							@DrugArray = @DrugArray;
					-- Table 3
					EXEC dbo.usp_CF_Master_State 
							@ClientAdminUserID = @ClientAdminUserID,  
							@IndicationAbbrv = @IndicationArray,  
							@DrugNameArray = @DrugArray 
					-- Table 4
					EXEC dbo.usp_Get_DataDictionary 
							@UserID = @ClientAdminUserID,  
							@Abbreviation = @IndicationArray
					-- Table 5
					EXEC  dbo.usp_CF_AMPR_DataDictionary
				END 
			ELSE
				BEGIN 
						--Monthly Process
						DECLARE @SINGLE_INDICATION VARCHAR(255)

						-- Construct Packing Slip
						DECLARE PACKINGSLIP_CURSOR CURSOR
						LOCAL STATIC READ_ONLY FORWARD_ONLY
						FOR 
							SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs

							OPEN PACKINGSLIP_CURSOR
							FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
							WHILE @@FETCH_STATUS = 0
							BEGIN 
									INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Master')
									INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Data')
									INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' PBM Data')
									INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Data Dictionary')

								FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
							END 
						CLOSE PACKINGSLIP_CURSOR
						DEALLOCATE PACKINGSLIP_CURSOR

						INSERT INTO @PACKING_SLIP VALUES ( 'Definition')

				-------------------------------------------------------------------------------------------------------

				-- Table 1
				SELECT * FROM @PACKING_SLIP;

				-- Process each indication
				DECLARE INDICATION_CURSOR CURSOR
				LOCAL STATIC READ_ONLY FORWARD_ONLY
				FOR 
					SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs

					OPEN INDICATION_CURSOR
					FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
					WHILE @@FETCH_STATUS = 0
					BEGIN 
							--PRINT '-----------------------------------------------------------------------------------'
							--PRINT ' >>>>>>>>>>>>>>>>>>>>>> ' + @SINGLE_INDICATION
							--PRINT '-----------------------------------------------------------------------------------'

							-- Drug Array to use for each client intication
							DECLARE @ClientDrugs VARCHAR(8000)
							SELECT @ClientDrugs = COALESCE(@ClientDrugs + ', ', '') + RTRIM(LTRIM(vccdi.Drug)) FROM dbo.vw_CF_ClientDrugIndication vccdi
							WHERE ClientID = @ClientID
							AND vccdi.IndicationAbbrv = @SINGLE_INDICATION

							-- [Indication Name] + Master Data, and Master Raw Data
							EXEC usp_CF_Master_Only
									@ClientAdminUserID = @ClientAdminUserID, 
									@IndicationorDrug = @SINGLE_INDICATION, 
									@DrugArray = @ClientDrugs;

							-- PBM Data
							EXEC usp_Get_Plans 
									@Userid = @ClientAdminUserID, 
									@IndicationorDrug = @SINGLE_INDICATION, 
									@isPBMOnly = 1;

							-- Data Dictionary
							EXEC dbo.usp_Get_DataDictionary 
									@UserID = @ClientAdminUserID,  
									@Abbreviation = @SINGLE_INDICATION

						FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
					END 
				CLOSE INDICATION_CURSOR
				DEALLOCATE INDICATION_CURSOR


				-- Only Need One Definition at the end
				EXEC dbo.usp_Get_DataDefinition 
						@UserID = @ClientAdminUserID

	  
			END 
				
		END


		--Next Category
		IF @CategoryID = 2 -- MET - Pivots
		BEGIN
			
			IF @ScheduleTypeID = 2 -- Relevant to the Monthly Process
				BEGIN 
				
						-- Construct Packing Slip
						DECLARE PACKINGSLIP_CURSOR CURSOR
						LOCAL STATIC READ_ONLY FORWARD_ONLY
						FOR 
							SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs

							OPEN PACKINGSLIP_CURSOR
							FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
							WHILE @@FETCH_STATUS = 0
							BEGIN 
									INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Master')
								---	INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Pivots')
 
								FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
							END 
						CLOSE PACKINGSLIP_CURSOR
						DEALLOCATE PACKINGSLIP_CURSOR

					-- Table 1
					SELECT * FROM @PACKING_SLIP;

					--PRINT 'Client Admin User ID = ' + CONVERT(VARCHAR, @ClientAdminUserID )
					--PRINT 'Indication = ' + CONVERT(VARCHAR, @IndicationArray )
					--PRINT 'Drug Array = ' + CONVERT(VARCHAR, @DrugArray )

					-- Table 2 ( Master )


							-- Process each indication
				DECLARE INDICATION_CURSOR CURSOR
				LOCAL STATIC READ_ONLY FORWARD_ONLY
				FOR 
					SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs

					OPEN INDICATION_CURSOR
					FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
					WHILE @@FETCH_STATUS = 0
					BEGIN 
							PRINT '-----------------------------------------------------------------------------------'
							PRINT ' >>>>>>>>>>>>>>>>>>>>>> ' + @SINGLE_INDICATION
							PRINT '-----------------------------------------------------------------------------------'

							-- Drug Array to use for each client intication
							--DECLARE @ClientDrugs2 VARCHAR(8000)
							--SELECT @ClientDrugs2 = COALESCE(@ClientDrugs2 + ', ', '') + RTRIM(LTRIM(vccdi.Drug)) FROM dbo.vw_CF_ClientDrugIndication vccdi
							--WHERE ClientID = @ClientID
							--AND vccdi.IndicationAbbrv = @SINGLE_INDICATION

								--PRINT 'Client Admin User ID = ' + CONVERT(VARCHAR, @ClientAdminUserID )
								--PRINT 'Indication = ' + CONVERT(VARCHAR, @IndicationArray )
								--PRINT 'Drug Array = ' + CONVERT(VARCHAR, @DrugArray )

							EXEC usp_CF_Master_Single
								@ClientAdminUserID = @ClientAdminUserID, 
								@IndicationorDrug = @SINGLE_INDICATION, 
								@DrugArray =@DrugArray;


							--Empty RS
							--SELECT '' AS PivotTable

						FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
					END 
				CLOSE INDICATION_CURSOR
				DEALLOCATE INDICATION_CURSOR


					
					/*
					
					Client Admin User ID = 2647
					Indication = MEL, NSCLC
					Drug Array = Keytruda,Opdivo
					
					EXEC usp_CF_Master_Single
							@ClientAdminUserID = 2647, 
							@IndicationorDrug = 'MEL', 
							@DrugArray ='Keytruda, Opdivo';

 
					*/
					
				 
				END 


		END 

		----End Category

	END
	ELSE IF  @ClientID = 14 ---Janssen
	   BEGIN		
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;

		--- Check the Category
		IF @CategoryID  =3 -- state
		BEGIN
			
	

							-- PBM Data
							EXEC usp_CF_JANSSEN_Get_Plans 
								   @Userid =@ClientAdminUserID,
								   @IndicationorDrug=@IndicationArray,
								   @CategoryID=@CategoryID
							
							
								-- Only Need One Definition at the end
							EXEC dbo.usp_Get_DataDefinition 
									@UserID = @ClientAdminUserID
						
						
							-- Data Dictionary
							EXEC dbo.usp_Get_DataDictionary 
									@UserID = @ClientAdminUserID,  
									@Abbreviation = @IndicationArray

						
				
		END
		IF @CategoryID = 4 -- CBSA
		BEGIN
			
				
							EXEC usp_CF_JANSSEN_Get_Plans 
								   @Userid =@ClientAdminUserID,
								   @IndicationorDrug=@IndicationArray,
								   @CategoryID=@CategoryID
							
									-- Only Need One Definition at the end
							EXEC dbo.usp_Get_DataDefinition 
								@UserID = @ClientAdminUserID
						
							-- Data Dictionary
							EXEC dbo.usp_Get_DataDictionary 
									@UserID = @ClientAdminUserID,  
									@Abbreviation = @IndicationArray

				
		END

		IF @CategoryID = 5 --Sales Division / Zip 
		BEGIN
			
			EXEC usp_CF_JANSSEN_SALES_DIVISON_ZIP
		END

		IF @CategoryID = 6 --National 
		BEGIN

			-- 4 Indications { CD, PSO, RA, UC }
			DECLARE @IndicationNational VARCHAR(25) = 'CD, PSO, RA, UC';

			-- Load Packing Slip
			DECLARE PACKINGSLIP_CURSOR CURSOR
			LOCAL STATIC READ_ONLY FORWARD_ONLY
			FOR 
				SELECT ITEM FROM dbo.fnSplit(@IndicationNational,',') fs

				OPEN PACKINGSLIP_CURSOR
				FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
				WHILE @@FETCH_STATUS = 0
				BEGIN 
						INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
 
					FETCH NEXT FROM PACKINGSLIP_CURSOR INTO @SINGLE_INDICATION
				END 
			CLOSE PACKINGSLIP_CURSOR
			DEALLOCATE PACKINGSLIP_CURSOR

			SELECT * FROM @PACKING_SLIP;

			--Loop Through each indication
			DECLARE INDICATION_CURSOR CURSOR
			LOCAL STATIC READ_ONLY FORWARD_ONLY
			FOR 
				SELECT ITEM FROM dbo.fnSplit(@IndicationNational,',') fs

				OPEN INDICATION_CURSOR
				FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
				WHILE @@FETCH_STATUS = 0
				BEGIN 
						INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
						EXEC usp_CF_JANSSEN_NATIONAL_PBM @SINGLE_INDICATION

					FETCH NEXT FROM INDICATION_CURSOR INTO @SINGLE_INDICATION
				END 
			CLOSE INDICATION_CURSOR
			DEALLOCATE INDICATION_CURSOR

		END



		IF @CategoryID = 7 -- VECTOR
		BEGIN
		
		       INSERT INTO @PACKING_SLIP VALUES ('Vector')
			
				SELECT * FROM @PACKING_SLIP;
		         EXEC usp_CF_JANSSEN_VECTOR_Get_Master
		             @Userid =@ClientAdminUserID,
					@IndicationArray=@IndicationArray
		
		END

		
    END
	 ELSE IF  @ClientID = 67 ---Sandoz
    BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
    
		----------------------------- Cursor for indications, reused in several loops
		DECLARE @Indications VARCHAR(25)
		
		-- CLL step contains 2 indications: CLL and NHL
		-- (we couldn't put 'CLL,NHL' to act like "join indications into file" because all the other steps act as "separate indications into different files")
		IF @IndicationArray = 'CLL'
			BEGIN
				SET @IndicationArray = 'CLL,NHL'
			END

		SET @Indications = @IndicationArray
		
		DECLARE curIndications CURSOR SCROLL
		LOCAL STATIC READ_ONLY
		FOR SELECT ITEM FROM dbo.fnSplit(@Indications,',') fs
			
		----------------------------- Packing Slip
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Recent Changes')
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' PBM')
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' by State')
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
			
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + '_DD')
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications
		
		INSERT INTO @PACKING_SLIP VALUES ('Definition')
		SELECT * FROM @PACKING_SLIP;
		

		------------------------------- Recent Changes, Flat, PBM, State
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN
				
				IF @SINGLE_INDICATION = 'NEU'
					BEGIN
						SELECT 'NEU Recent Changes' AS 'Recent Changes'
						EXEC usp_CF_SANDOZ_FLAT_NEU 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NEU'
						EXEC usp_CF_SANDOZ_PBM_NEU 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NEU'
						EXEC usp_CF_SANDOZ_STATE_NEU 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NEU'
					END
				ELSE IF @SINGLE_INDICATION = 'GHD'
					BEGIN
						SELECT 'GHD Recent Changes' AS 'Recent Changes'
						EXEC usp_CF_SANDOZ_FLAT_GHD 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'GHD'
						EXEC usp_CF_SANDOZ_PBM_GHD 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'GHD'
						EXEC usp_CF_SANDOZ_STATE_GHD 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'GHD'
					END
				ELSE IF @SINGLE_INDICATION = 'CLL'
					BEGIN
						SELECT 'CLL Recent Changes' AS 'Recent Changes'
						EXEC usp_CF_SANDOZ_FLAT_CLL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'CLL'
						EXEC usp_CF_SANDOZ_PBM_CLL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'CLL'
						EXEC usp_CF_SANDOZ_STATE_CLL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'CLL'
					END
				ELSE IF @SINGLE_INDICATION = 'NHL'
					BEGIN
						SELECT 'NHL Recent Changes' AS 'Recent Changes'
						EXEC usp_CF_SANDOZ_FLAT_NHL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NHL'
						EXEC usp_CF_SANDOZ_PBM_NHL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NHL'
						EXEC usp_CF_SANDOZ_STATE_NHL 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'NHL'
					END
				ELSE IF @SINGLE_INDICATION = 'RA'
					BEGIN
						SELECT 'RA Recent Changes' AS 'Recent Changes'
						EXEC usp_CF_SANDOZ_FLAT_RA 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'RA'
						EXEC usp_CF_SANDOZ_PBM_RA 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'RA'
						EXEC usp_CF_SANDOZ_STATE_RA 
							@Userid = @ClientAdminUserID,
							@IndOrDrug = 'RA'
					END


				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications

		------------------------------- [INDICATION]_DD
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN

				EXEC dbo.usp_Get_DataDictionary 
					@UserID = @ClientAdminUserID,  
					@Abbreviation = @SINGLE_INDICATION

				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications

		------------------------------- Definition
		EXEC dbo.usp_Get_DataDefinition 
			@UserID = @ClientAdminUserID

    END





	ELSE IF  @ClientID = 58 ---Regeneron - Sanofi
    BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
    
		-- Regeneron
		IF @CategoryID = 9 
			BEGIN
				----------------------------- Packing Slip
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray )
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' PBM Data')
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' Data Dictionary')
				INSERT INTO @PACKING_SLIP VALUES ( 'Definition')
				SELECT * FROM @PACKING_SLIP;
				
				----------------------------- [INDICATION]
				IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_FLAT_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END

				ELSE IF @IndicationArray = 'RA'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_FLAT_RA
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				
				----------------------------- [INDICATION] PBM Data
				IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_PBM_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_PBM_RA
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				
				----------------------------- [INDICATION] Data Dictionary
				EXEC dbo.usp_Get_DataDictionary 
					@UserID = @ClientAdminUserID,  
					@Abbreviation = @IndicationArray
				
				----------------------------- Definition
				EXEC dbo.usp_Get_DataDefinition 
					@UserID = @ClientAdminUserID

			END
			
			
		-- Regeneron-PayerSciences
		IF @CategoryID = 10
			BEGIN
				----------------------------- Packing Slip
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray )
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' PBM Data')
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' Data Dictionary')
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' Health Plan Management')
				INSERT INTO @PACKING_SLIP VALUES ( 'Definition')
				SELECT * FROM @PACKING_SLIP;
				
				----------------------------- [INDICATION]
				IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_PAYER_SCIENCES_FLAT_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						SELECT 'This indication does not have a defined custom file' AS 'Error Message'
					END
				
				----------------------------- [INDICATION] PBM Data
				IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_PAYER_SCIENCES_PBM_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						SELECT 'This indication does not have a defined custom file' AS 'Error Message'
					END
				
				----------------------------- [INDICATION] Data Dictionary
				EXEC dbo.usp_Get_DataDictionary 
					@UserID = @ClientAdminUserID,  
					@Abbreviation = @IndicationArray
					
					
				----------------------------- [INDICATION] Health Plan Management
				SELECT 'Health Plan Management' AS 'Health Plan Management'
				
				----------------------------- Definition
				EXEC dbo.usp_Get_DataDefinition 
					@UserID = @ClientAdminUserID

			END
			
			
		-- Regeneron-McCann
		IF @CategoryID = 11
			BEGIN
				----------------------------- Packing Slip
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray ) -- It's 'HCHL by State' but users want 'HCHL'
				INSERT INTO @PACKING_SLIP VALUES ( 'Definition')
				SELECT * FROM @PACKING_SLIP;
				
				----------------------------- [INDICATION]  by State
				IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_MCCANN_STATE_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						SELECT 'This indication does not have a defined custom file' AS 'Error Message'
					END
				
				----------------------------- Definition
				EXEC dbo.usp_Get_DataDefinition 
					@UserID = @ClientAdminUserID

			END		
			
		-- Regeneron-Protean
		IF @CategoryID = 12
			BEGIN
				----------------------------- Packing Slip
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' by State')
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' Data Dictionary')
				INSERT INTO @PACKING_SLIP VALUES ( @IndicationArray + ' Health Plan Management')
				INSERT INTO @PACKING_SLIP VALUES ( 'Definition')
				SELECT * FROM @PACKING_SLIP;
				
				----------------------------- [INDICATION]  by State
				IF @IndicationArray = 'HCHL'
					BEGIN
						EXEC dbo.usp_CF_REGENERON_PROTEAN_STATE_HCHL
							@Userid = @ClientAdminUserID,
							@IndOrDrug = @IndicationArray
					END
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						SELECT 'This indication does not have a defined custom file' AS 'Error Message'
					END
				
				----------------------------- [INDICATION] Data Dictionary
				EXEC dbo.usp_Get_DataDictionary 
					@UserID = @ClientAdminUserID,  
					@Abbreviation = @IndicationArray
					
					
				----------------------------- [INDICATION] Health Plan Management
				SELECT 'Health Plan Management' AS 'Health Plan Management'
				
				----------------------------- Definition
				EXEC dbo.usp_Get_DataDefinition 
					@UserID = @ClientAdminUserID

			END		
    END
    
    
    ELSE IF  @ClientID = 63 -- Heron
    BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
    
		----------------------------- Cursor for indications, reused in several loops
		--DECLARE @Indications VARCHAR(25) = 'CINV';
		SET @Indications = 'CINV'; -- Already declared in Sandoz code
		
		DECLARE curIndications CURSOR SCROLL
		LOCAL STATIC READ_ONLY
		FOR SELECT ITEM FROM dbo.fnSplit(@Indications,',') fs
			
		----------------------------- Packing Slip
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				-- Give 'Recent Changes' after source sheet - so no need for PT refresh in VBA
				-- VBA will reorder sheets in proper way
				-- For Heron only, not needed for Sandoz because Sandoz has no ValueFields(Sums)
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' by State' )
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Recent Changes by State')
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' Recent Changes')
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
			
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + '_DD')
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications
		
		INSERT INTO @PACKING_SLIP VALUES ('Definition')
		SELECT * FROM @PACKING_SLIP;
		
		
		
		----------------------------- CINV
		EXEC usp_CF_HERON_STATE_CINV
			@Userid = @ClientAdminUserID,
			@SINGLE_INDICATION = 'CINV'
		SELECT 'CINV Recent Changes by State' AS 'Recent Changes by State'
		EXEC usp_CF_HERON_FLAT_CINV
			@Userid = @ClientAdminUserID,
			@SINGLE_INDICATION = 'CINV'
		SELECT 'CINV Recent Changes' AS 'Recent Changes'
		----------------------------- [INDICATION]_DD
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				EXEC dbo.usp_Get_DataDictionary 
					@UserID = @ClientAdminUserID,  
					@Abbreviation = @SINGLE_INDICATION
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications
		DEALLOCATE curIndications
		
		----------------------------- Definition
		EXEC dbo.usp_Get_DataDefinition 
			@UserID = @ClientAdminUserID
    END
    
    
    
    
    ELSE IF  @ClientID = 41 -- Regeneron
    BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
    
		IF @CategoryID = 14 -- (Beghou)
		BEGIN
			----------------------------- Cursor for indications, reused in several loops
			--DECLARE @Indications VARCHAR(25) = 'AMD,DME,RVO,DR';
			SET @Indications = 'AMD,DME,RVO,DR'; -- Already declared in Sandoz code
		
			DECLARE curIndications CURSOR SCROLL
			LOCAL STATIC READ_ONLY
			FOR SELECT ITEM FROM dbo.fnSplit(@Indications,',') fs
			
			----------------------------- Packing Slip
			OPEN curIndications
				FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
				WHILE @@FETCH_STATUS = 0
				BEGIN 
					INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
					FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
				END 
			CLOSE curIndications
		
			INSERT INTO @PACKING_SLIP VALUES ('HP Management')
			INSERT INTO @PACKING_SLIP VALUES ('Data Dictionary')
			INSERT INTO @PACKING_SLIP VALUES ('Definition')
			SELECT * FROM @PACKING_SLIP;
		
		
		
			----------------------------- AMD
			EXEC usp_CF_BEGHOU_CBSA_AMD
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'AMD'
			
			----------------------------- DME
			EXEC usp_CF_BEGHOU_CBSA_DME
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'DME'
			
			----------------------------- RVO
			EXEC usp_CF_BEGHOU_CBSA_RVO
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'RVO'

			----------------------------- DR
			EXEC usp_CF_BEGHOU_CBSA_DR
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'DR'
			
			----------------------------- HP Management
			SELECT 'HP Management' AS 'HP Management'

			----------------------------- Data Dictionary
			EXEC dbo.usp_Get_DataDictionary 
				@UserID = @ClientAdminUserID,  
				@Abbreviation = 'RVO' -- We call it for RVO, because AMD and DME are default to RVO anyway

			DEALLOCATE curIndications
		
			----------------------------- Definition
			EXEC dbo.usp_Get_DataDefinition 
				@UserID = @ClientAdminUserID
		END

		
		IF @CategoryID = 24 -- 'Regeneron - AMD State File'
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('AMD')
			INSERT INTO @PACKING_SLIP VALUES ('Definition')
			SELECT * FROM @PACKING_SLIP;

			EXEC usp_CF_REGENERON_STATE_AMD
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'AMD'

			EXEC dbo.usp_Get_DataDefinition 
				@UserID = @ClientAdminUserID
		END

		IF @CategoryID = 25 -- 'Regeneron - DME State File'
		BEGIN			   
			INSERT INTO @PACKING_SLIP VALUES ('DME')
			INSERT INTO @PACKING_SLIP VALUES ('Definition')
			SELECT * FROM @PACKING_SLIP;

		    EXEC usp_CF_REGENERON_STATE_DME
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'DME'

			EXEC dbo.usp_Get_DataDefinition 
				@UserID = @ClientAdminUserID
		END				   

		IF @CategoryID = 26 -- 'Regeneron - DR State File'
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('DR')
			INSERT INTO @PACKING_SLIP VALUES ('Definition')
			SELECT * FROM @PACKING_SLIP;

			EXEC usp_CF_REGENERON_STATE_DR
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'DR'

			EXEC dbo.usp_Get_DataDefinition 
				@UserID = @ClientAdminUserID
		END

		IF @CategoryID = 27 -- 'Regeneron - RVO State File'
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('RVO')
			INSERT INTO @PACKING_SLIP VALUES ('Definition')
			SELECT * FROM @PACKING_SLIP;

			EXEC usp_CF_REGENERON_STATE_RVO
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'RVO'

			EXEC dbo.usp_Get_DataDefinition 
				@UserID = @ClientAdminUserID
		END

	END






	ELSE IF  @ClientID = 15 -- Novartis (Entresto and Jadenu ( 2/6/17 ) ) ; Novartis Access Analysis 2017-06-19
	BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
		
		-- CHF+Entresto
		IF @CategoryID = 15 
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('Changes')
			INSERT INTO @PACKING_SLIP VALUES ('State view')
			INSERT INTO @PACKING_SLIP VALUES ('States')
			INSERT INTO @PACKING_SLIP VALUES ('CHF State Flat File � Full')
			--INSERT INTO @PACKING_SLIP VALUES ('CHF PBM File') -- (to be removed)
			SELECT * FROM @PACKING_SLIP;
		
			----------------------------- Changes
			SELECT 'Changes' AS 'Changes' -- Placeholder for tab. VBA will generate Pivot there

			----------------------------- State view, States, CHF State Flat File � Full
			EXEC usp_CF_NOVARTIS_ENTRESTO
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'CHF'
		END

		-- IO+Jadenu
		IF @CategoryID = 17
		BEGIN
			INSERT INTO @PACKING_SLIP VALUES ('Jadenu State Data')
			SELECT * FROM @PACKING_SLIP;

			EXEC usp_CF_NOVARTIS_JADENU
				@ClientID = @ClientID,
				@Userid = @ClientAdminUserID,
				@IndOrDrug = 'IO'
		END




		-- Novartis Access Analysis
		IF @CategoryID = 19
		BEGIN
			
			-- Indication and Drug parameters are ignored.
			-- Packing logic is here, Ind/Drug/Filtering logic is inside SPs

			INSERT INTO @PACKING_SLIP VALUES ('Afinitor')
			INSERT INTO @PACKING_SLIP VALUES ('Gleevec')
			INSERT INTO @PACKING_SLIP VALUES ('Votrient')
			INSERT INTO @PACKING_SLIP VALUES ('Sandostatin_LAR')
			INSERT INTO @PACKING_SLIP VALUES ('Promacta')
			INSERT INTO @PACKING_SLIP VALUES ('Rydapt')
			SELECT * FROM @PACKING_SLIP;
			
			EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_AFINITOR
				@Userid = @ClientAdminUserID,
				@IndOrDrug = ''

			EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_GLEEVEC
				@Userid = @ClientAdminUserID,
				@IndOrDrug = ''

			EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_VOTRIENT
				@Userid = @ClientAdminUserID,
				@IndOrDrug = ''

			EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_SANDOSTATIN_LAR
				@Userid = @ClientAdminUserID,
				@IndOrDrug = ''

			EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_PROMACTA
				@Userid = @ClientAdminUserID,
				@IndOrDrug = ''

			EXEC usp_CF_NOVARTIS_ACCESS_ANALYSIS_RYDAPT
				@Userid = @ClientAdminUserID,
				@IndOrDrug = ''

		END





		-- Novartis RCC Flat+PBM
		IF @CategoryID = 22
		BEGIN
			-- Indication and Drug parameters are ignored.
			INSERT INTO @PACKING_SLIP VALUES ('Instructions')
			INSERT INTO @PACKING_SLIP VALUES ('Pivots')
			INSERT INTO @PACKING_SLIP VALUES ('RCC Combined')
			SELECT * FROM @PACKING_SLIP;
			
			-- Instructions
			SELECT * FROM CF_NovartisRccInstructions

			-- Pivots
			SELECT 'Pivots' AS 'Pivots'

			-- RCC Combined FLAT+PBM
			EXEC usp_CF_NOVARTIS_RCC_FLAT_PBM
				@Userid = @ClientAdminUserID,
				@IndOrDrug = ''
			
		END


    END



    
    
	ELSE IF  @ClientID = 6 -- BMS
	BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;

		----------------------------- Cursor for indications, reused in several loops
		DECLARE curIndications CURSOR SCROLL
		LOCAL STATIC READ_ONLY
		FOR SELECT ITEM FROM dbo.fnSplit(@IndicationArray,',') fs
		----------------------------- Packing Slip
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				-- All indications have flat file
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )

				-- All indications except HL and URC have PBM file
				IF (@SINGLE_INDICATION <> 'HL') AND (@SINGLE_INDICATION <> 'URC')
					BEGIN
						INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' PBM')
					END
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications
		
		SELECT * FROM @PACKING_SLIP;


		----------------------------- Flat + PBM
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				-- All indications have flat file
				IF @SINGLE_INDICATION = 'HL'
					BEGIN
						EXEC usp_CF_BMS_FLAT_HL @ClientAdminUserID, @SINGLE_INDICATION
						-- No PBM for HL
					END
				ELSE IF @SINGLE_INDICATION = 'MEL'
					BEGIN
						EXEC usp_CF_BMS_FLAT_MEL @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_MEL @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'MM'
					BEGIN
						EXEC usp_CF_BMS_FLAT_MM @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_MM @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'NSCLC'
					BEGIN
						EXEC usp_CF_BMS_FLAT_NSCLC @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_NSCLC @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'RCC'
					BEGIN
						EXEC usp_CF_BMS_FLAT_RCC @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_RCC @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'RA'
					BEGIN
						EXEC usp_CF_BMS_FLAT_RA @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_RA @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'HN'
					BEGIN
						EXEC usp_CF_BMS_FLAT_HN @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_HN @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'URC'
					BEGIN
						EXEC usp_CF_BMS_FLAT_URC @ClientAdminUserID, @SINGLE_INDICATION
						-- No PBM for URC
					END
				ELSE IF @SINGLE_INDICATION = 'CRC'
					BEGIN
						EXEC usp_CF_BMS_FLAT_CRC @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_CRC @ClientAdminUserID, @SINGLE_INDICATION
					END
				ELSE IF @SINGLE_INDICATION = 'HCC'
					BEGIN
						EXEC usp_CF_BMS_FLAT_HCC @ClientAdminUserID, @SINGLE_INDICATION
						EXEC usp_CF_BMS_PBM_HCC @ClientAdminUserID, @SINGLE_INDICATION
					END

				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications
		DEALLOCATE curIndications

    END



	ELSE IF  @ClientID = 9 -- Genentech
	BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;


		IF @CategoryID = 18 -- Custom File
			BEGIN
				-- Asthma (Asthma, CIU) UI allows only 1 indication. @DrugArray ignored, get back all Drugs data
				IF @IndicationArray = 'Asthma'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('Asthma')
						INSERT INTO @PACKING_SLIP VALUES ('CIU')

						INSERT INTO @PACKING_SLIP VALUES ('PBM_Asthma')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_CIU')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_ASTHMA @ClientAdminUserID, 'Asthma'
						EXEC usp_CF_GENENTECH_FLAT_CIU @ClientAdminUserID, 'CIU'

						EXEC usp_CF_GENENTECH_PBM_ASTHMA @ClientAdminUserID, 'Asthma'
						EXEC usp_CF_GENENTECH_PBM_CIU @ClientAdminUserID, 'CIU'
					END

				-- CF. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'CF'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('CF')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_CF')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_CF @ClientAdminUserID, 'CF'
						EXEC usp_CF_GENENTECH_PBM_CF @ClientAdminUserID, 'CF'
					END

				-- IPF. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'IPF'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('IPF')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_IPF')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_IPF @ClientAdminUserID, 'IPF'
						EXEC usp_CF_GENENTECH_PBM_IPF @ClientAdminUserID, 'IPF'
					END


				-- AMD (AMD, DME, DR, RVO) UI allows only 1 indication. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'AMD'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('AMD')
						INSERT INTO @PACKING_SLIP VALUES ('DME')
						INSERT INTO @PACKING_SLIP VALUES ('DR')
						INSERT INTO @PACKING_SLIP VALUES ('RVO')
						INSERT INTO @PACKING_SLIP VALUES ('mCNV')

						INSERT INTO @PACKING_SLIP VALUES ('PBM_AMD')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_DME')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_DR')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_RVO')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_mCNV')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_AMD @ClientAdminUserID, 'AMD'
						EXEC usp_CF_GENENTECH_FLAT_DME @ClientAdminUserID, 'DME'
						EXEC usp_CF_GENENTECH_FLAT_DR @ClientAdminUserID, 'DR'
						EXEC usp_CF_GENENTECH_FLAT_RVO @ClientAdminUserID, 'RVO'
						EXEC usp_CF_GENENTECH_FLAT_MCNV @ClientAdminUserID, 'mCNV'

						EXEC usp_CF_GENENTECH_PBM_AMD @ClientAdminUserID, 'AMD'
						EXEC usp_CF_GENENTECH_PBM_DME @ClientAdminUserID, 'DME'
						EXEC usp_CF_GENENTECH_PBM_DR @ClientAdminUserID, 'DR'
						EXEC usp_CF_GENENTECH_PBM_RVO @ClientAdminUserID, 'RVO'
						EXEC usp_CF_GENENTECH_PBM_MCNV @ClientAdminUserID, 'mCNV'
					END


				-- MS. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'MS'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('MS')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_MS')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_MS @ClientAdminUserID, 'MS'
						EXEC usp_CF_GENENTECH_PBM_MS @ClientAdminUserID, 'MS'
					END


				-- BCC (BCC, BC, CLL, GC, GIST, MCL, NHL, NSCLC, OC, URC) UI allows only 1 indication. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'BCC'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('BCC')
						INSERT INTO @PACKING_SLIP VALUES ('BC')
						INSERT INTO @PACKING_SLIP VALUES ('CLL')
						INSERT INTO @PACKING_SLIP VALUES ('GC')
						INSERT INTO @PACKING_SLIP VALUES ('GIST')
						INSERT INTO @PACKING_SLIP VALUES ('MCL')
						INSERT INTO @PACKING_SLIP VALUES ('NHL')
						INSERT INTO @PACKING_SLIP VALUES ('NSCLC')
						INSERT INTO @PACKING_SLIP VALUES ('OC')
						INSERT INTO @PACKING_SLIP VALUES ('URC')

						INSERT INTO @PACKING_SLIP VALUES ('PBM_BCC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_BC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_CLL')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_GC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_GIST')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_MCL')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_NHL')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_NSCLC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_OC')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_URC')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_BCC @ClientAdminUserID, 'BCC'
						EXEC usp_CF_GENENTECH_FLAT_BC @ClientAdminUserID, 'BC'
						EXEC usp_CF_GENENTECH_FLAT_CLL @ClientAdminUserID, 'CLL'
						EXEC usp_CF_GENENTECH_FLAT_GC @ClientAdminUserID, 'GC'
						EXEC usp_CF_GENENTECH_FLAT_GIST @ClientAdminUserID, 'GIST'
						EXEC usp_CF_GENENTECH_FLAT_MCL @ClientAdminUserID, 'MCL'
						EXEC usp_CF_GENENTECH_FLAT_NHL @ClientAdminUserID, 'NHL'
						EXEC usp_CF_GENENTECH_FLAT_NSCLC @ClientAdminUserID, 'NSCLC'
						EXEC usp_CF_GENENTECH_FLAT_OC @ClientAdminUserID, 'OC'
						EXEC usp_CF_GENENTECH_FLAT_URC @ClientAdminUserID, 'URC'

						EXEC usp_CF_GENENTECH_PBM_BCC @ClientAdminUserID, 'BCC'
						EXEC usp_CF_GENENTECH_PBM_BC @ClientAdminUserID, 'BC'
						EXEC usp_CF_GENENTECH_PBM_CLL @ClientAdminUserID, 'CLL'
						EXEC usp_CF_GENENTECH_PBM_GC @ClientAdminUserID, 'GC'
						EXEC usp_CF_GENENTECH_PBM_GIST @ClientAdminUserID, 'GIST'
						EXEC usp_CF_GENENTECH_PBM_MCL @ClientAdminUserID, 'MCL'
						EXEC usp_CF_GENENTECH_PBM_NHL @ClientAdminUserID, 'NHL'
						EXEC usp_CF_GENENTECH_PBM_NSCLC @ClientAdminUserID, 'NSCLC'
						EXEC usp_CF_GENENTECH_PBM_OC @ClientAdminUserID, 'OC'
						EXEC usp_CF_GENENTECH_PBM_URC @ClientAdminUserID, 'URC'
					END

				-- RA. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'RA'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('RA')
						INSERT INTO @PACKING_SLIP VALUES ('GCA')

						INSERT INTO @PACKING_SLIP VALUES ('PBM_RA')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_GCA')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_RA @ClientAdminUserID, 'RA'
						EXEC usp_CF_GENENTECH_FLAT_GCA @ClientAdminUserID, 'GCA'

						EXEC usp_CF_GENENTECH_PBM_RA @ClientAdminUserID, 'RA'
						EXEC usp_CF_GENENTECH_PBM_GCA @ClientAdminUserID, 'GCA'
					END

				-- Hemo. @DrugArray ignored, get back all Drugs data
				ELSE IF @IndicationArray = 'Hemo'
					BEGIN
						-- Packing slip, sheet names and order
						INSERT INTO @PACKING_SLIP VALUES ('Hemo')
						INSERT INTO @PACKING_SLIP VALUES ('PBM_Hemo')
				
						SELECT * FROM @PACKING_SLIP;

						-- Data
						EXEC usp_CF_GENENTECH_FLAT_HEMO @ClientAdminUserID, 'Hemo'
						EXEC usp_CF_GENENTECH_PBM_HEMO @ClientAdminUserID, 'Hemo'
					END
			END

		IF @CategoryID = 20 -- Specialty Pharmacy
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('Specialty Pharmacy - Genentech')
				SELECT * FROM @PACKING_SLIP;

				EXEC usp_CF_GENENTECH_SPECIALTY_PHARMACY @ClientAdminUserID
			END

		IF @CategoryID = 23 -- Ocular
			BEGIN
				INSERT INTO @PACKING_SLIP VALUES ('Genentech Ocular')
				SELECT * FROM @PACKING_SLIP;

				-- NOTE: We can not use old SPs usp_CF_GENENTECH_FLAT_AMD, usp_CF_GENENTECH_FLAT_DME ...
				--       Because they are designed for different saved process 'Monthly Files for Genentech'
				--       And they have different set of output columns
				EXEC usp_CF_GENENTECH_OCULAR @ClientAdminUserID
			END
	END



	ELSE IF  @ClientID = 51 -- Ipsen
	BEGIN
		--- Get the Admin User ID
        SELECT TOP 1 @ClientAdminUserID = vcu.UserID FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = @ClientID;
    
		----------------------------- Cursor for indications, reused in several loops
		SET @Indications = 'AS,ULS,CervDys,PLLS'; -- Already declared in the code above
		/*DECLARE @SINGLE_INDICATION_FULL_NAME VARCHAR(255)
		
		DECLARE curIndications CURSOR SCROLL
		LOCAL STATIC READ_ONLY
		FOR SELECT ITEM FROM dbo.fnSplit(@Indications,',') fs
			
		----------------------------- Packing Slip
		OPEN curIndications
			FETCH FIRST FROM curIndications INTO @SINGLE_INDICATION
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				SELECT @SINGLE_INDICATION_FULL_NAME = (SELECT TOP 1 Name FROM Indication WHERE Abbreviation = @SINGLE_INDICATION)
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION )
				INSERT INTO @PACKING_SLIP VALUES ( @SINGLE_INDICATION + ' PBM')
				FETCH NEXT FROM curIndications INTO @SINGLE_INDICATION
			END 
		CLOSE curIndications*/

		-- We will add values manually, sheet names in example file don't follow any rule
		INSERT INTO @PACKING_SLIP VALUES ('Adult Spasticity')
		INSERT INTO @PACKING_SLIP VALUES ('Adult Spasticity PBM')
		INSERT INTO @PACKING_SLIP VALUES ('Upper Limb Spasticity')
		INSERT INTO @PACKING_SLIP VALUES ('Upper Limb Spasticity PBM')
		INSERT INTO @PACKING_SLIP VALUES ('CervDyst')
		INSERT INTO @PACKING_SLIP VALUES ('CervDyst PBM')
		INSERT INTO @PACKING_SLIP VALUES ('Pediatric Limb Spasticity')
		INSERT INTO @PACKING_SLIP VALUES ('Pediatric Limb Spasticity PBM')
		SELECT * FROM @PACKING_SLIP;

		EXEC usp_CF_IPSEN_FLAT_AS      @ClientAdminUserID, 'AS',      'Dysport'
		EXEC usp_CF_IPSEN_PBM_AS       @ClientAdminUserID, 'AS',      'Dysport'
		EXEC usp_CF_IPSEN_FLAT_ULS     @ClientAdminUserID, 'ULS',     'Dysport'
		EXEC usp_CF_IPSEN_PBM_ULS      @ClientAdminUserID, 'ULS',     'Dysport'
		EXEC usp_CF_IPSEN_FLAT_CERVDYS @ClientAdminUserID, 'CervDys', 'Dysport'
		EXEC usp_CF_IPSEN_PBM_CERVDYS  @ClientAdminUserID, 'CervDys', 'Dysport'
		EXEC usp_CF_IPSEN_FLAT_PLLS    @ClientAdminUserID, 'PLLS',    'Dysport'
		EXEC usp_CF_IPSEN_PBM_PLLS     @ClientAdminUserID, 'PLLS',    'Dysport'
	END


END









GO