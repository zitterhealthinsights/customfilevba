--USE [CWP_3.0]

SELECT * FROM Process ORDER BY ProcessID DESC
SELECT IDENT_CURRENT('Process')
--DBCC CHECKIDENT ('Process', RESEED, 32);

SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC
SELECT IDENT_CURRENT('ProcessDetail')
--DBCC CHECKIDENT ('ProcessDetail', RESEED, 73);




/*
EXEC usp_CF_SaveProcess
    @ProcessID = 0, -- Add, not Update
    @ProcessName = 'Regeneron - AMD State File',
    @ScheduleTypeID = 2, -- Monthly
    @LastRunUserName = 'Milan', 
    @CategoryID = 24, -- AMD State
    @ClientID = 41, -- Regeneron
    @ExportFilePath = 'C:\Temp\'
GO
SELECT * FROM Process ORDER BY ProcessID DESC

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 33, -- the one we just created
    @ClientID = 41, -- Genentech
    @StepDescription = 'AMD',
    @IndicationNames = 'AMD', -- just a placeholder
    @DrugNames = 'Eylea' -- just a placeholder
GO
SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC
*/



/*
EXEC usp_CF_SaveProcess
    @ProcessID = 0, -- Add, not Update
    @ProcessName = 'Regeneron - DME State File',
    @ScheduleTypeID = 2, -- Monthly
    @LastRunUserName = 'Milan', 
    @CategoryID = 25, -- DME State
    @ClientID = 41, -- Regeneron
    @ExportFilePath = 'C:\Temp\'
GO
SELECT * FROM Process ORDER BY ProcessID DESC

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 34, -- the one we just created
    @ClientID = 41, -- Genentech
    @StepDescription = 'DME',
    @IndicationNames = 'DME', -- just a placeholder
    @DrugNames = 'Eylea' -- just a placeholder
GO
SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC
*/






/*
EXEC usp_CF_SaveProcess
    @ProcessID = 0, -- Add, not Update
    @ProcessName = 'Regeneron - DR State File',
    @ScheduleTypeID = 2, -- Monthly
    @LastRunUserName = 'Milan', 
    @CategoryID = 26, -- DR State
    @ClientID = 41, -- Regeneron
    @ExportFilePath = 'C:\Temp\'
GO
SELECT * FROM Process ORDER BY ProcessID DESC

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 35, -- the one we just created
    @ClientID = 41, -- Genentech
    @StepDescription = 'DR',
    @IndicationNames = 'DR', -- just a placeholder
    @DrugNames = 'Eylea' -- just a placeholder
GO
SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC
*/





/*
EXEC usp_CF_SaveProcess
    @ProcessID = 0, -- Add, not Update
    @ProcessName = 'Regeneron - RVO State File',
    @ScheduleTypeID = 2, -- Monthly
    @LastRunUserName = 'Milan', 
    @CategoryID = 27, -- RVO State
    @ClientID = 41, -- Regeneron
    @ExportFilePath = 'C:\Temp\'
GO
SELECT * FROM Process ORDER BY ProcessID DESC

EXEC usp_CF_SaveStep
    @ProcessDetailID = 0, -- Add, not Update
    @ProcessID = 36, -- the one we just created
    @ClientID = 41, -- Genentech
    @StepDescription = 'RVO',
    @IndicationNames = 'RVO', -- just a placeholder
    @DrugNames = 'Eylea' -- just a placeholder
GO
SELECT * FROM ProcessDetail ORDER BY ProcessDetailID DESC
*/





SELECT * FROM Process WHERE ClientID = 41


SELECT * FROM Process
UPDATE Process
	SET ExportFilePath = 'C:\Temp\'
WHERE ExportFilePath = 'C:\Temp'
SELECT * FROM Process