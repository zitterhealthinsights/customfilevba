--USE [CWP_3.0]

SELECT c.* FROM dbo.Category c
SELECT cc.* FROM dbo.ClientCategory cc

INSERT dbo.Category (CategoryName,Active)
VALUES('AMD State',1) -- New ID should be 24
GO
INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(41,24,1) -- New ID should be 24
GO


INSERT dbo.Category (CategoryName,Active)
VALUES('DME State',1) -- New ID should be 25
GO
INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(41,25,1) -- New ID should be 25
GO


INSERT dbo.Category (CategoryName,Active)
VALUES('DR State',1) -- New ID should be 26
GO
INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(41,26,1) -- New ID should be 26
GO


INSERT dbo.Category (CategoryName,Active)
VALUES('RVO State',1) -- New ID should be 27
GO
INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(41,27,1) -- New ID should be 27
GO



SELECT c.* FROM dbo.Category c
SELECT cc.* FROM dbo.ClientCategory cc

GO



