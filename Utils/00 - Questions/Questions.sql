1. How do we get @Userid = 3236 in examples below ? 
SELECT * FROM dbo.vw_ClientUsers vcu WHERE vcu.Username LIKE '%_Admin' AND ClientID = 58;

2. Are the calls ok ?
3. Do we need @isPBMOnly = 0 for FLAT ?

---------------------------------------------------- FLAT
EXEC dbo.usp_Get_Plans
	@IndicationorDrug = 'NEU',
	@Userid = 3236
	
---------------------------------------------------- PBM

EXEC dbo.usp_Get_Plans
	@IndicationorDrug = 'NEU',
	@Userid = 3236,
	@isPBMOnly = 1

---------------------------------------------------- State

EXEC dbo.usp_Get_Plans
	@IndicationorDrug = 'NEU',
	@Userid = 3236,
    	@Level = 'State'