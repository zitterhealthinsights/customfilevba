USE [CWP_3.0]

INSERT dbo.Category (CategoryName,Active)
VALUES('Custom File',1) -- New ID should be 9

INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(58,9,1) -- New ID should be 9


INSERT dbo.Category (CategoryName,Active)
VALUES('PayerSciences',1) -- New ID should be 10

INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(58,10,1) -- New ID should be 10


INSERT dbo.Category (CategoryName,Active)
VALUES('McCann',1) -- New ID should be 11

INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(58,11,1) -- New ID should be 11


INSERT dbo.Category (CategoryName,Active)
VALUES('Protean',1) -- New ID should be 12

INSERT dbo.ClientCategory(ClientID,CategoryID,Active)
VALUES(58,12,1) -- New ID should be 12


SELECT c.* FROM dbo.Category c
SELECT cc.* FROM dbo.ClientCategory cc

GO