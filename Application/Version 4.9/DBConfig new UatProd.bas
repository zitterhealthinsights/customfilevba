Attribute VB_Name = "DBConfig"
Option Explicit

Public Const LOCAL_DB As Integer = 1
Public Const DEV  As Integer = 2
Public Const QA  As Integer = 3
Public Const UAT  As Integer = 4
Public Const PROD  As Integer = 5


'----------------------------------------------------------
Public Const DB_SERVER As Integer = LOCAL_DB
Public Const VERSION_NUMBER As Double = 4.9
Public Const VERSION_DATE As Date = #11/27/2018#           ' PROD release date, just a label, not used for check
'----------------------------------------------------------

Private Const APPLICATION_ID As Integer = 4 ' Application specific, ID in Jetpack Applications table

Public Enum enmDatabase
    MCMM_PROD = 1
    JetPack = 2
    CWP = 3
End Enum

Public CURRENT_USER As CurrentUser

Public Function GetServerDisplayName() As String
    Select Case DB_SERVER
        Case LOCAL_DB
            GetServerDisplayName = "LOCAL"
        Case DEV
            GetServerDisplayName = "DEV"
        Case QA
            GetServerDisplayName = "QA"
        Case UAT
            GetServerDisplayName = "UAT"
        Case PROD
            GetServerDisplayName = "PROD"
        Case Else
            GetServerDisplayName = "Unknown"
    End Select
End Function

Public Sub FillServerTextbox(tb As MSForms.TextBox)
    tb.Text = GetServerDisplayName
    
    Select Case DB_SERVER
        Case LOCAL_DB
            tb.Font.Bold = True
            tb.BackColor = vbRed
        Case DEV
            tb.Font.Bold = True
            tb.ForeColor = vbRed
        Case QA
            tb.Font.Bold = True
            tb.ForeColor = vbYellow
            tb.BackColor = vbBlack
        Case UAT
            tb.Font.Bold = True
            tb.ForeColor = vbBlue
        Case PROD
            ' As it is on a form in design time
        Case Else
            
    End Select
End Sub

Public Sub ColorFrame(frm As MSForms.Frame)
    Select Case DB_SERVER
        Case LOCAL_DB
            frm.BackColor = RGB(198, 224, 180)
        Case DEV
            frm.BackColor = RGB(255, 200, 200)
        Case QA
            frm.BackColor = RGB(255, 230, 153)
        Case UAT
            frm.BackColor = RGB(180, 198, 231)
        Case PROD
            ' As it is on a form in design time
        Case Else
            
    End Select
End Sub

Private Function GetLocalServer() As String
Dim strWinUserName As String

    strWinUserName = CStr(Environ("UserName"))
    
    If strWinUserName = "Milan" Then
        GetLocalServer = "RADOVAN4\SQLEXPRESS"
    ElseIf strWinUserName = "Tejas" Then
        GetLocalServer = "SAI\SQLEXPRESS"
     ElseIf strWinUserName = "EHASANUL" Then
        GetLocalServer = "localhost"
    Else
        GetLocalServer = ".\SQLEXPRESS"
    End If
End Function

Public Function GetConnectionString(DB As enmDatabase) As String
Dim strServer As String
Dim strDbName As String
Dim strUserID As String
Dim strUserPass As String

    GetConnectionString = ""

    Select Case DB
            
        Case JetPack
            Select Case DB_SERVER
                Case LOCAL_DB
                    strServer = GetLocalServer: strDbName = "JetPack_PROD": strUserID = "": strUserPass = ""
                Case DEV
                    strServer = "10.192.8.42\SQLSERVER2016": strDbName = "JetPack_DEV": strUserID = "VBA_FG": strUserPass = "cwp3.0"
                Case QA
                    strServer = "10.192.8.42\SQLSERVER2016": strDbName = "JetPack_QA": strUserID = "VBA_FG": strUserPass = "cwp3.0"
                Case UAT
                    strServer = "TZGSRVSQL02": strDbName = "JETPACK_UAT": strUserID = "PATT_UATPROD_UserID": strUserPass = "bluejuice25"
                Case PROD
                    strServer = "TZGSRVSQL02": strDbName = "JETPACK": strUserID = "PATT_UATPROD_UserID": strUserPass = "bluejuice25"
            End Select
        
        Case CWP
            Select Case DB_SERVER
                Case LOCAL_DB
                    strServer = GetLocalServer: strDbName = "CWP_3.0": strUserID = "": strUserPass = ""
                Case DEV
                    strServer = "10.192.8.42\SQLSERVER2016": strDbName = "CWP_VBA_DEV": strUserID = "VBA_FG": strUserPass = "cwp3.0"
                Case QA
                    strServer = "10.192.8.42\SQLSERVER2016": strDbName = "CWP_VBA_QA": strUserID = "VBA_FG": strUserPass = "cwp3.0"
                Case UAT
                    strServer = "TZGSRVSQL01": strDbName = "PATT_UAT": strUserID = "PATT_UATPROD_UserID": strUserPass = "bluejuice25"
                Case PROD
                    strServer = "TZGSRVSQL01": strDbName = "PATT": strUserID = "PATT_UATPROD_UserID": strUserPass = "bluejuice25"
            End Select
            
        Case MCMM_PROD
            Select Case DB_SERVER
                Case LOCAL_DB
                    strServer = GetLocalServer: strDbName = "MCMM_PROD": strUserID = "": strUserPass = ""
                Case DEV
                    strServer = "10.192.8.42\SQLSERVER2016": strDbName = "MCMM_VBA_DEV": strUserID = "VBA_FG": strUserPass = "cwp3.0"
                Case QA
                    strServer = "10.192.8.42\SQLSERVER2016": strDbName = "MCMM_VBA_QA": strUserID = "VBA_FG": strUserPass = "cwp3.0"
                Case UAT
                    strServer = "TZGSRVSQL01": strDbName = "MCMM_UAT": strUserID = "MCMM_UATPROD_UserID": strUserPass = "roseox83"
                Case PROD
                    strServer = "TZGSRVSQL01": strDbName = "MCMM": strUserID = "MCMM_UATPROD_UserID": strUserPass = "roseox83"
            End Select
    End Select
    
    If DB_SERVER = LOCAL_DB Then
        GetConnectionString = "Provider=SQLOLEDB;" & _
                              "Server=" & strServer & ";" & _
                              "Database=" & strDbName & ";" & _
                              "Trusted_connection=yes;"
    
        
    Else
        GetConnectionString = "Provider=SQLOLEDB.1;" & _
                              "Server=" & strServer & ";" & _
                              "Database=" & strDbName & ";" & _
                              "User ID=" & strUserID & ";" & _
                              "Password=" & strUserPass & ";" & _
                              "persist security info=False;packet size=4096;Pooling=true;"
    End If
End Function

Public Function VersionExpirationOK() As Boolean

    VersionExpirationOK = False

    Set CURRENT_USER = Nothing
    Set CURRENT_USER = ObjectFactory.GetUser(Constants.WIN_USERNAME, APPLICATION_ID, GetServerDisplayName, VERSION_NUMBER)

    If CURRENT_USER Is Nothing Then
        VersionExpirationOK = False
    Else
        SESSION_ID = CURRENT_USER.SessionID
        USER_ID = CURRENT_USER.UserID
       
        VersionExpirationOK = True
    End If
End Function






